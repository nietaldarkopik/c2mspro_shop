<?php
//@session_save_path('.');
//@session_name($_SERVER['SERVER_NAME']);
session_start();
include 'configs/vars.php';
include 'configs/database.php';
include 'libraries/dbconnect.php';
include 'libraries/settings.php';
include 'libraries/null.php';
include 'libraries/auth.php';
include 'libraries/formatdate.php';
include 'libraries/function.php';
if($_SERVER['QUERY_STRING'] == '' || $_GET['node1'] == 'search')
{
	$homepage = HOMEPAGE;
	$q	  = $mysqli->query("select * from pages where id='$homepage' and status='1'");

	if($q->num_rows == 0)
	{
		echo '<h1>Site Offline</h1>';
		//include 'components/adminbar.php';
		exit();
	}
	else
	{	
		$page = $q->fetch_object();
		if ($_SERVER['QUERY_STRING'] != '' && $_GET['node1'] =='search')
		{
			define('PAGEID',$page->id);
			define('PAGEHREF','search');	
		}else{
			define('PAGEID',$page->id);
			define('PAGEHREF',$page->href);	
		}	
	}
}
else
{		
	$q	= $mysqli->query("select * from pages where href='$node' and status='1'");		
	if($q->num_rows == '0')
	{			
			//Array ( [0] => node1=galeri-foto [1] => node2=pembangunan-menara-eiffel [2] => node3=19 )

			$nodes	= @explode('&', $_SERVER['QUERY_STRING']);
			
			$node1	= @explode('=',$nodes[0]);		
			$node1 = @$node1[1];
			

			$node2	= @explode('=',$nodes[1]);
			$node2 = @$node2[1];
			

			$node3	= @explode('=',$nodes[2]);
			$node3 = @$node3[1];
		
			$q	= $mysqli->query("select * from pages where href='$node1' and status='1'");
			
			if($q->num_rows != '0')
			{		
			$page	= $q->fetch_object();
			define('PAGEID',$page->id);
			define('PAGEHREF',$page->href);
			}
				else
			{
				header("Location: ".URL.ERROR_PAGES."");
				exit;
			}
	}
	else
	{
	 	$page	= $q->fetch_object();
		define('PAGEID',$page->id);
		define('PAGEHREF',$page->href);
		//echo URL.ERROR-PAGES;
	}
}
//search
include 'components/search.php';
$search = new search;

//css
include 'components/css.php';
$css = new css($mysqli);

//css
include 'components/js.php';
$js = new js;

//logo
include 'components/logo.php';
$logo = new logo;

include 'templates/'.$tpl.'/manage/libraries.php';
$header = new header;

//highlight
include 'components/highlight.php';
$highlight = new highlight($mysqli);

//menu
include 'components/menu.php';
$menu = new menu($mysqli);

//submenu
include 'components/submenu.php';
$submenu = new submenu($mysqli);

//page
define('PAGETYPE',$page->page_type);
define('CONTENT',$page->content);
include 'components/content.php';
$content = new content($mysqli);

$query = $mysqli->query("select category_href from plugin_blog_categories");
while ($data = $query->fetch_assoc()) $category[] = $data["category_href"];

if ($page->id == HOMEPAGE) {
	define('TITLE',SITE_TITLE);
	define('MENU_TITLE',ucwords($page->title));
	define('KEYWORDS',SITE_KEYWORD);
	define('DESCRIPTION',SITE_DESCRIPTION);
}
else if ($page->page_type =='plugin' && $page->content=='blog' && isset($_GET['node3']) && !(is_numeric($_GET['node3'])))
{
	$q = $mysqli->query("select * from plugin_blog_posts where post_href='$_GET[node3]'");
	$blog = $q->fetch_object();
	
	define('TITLE',ucwords($blog->title));
	define('MENU_TITLE',ucwords($blog->title));
	define('KEYWORDS',$blog->keywords);
	define('DESCRIPTION',$blog->description);
}
else if ($page->page_type =='plugin' && $page->content=='blog' && isset($_GET['node2']) && !(is_numeric($_GET['node2'])) && in_array($_GET['node2'],$category))
	{
		$q= $mysqli->query("select * from plugin_blog_categories where category_href='$_GET[node2]'");
		$blog = $q->fetch_object();
		
		define('TITLE',ucwords($blog->category));
		define('MENU_TITLE',ucwords($blog->category));
		define('KEYWORDS',$blog->category);
		define('DESCRIPTION',$blog->category);	
}
else
	{
	define('TITLE',ucwords($page->title.' | '.$page->site_title));
	define('MENU_TITLE',ucwords($page->title));
	define('KEYWORDS',$page->keywords);
	define('DESCRIPTION',$page->description);
}
include 'components/title.php';
$title = new title;

include 'components/keywords.php';
$keywords = new keywords;

include 'components/description.php';
$description = new description;

//footer
include 'components/footer.php';
$footer = new footer($mysqli);

include 'components/widget.php';
$widget = new widget($mysqli);

if($_SERVER['QUERY_STRING'] != '')
{
	$link = explode("&",$_SERVER['QUERY_STRING']);
	foreach ($link as $value){
		$linked = explode("=", $value);
		$linking[] = $linked[1];
		$linkvalue[] = str_replace("-"," ", $linked[1]);
	}
	if (!empty($linking[0])) 
	{
		if (isset($linking[1])) $linkbar = "<a href=\"".URL.$linking[0]."\">".($linkvalue[0])."</a>"; 
		else $linkbar = ($title->view());
	}
	if (!empty($linking[1]))
	{
		if (isset($linking[2])) $linkbar .= "&nbsp;<a href=\"".URL.$linking[0]."/".$linking[1]."\">".($linkvalue[1])."</a>";
		else $linkbar .= "&nbsp;".($title->view());
	}
	if (!empty($linking[2])) $linkbar .= "&nbsp;".($title->view());
		
}else $linkbar = "HOME";
define('LINKBAR',$linkbar);
define('PAGE_TYPE',$page->pageType);
define('SLIDESHOW',$page->slideshow);
define('SHOW_TITLE',$page->show_title);
if ($page->id == HOMEPAGE) {
	if	($page->pageType == 'left' ) include 'templates/'.$tpl.'/leftSide.html'; 
	else if	($page->pageType == 'right' ) include 'templates/'.$tpl.'/rightSide.html'; 
	else if ($page->pageType == "full") include 'templates/'.$tpl.'/fullPage.html'; 
	else include 'templates/'.$tpl.'/index.html'; 
}
else if ($page->pageType == "full") include 'templates/'.$tpl.'/fullPage.html'; 
else if ($page->pageType == "left") include 'templates/'.$tpl.'/leftSide.html'; 
else if ($page->pageType == "right") include 'templates/'.$tpl.'/rightSide.html'; 
else include 'templates/'.$tpl.'/fullPage.html'; 
include 'components/adminbar.php';
?>