<?php
function generate_seo_link($input,$replace = '-',$remove_words = true,$words_array = array())
{
	$return = trim(preg_replace('{ +}',' ',preg_replace('/[^a-zA-Z0-9\s]/','',strtolower($input))));

	if($remove_words) 
	{ 
		$return = remove_words($return,$replace,$words_array); 
	}

	return str_replace(' ',$replace,$return);
}

function remove_words($input,$replace,$words_array = array(),$unique_words = true)
{
	$input_array = explode(' ',$input);

	$return = array();

	foreach($input_array as $word)
	{
		if(!in_array($word,$words_array) && ($unique_words ? !in_array($word,$return) : true))
		{
			$return[] = $word;
		}
	}
	return implode($replace,$return);
}
?>