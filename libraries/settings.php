<?php
//load template, path & url
$q		= $mysqli->query("select value from settings where param='template'");
$tpl	= $q->fetch_object();
$tpl	= $tpl->value;
define('PATH',$path);
define('URL',$url);
define('TPL',$tpl);

//check menu type
$q		= $mysqli->query("select value from settings where param='menu'");
$menu	= $q->fetch_object();
$menu	= $menu->value;
define('MENU',$menu);

//check url node
$nodes	= explode('&', $_SERVER['QUERY_STRING']);
$node	= explode('=', end($nodes));
$node	= end($node);
define('NODE',$node);

//SITE NAME
$q		= $mysqli->query("select value from settings where param='site'");
$site	= $q->fetch_object();
define('SITE',$site->value);
define('SITE_TITLE',$site->value);


//SITE KEYWORDS
$q		= $mysqli->query("select value from settings where param='keyword'");
$site	= $q->fetch_object();
define('SITE_KEYWORD',$site->value);

//SITE DESCRIPTION
$q		= $mysqli->query("select value from settings where param='description'");
$site	= $q->fetch_object();
define('SITE_DESCRIPTION',$site->value);

//HOME PAGE
$q			= $mysqli->query("select value from settings where param='homepage'");
$homepage	= $q->fetch_object();
define('HOMEPAGE',$homepage->value);

//FAVICON
$q		= $mysqli->query("select * from settings where param='favicon'");
$favicon	= $q->fetch_object();
define('FAVICON',$favicon->value);
define('USEDEFAULTFAVICON',$favicon->usedefault);

//LOGO
$q		= $mysqli->query("select * from settings where param='logo'");
$logo	= $q->fetch_object();
define('LOGO',$logo->value);
define('USEDEFAULTLOGO',$logo->usedefault);

// ORGANIZATION
$q		= $mysqli->query("select * from settings where param='organization'");
$organization	= $q->fetch_object();
define('ORGANIZATION',$organization->value);

// ADDRESS
$q			= $mysqli->query("select * from settings where param='address'");
$address	= $q->fetch_object();
define('ADDRESS',$address->value);

// PHONE
$q		= $mysqli->query("select * from settings where param='phone'");
$phone	= $q->fetch_object();
define('PHONE',$phone->value);

// HP
$q		= $mysqli->query("select * from settings where param='hp'");
$hp		= $q->fetch_object();
define('HP',$hp->value);

// FAX
$q		= $mysqli->query("select * from settings where param='fax'");
$fax	= $q->fetch_object();
define('FAX',$fax->value);

//EMAIL
$q		= $mysqli->query("select * from settings where param='email'");
$email	= $q->fetch_object();
define('EMAIL',$email->value);

// ALT EMAIL
$q		= $mysqli->query("select * from settings where param='altemail'");
$altemail	= $q->fetch_object();
define('ALTEMAIL',$altemail->value);

// ALT EMAIL
$q		= $mysqli->query("select * from settings where param='map'");
$altemail	= $q->fetch_object();
define('MAP',$altemail->value);

// ALT EMAIL
$q		= $mysqli->query("select * from settings where param='imgcontact'");
$altemail	= $q->fetch_object();
define('IMGCONT',$altemail->value);

$q		= $mysqli->query("select email, admin_name from admins LIMIT 1");
$adm	= $q->fetch_object();
define('AUTHOR',$adm->admin_name);
define('ADM_MAIL',$adm->email);

//error-Pages
define('ERROR_PAGES','error-page');
?>