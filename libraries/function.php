<?PHP
	function pagging($no, $totpage, $href) { 
		$output = '<div class="wp-pagenavi clearfix">';
		$output .= '&nbsp;<span class="pages">Page '.$no.' of '.$totpage.'</span>';
		if ($no > 1) $output .= '&nbsp;<a href="'.$href.'/'.($no-1).'" title="'.($no-1).'" class="prevpostslink">Previous</a>';
		else  $output .= '&nbsp;<a href="#" class="prevpostslink">Previous</a>';
		$no >= ($totpage-5) ? $y = $totpage : $y = $no+5;
		$z=1;
		if ($no>5) $z = $no-5;
		for($i=$z; $i<=$y; $i++)
		{
		if ((($i > $no - 5) && ($i <= $no + 5)) || ($i == 1) || ($i == $totpage))
			{
				$link = '&nbsp;<a href="'.$href.'/'.$i.'" class="page">'.$i.'</a>';
				if ($i==$no) $link = '&nbsp;<span class="current">'.$i.'</span>';
				$output .= $link;
			}
		}
		if ($no < $totpage) 
		$output .= '&nbsp;<a href="'.$href.'/'.($no+1).'" class="nextpostslink">Next</a></li>'; 
		else $output .= '&nbsp;<a href="#" class="nextpostslink">Next</a>'; 
		$output .= '</div><!--/ .wp-pagenavi-->';		
		return $output;
   }
   function ajaxPagging($no, $totpage, $href) { 
	echo '<div class="wp-pagenavi clearfix">';
    echo "Page ";
	if ($no > 1) echo '<a href="javascript:void(0);" class="openmodalbox prevpostslink page-numbers" ><input type="hidden" name="ajaxhref" value="'.$href.'&amp;p='.($no-1).'"/>&larr;</a>&nbsp;';

		$no > ($totpage-5) ? $y = $totpage : $y = $no+5; 
		$z=1;
		if ($no>5) $z = $no-5;
			for($i=$z; $i<=$y; $i++)

				{
				
				if ((($i > $no - 5) && ($no <= $no + 5)) || ($i == 1) || ($i == $totpage))
					{ 
						$link = '<a href="javascript:void(0);" class="openmodalbox page"><input type="hidden" name="ajaxhref" value="'.$href.'&amp;p='.$i.'"/>'.$i.'</a>&nbsp;';
						if ($i==$no) $link = '<span class="page current">'.$i.'</span>&nbsp;';
						echo $link;
					}

				}
	if ($no < $totpage) echo '<a href="javascript:void(0);" class="openmodalbox nextpostslink page-numbers" ><input type="hidden" name="ajaxhref" value="'.$href.'&amp;p='.($no+1).'"/>&rarr;</a>&nbsp;';
	echo '</div>';
   }

	function short_view($data, $batas){
		$content = strip_tags($data);
		$count = strlen($content);
		if($count>$batas) $data = substr($content,0,$batas)."...";
		return $data;
}
function ext($str) {
	 $i = strrpos($str,".");
	 if (!$i) { return ""; }
	 $l = strlen($str) - $i;
	 $ext = substr($str,$i+1,$l);
	 return $ext;
 }

function loadMaxDir($handle) {
	if ($data = opendir($handle)) {
		while (false !== ($file = readdir($data))) 
		{
			$getFile[]= $file;		
			if ($file != "." && $file != ".." && is_file($file)) {
				$getFile[]= $file;				
			}
		}
	
			$x = count($getFile);
			sort($getFile,SORT_REGULAR);
			//print_r($getFile);
			//exit;
			return $getFile[$x-1];
		closedir($data);
	}
}
function loadDir($handle) {
	if ($data = opendir($handle)) {
		$getFile = array();
		while (false !== ($file = readdir($data))) {
		if ($file != "." && $file != "..") {
			$getFile[]= $file;
			}
		}
			//print_r($getFile);
			sort($getFile,SORT_REGULAR);
			return $getFile;
		closedir($data);
	}
}
function rrmdir($dir) {
	  if (is_dir($dir)) {
	    $objects = scandir($dir);
	    foreach ($objects as $object) {
	      if ($object != "." && $object != "..") {
	        if (filetype($dir."/".$object) == "dir") 
	           rrmdir($dir."/".$object); 
	        else unlink   ($dir."/".$object);
	      }
	    }
	    reset($objects);
	    rmdir($dir);
	  }
	 }
 function input($type,$name,$id='',$value='',$param='',$selectedValue='') {
	if(empty($id)) $id = $name;
	if($type=="textarea") {
	  $html = "<textarea name=\"$name\" id=\"$id\" $param>$value</textarea>\n";
	  return $html;
	  }

	elseif($type=="select") {
	  $html = "\n<select name=\"$name\" id=\"$id\" $param>\n";
	  foreach($value as $k => $v) {
		if(is_array($selectedValue) && in_array($k,$selectedValue)) $selected = 'selected="selected"';
		elseif($selectedValue == $k) {$selected = 'selected="selected"';}
		else {$selected = '';}
		
		//if(in_array($k,is_array($selectedValue))) $selected = 'selected="selected"'; else $selected = '';
		$html .= "\t<option value=\"$k\" $selected>$v</option>\n";
		}
	  $html .="</select>\n";
	  }
	  
	  elseif($type=="check" || $type=="radio") {
	  $tot = count($value);
	  $i = 1;
	  while ( $i < $tot)
	  {
	  foreach($value as $k => $v) {
		if(is_array($selectedValue) && in_array($k,$selectedValue)) $selected = 'checked="checked"';
		elseif($selectedValue == $k) {$selected = 'checked="checked"';}
		else {$selected = '';}
		if ($k == "other") $k="0"; else $k=$k; 
		//if(in_array($k,is_array($selectedValue))) $selected = 'selected="selected"'; else $selected = '';
		$html .= "<input type=\"$type\" name=\"$name\" value=\"$k\" $param $selected id=\"$k\" />&nbsp;$v";
		$i++;		
		}
	   
	   }
	  return $html;
	  }

	else  {
	  $html = "<input type=\"$type\" name=\"$name\" id=\"$id\" value=\"$value\" $param>\n";
	  }
	return $html;
	}

	function selectOption($array=array(),$first,$selectedValue='',$param='', $name='')
 {
 	if($first!="NULL") $array[""] = $first;
	if(empty($name)) $name = $array; else $name = $name; 
	return input('select',$name,$name,$array,$param,$selectedValue);
 }
 function dateSelected($name,$date) {
 	list($y, $m, $d) = array_pad(explode('-', $date, 3), 3, 0);
 	$currYear = date("Y");
 	for ($i=1; $i<=31; $i++ ) { $tanggal[$i] = $i<10? "0".$i : $i;}
 	$bulan = ["01"=>"Januari","02"=>"Februari","03"=>"Maret","04"=>"April", "05"=>"Mei","06"=>"Juni","07"=>"Juli", "08"=>"Agustus", "09"=>"September", "10"=>"Oktober", "11"=>"November","12"=>"Desember"];
 	for ($i=$currYear-1; $i<$currYear+5; $i++ ) {$tahun[$i] = $i;}
 	$selectDate = selectOption($tanggal,'Pilih Tanggal',[$d],'',$name."[d]");
 	$selectMonth = selectOption($bulan,'Pilih Bulan',[$m],'',$name."[m]");
 	$selectYear = selectOption($tahun,'Pilih Tahun',[$y],'',$name."[y]");

 	return $selectDate . $selectMonth.$selectYear;
 }
 function alert($alert=null, $link="")
{
	echo "<script type=\"text/javascript\">\n";
	if ($alert !=null) echo "alert('".$alert."');\n";
	if ($link== "") echo "history.back()"; else echo "location.href='".$link."'";
	echo "</script>";
	exit;
}
?>