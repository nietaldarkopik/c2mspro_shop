<?php
function formatdate($date)
{
	$day	= substr($date,-2);
	$month	= substr($date,2,-3);
	$year	= substr($date,0,4);
	
	switch($month)
	{
		case 01:
			$month	= 'Januari';
		case 02:
			$month	= 'Februari';
		case 03:
			$month	= 'Maret';
		case 04:
			$month	= 'April';
		case 05:
			$month	= 'Mei';
		case 06:
			$month	= 'Juni';
		case 07:
			$month	= 'Juli';
		case 08:
			$month	= 'Agustus';
		case 09:
			$month	= 'September';
		case 10:
			$month	= 'Oktober';
		case 11:
			$month	= 'November';
		case 12:
			$month	= 'Desember';
	}

	$date = $day.'&nbsp;'.$month.'&nbsp;'.$year;

	return $date;
}
?>