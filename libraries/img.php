<?php
function resize($upfile,$img,$tn_width,$tn_height,$path)
{	
	$thumb =  $path.$img;		

	$size = GetImageSize($upfile);
	$width = $size[0];
	$height = $size[1];

	if(substr($img,-3) == 'jpg' || substr($img,-4) == 'jpeg')
	{
		$src = @ImageCreateFromJpeg($upfile);
		$dst = @ImageCreateTrueColor($tn_width, $tn_height);

		$resized = @imagecopyresized($dst, $src, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
		@ImageJpeg($dst,$thumb,'100');
		@ImageDestroy($src);
		@ImageDestroy($dst);
	}
	elseif(substr($img,-3) == 'gif')
	{
		$src = @imageCreateFromGif ($upfile);
		$dst = @ImageCreateTrueColor($tn_width, $tn_height);
		
		$bgcolor = @imagecolorallocate($dst, 255, 255, 255);
		@imagefilledrectangle ($dst, 0, 0, $tn_width, $tn_height, $bgcolor);

		@imagecopyresampled($dst, $src, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
		@ImageGif($dst,$thumb);
		@ImageDestroy($src);
		@ImageDestroy($dst);
	}
	elseif(substr($img,-3) == 'png')
	{
		$src = @imageCreateFromPng($upfile);
		$dst = @ImageCreateTrueColor($tn_width, $tn_height);
		
		$bgcolor = @imagecolorallocate($dst, 255, 255, 255);
		@imagecolortransparent($dst, $bgcolor);
		@imagefilledrectangle ($dst, 0, 0, $tn_width, $tn_height, $bgcolor);

		@imagecopyresampled($dst, $src, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
		@ImagePng($dst,$thumb,10);
		@ImageDestroy($src);
		@ImageDestroy($dst);
	}
}

function ext($str) 
{
	 $i = strrpos($str,".");
	 if (!$i) { return ""; }
	 $l = strlen($str) - $i;
	 $ext = substr($str,$i+1,$l);
	 return $ext;
 }
?>