<?PHP
class idCreator {
	public $digits = 4;
	var $angka = array();
	var $string = array();
	var $id;
	
	function __construct($digits) {
		$this->digits = $digits;
		return $this->generate();
	}
	
	function generate() {
		$this->randomNumber();
		$this->randomHuruf();
		$this->merge();
	}		
	function randomNumber() {
		$this->digits;
		$angka = rand(0, pow(10,$this->digits)-1);
		$angka = str_pad($angka, $this->digits, rand(0, 9), STR_PAD_BOTH);
		$angka."<br>";
		$this->angka = str_split($angka);
	}
	function randomHuruf() {
		$char = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		$randomHuruf = "";
		for ($i = 0; $i<$this->digits; $i++)
		{
			$randomHuruf .= $char[rand(0,strlen($char)-1)];
		}
		$this->string = str_split($randomHuruf);
	}
	function merge(){
		$mergeArray = array();
		while($this->angka) {
			$mergeArray[] = array_shift($this->string);
			$mergeArray[] = array_shift($this->angka);
		}
		$this->id = implode($mergeArray);
	}
}