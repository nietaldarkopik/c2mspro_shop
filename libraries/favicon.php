<?php
function favicon($upfile,$img,$tn_width,$tn_height)
{	
	$thumb	 =  './../images/user/'.$img;

	$arr	 = explode('.',$img);
	$favicon = './../images/user/'.$arr[0].'.png'; 

	$size = GetImageSize($upfile);
	$width = $size[0];
	$height = $size[1];

	if(substr($img,-3) == 'jpg' || substr($img,-4) == 'jpeg')
	{
		$src = @ImageCreateFromJpeg($upfile);
	}
	elseif(substr($img,-3) == 'gif')
	{
		$src = @imageCreateFromGif ($upfile);

	}
	elseif(substr($img,-3) == 'png')
	{
		$src = @imageCreateFromPng($upfile);
		
	}

	$dst = @ImageCreateTrueColor($tn_width, $tn_height);				
	$bgcolor = @imagecolorallocate($dst, 255, 255, 255);

	@imagecolortransparent($dst, $bgcolor);

	@imagefilledrectangle ($dst, 0, 0, $tn_width, $tn_height, $bgcolor);

	@imagecopyresampled($dst, $src, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
	@ImagePng($dst,$favicon,1);
	@ImageDestroy($src);
	@ImageDestroy($dst);
}
?>