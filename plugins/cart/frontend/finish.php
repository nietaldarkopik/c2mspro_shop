<?php
if(!isset($_SESSION['cart'])) :
    echo "There are no items in your shopping cart!";
else:
?>
<div class="contentBox clearfix">
    <div class="clearfix_X leftBox push_1 login_wrap">
    	<div class="entry-title">
        <h2 class="title">Order Detail</h2>			
      </div>
      <table border="2" cellpadding="5px" cellspacing="1px" class="finish-order" width="100%">
    	<?php
        	echo '<tr><th>Name</th><th>Price</th><th>Qty</th><th>Amount</th></tr>';
			$max=count($_SESSION['cart']);
			$total = 0;
			for($i=0;$i<$max;$i++){
				$itemid=$_SESSION['cart'][$i]['itemid'];
				$q=$_SESSION['cart'][$i]['qty'];
				$itemLib->itemid = $itemid;
				$item = $itemLib->getItem();
				$price = $item->price-$itemLib->diskon();
				$subtotal = $q*$price;
				$total = $total+$subtotal;
				if($q==0) continue;
		?>
        		<tr style="<?php echo $i%2==0 ? 'background-color:#ECECEC;' : ''; ?>"><td><?php echo ($i+1).". ".$item->name;?></td>
                <td><?php echo $itemLib->number($price,"IDR");?></td>
                <td><?php echo $q?></td>                    
                <td><?php echo $itemLib->number($subtotal,"IDR");?></td
                </tr>
        <?php }	?>
		<tr><td colspan="3">Total</td><td><?php echo $itemLib->number($total,"IDR");?></td></tr>
        </table>
        <div class="entry-title">
			<h2 class="title">Payment Detail</h2>			
		</div>
		<form id="registerForm" name="registerForm" method="post" enctype="multipart/form-data">
		<?php if ($msg != "") : ?>
        	<div class="pesan login_row"><?php echo $msg; ?></div>
        <?php endif; ?>
        <div class="login_row">
        	<div class="login_t_label">Shipping Address</div>
        <div class="login_input">
                <div class="radio_check" id="usernotregister">
                    <input class="customer_type" checked="checked" type="radio" value="1" name="finishOrderForm[sameAsAdrr]" id="finishOrderForm_sameAsAdrr"></div>
                <p id="newCustomerLabel">
                    Same as Address<!--                            <span class="description">(--><!--)</span>-->
                </p>
                <div class="clear"></div>
            </div>
        </div>
        <div class="login_row space_30">
            <div class="login_input">
                <div class="radio_check" id="userisregister">
                    <input value="0" class="customer_type" type="radio" value="1" name="finishOrderForm[sameAsAdrr]" id="finishOrderForm_notsameAddr"></div>
                <p id="existingCustomerLabel">
                    New Address<!--                            <span class="description">(--><!--)</span>-->
                </p>
                <div class="clear"></div>
            </div>
        </div>
        <?php if ($userinfo->regional == "ID") : 
        $region->idprov = $userinfo->state;
        $region->idkab = $userinfo->regency;
        $region->idkec = $userinfo->district;
        $region->idkel =  $userinfo->village;
        ?>
        <div id="indonesia" class="hide">
            <div class="login_t_row">
                <div class="login_t_label">Province:</div>
                <div class="login_t_input">
                    <?php echo selectOption($region->state(),"Select Province", $userinfo->state,"class='input small' required", "state"); ?>
                </div>           
                <div class="clear"></div>
            </div>
            <div class="login_t_row">
                <div class="login_t_label">Regency:</div>
                <div class="login_t_input select_kab">
                    <?php echo selectOption($region->regency(),"Select Regency", $userinfo->regency,"class='input small' required", "regency"); ?>
                </div>           
                <div class="clear"></div>
            </div>
            <div class="login_t_row">
                <div class="login_t_label">District:</div>
                <div class="login_t_input select_kec">
                    <?php echo selectOption($region->district(),"Select District",$userinfo->district,"class='input small' required", "district"); ?>
                </div>           
                <div class="clear"></div>
            </div>
             <div class="login_t_row">
                <div class="login_t_label">Village:</div>
                <div class="login_t_input select_kel">
                    <?php echo selectOption($region->village(),"Select Village",$userinfo->village,"class='input small' required", "village"); ?>
                </div>           
                <div class="clear"></div>
            </div>
        </div>
        <?php endif; ?>
        <div id="email_login_password_row" class="login_t_row">
            <div class="login_t_label"></div>
            <div class="login_t_input">
            <textarea class="login_input_field" cols="35" rows="5" placeholder="address" name="finishOrderForm[address]" id="finishOrderForm_address" disabled=disabled></textarea></div>    
        </div>

        <div class="login_row">
        	<div class="login_t_label">Payment Method</div>
        <div class="login_input">
                <div class="radio_check">
                    <input class="customer_type" checked="checked" type="radio" value="T" name="finishOrderForm[payment]" id="finishOrderForm_transfer"></div>
                <p id="newCustomerLabel">
                    Bank Transfer<!--                            <span class="description">(--><!--)</span>-->
                </p>
                <div class="clear"></div>
            </div>
        </div>
        <div class="login_row space_30">
            <div class="login_input">
                <div class="radio_check">
                    <input value="0" class="customer_type" type="radio" value="C" name="finishOrderForm[payment]" id="finishOrderForm_cod"></div>
                <p id="existingCustomerLabel">
                    COD (Cash On Delivery)<!--                            <span class="description">(--><!--)</span>-->
                </p>
                <div class="clear"></div>
            </div>
        </div>
        <div class="login_t_row">
            <div class="login_t_label">Shpping Price:</div>
            <div class="login_t_input">
            <input class="login_input_field" size="30" name="finishOrderForm[shipping]" placeholder="Free/Waiting for Calculation" id="finishOrderForm_shipping" type="text" value="<?php echo $shipping; ?>" readonly></div>            
            <div class="clear"></div>
        </div>
         <div class="login_t_row">
            <div class="login_t_label">Total</div>
            <div class="login_t_input">
            <input type="hidden" name="finishOrderForm[total]" value="<?php echo $total; ?>" />
            <input class="login_input_field finishOrderForm_total" size="30" name="finishOrderForm[total_bayar]" id="finishOrderForm_total" type="text" value="<?php echo $total+$shipping; ?>" readonly></div>            
            <div class="clear"></div>
        </div>
        <div class="clearfix"></div>
        <div class="btn_form">
            <input type="hidden"  name="finishOrderForm[userid]" value="<?php echo $userinfo->userid; ?>" />
            <input type="submit" name="finishSubmit" value="Complete Order" class="submit-btn"/>
         </div>
        </div>
        <div class="clear space_30"></div>
        </form>
    </div>
    <div class="rightBox">
        <?php include ("layout/widget.php"); ?>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("input[name=finishOrderForm[sameAsAdrr]]:radio").change(function () {
            var hasil = $(this).val();
            if (hasil == "1") {
                $('#finishOrderForm_address:input').attr('disabled', 'disabled');
                $('#indonesia').hide();
            }else{
                $('#finishOrderForm_address:input').removeAttr('disabled');
                $('#indonesia').show();
            }
        });
    });
</script>
<script>
    $(document).ready(function(){
        var loader = '<img src="<?php echo URL;?>images/loader.gif" />';
        $("#regional").select2();
        $("#state").select2();
        $("#regency").select2();
        $("#district").select2();
        $("#village").select2();
        $('#regional').change(function() {
            var hasil = $(this).val();
            if (hasil == "ID") {
                $('#state:input').removeAttr('disabled');
                $('#regency:input').removeAttr('disabled');
                $('#district:input').removeAttr('disabled');
                $('#village:input').removeAttr('disabled');
                $('#indonesia').show('fast');
            }else{
                $('#state:input').attr('disabled', 'disabled');
                $('#regency:input').attr('disabled', 'disabled');
                $('#district:input').attr('disabled', 'disabled');
                $('#village:input').attr('disabled', 'disabled');
                $('#indonesia').hide('fast');
            }
        });
        $('#state').change(function() {
            $('.select_kab').html(loader);
            var id = $(this).val();
            $.post("<?php echo URL; ?>/plugins/cart/ajax.php?kab",{ id: id}, function(data){
            $('.select_kab').html(data);
            $("#regency").select2();
            });
        });
        $('#regency').live("change", function() {
            $('.select_kec').html(loader);
            var id = $(this).val();
            $.post("<?php echo URL; ?>/plugins/cart/ajax.php?kec",{ id: id}, function(data){
             $('.select_kec').html(data);
             $("#district").select2();
            });
             $.post("<?php echo URL; ?>/plugins/cart/ajax.php?shipping",{ id: id}, function(data){
                 $('#finishOrderForm_shipping').val(parseInt(data));
                 $('#finishOrderForm_total').val(parseInt(data)+parseInt(<?php echo $total;?>));
            });
        });
        $('#district').live("change", function() {
            $('.select_kel').html(loader);
            var id = $(this).val();
            $.post("<?php echo URL; ?>/plugins/cart/ajax.php?kel",{ id: id}, function(data){
             $('.select_kel').html(data);
             $("#village").select2();
            });
        });
    });
</script>
<?php 
endif;
