<div class="topbox">
	<?PHP include ("widget.php"); ?>
</div>
<div class="women">
	<ul class="portfolio-filter w_nav">
		<li>Sort : </li>
		<li><a class="active" href="#">popular</a></li>
		<li><a href="#">new </a></li>
		<li><a href="#">discount</a></li>
		<li><a href="#">price: Low High </a></li> 
		<div class="clear"></div>	
     </ul>
     <div class="clearfix"></div>	
</div>
<div class="contentBox">
	<ul class="item">
		<?PHP
		$item = array();
		foreach ($dataItem as $item)
		{
			$prod = $item->href;
			$itemLib->itemid = $item->itemid;
			$thumbnail = URL.'thumb?src='.URL.$itemLib->getLastImage().'&w=159&h=212&a=t';	
			$diskon = $itemLib->diskon();
			$old_price = $item->price;
			$new_price = $old_price-$diskon;
			echo "<li>";
			echo "<img src=\"".$thumbnail."\" align=\"center\"/>";
			echo "<h4>".$item->name."</h4>";
			echo "<p class=\"short_desc\">".short_view($item->short_description,45)."</p>";
			if ($diskon !=0) :
				echo '<span class="price-access old_price clearfix">'.$itemLib->number($old_price,"IDR").'</span>';
			else :
				echo '<br />';
			endif;
			echo "<p class=\"price\"> ".$itemLib->number($new_price,"IDR")."</p>";
			echo "<p>
			 <a class=\"prod_cart\" href=\"".URL.PAGEHREF."/buy/".$item->itemid."/".$prod."\">Buy</a>            
			 <a href=\"".URL.PAGEHREF."/detail/".$item->itemid."/".$prod."\" class=\"prod_details\">Detail</a>
			 <p>";
			echo "</li>";
		}
		?>
	</ul>
	<div class="clearfix"></div>
	<?PHP
		if (isset($totpage) && $totpage >1) :
			echo '<div class="pages"><ul class="page-list">';
			echo pagging($pg,  $totpage, $link);
			echo '</ul></div>';
		endif;
		?>
	</div>
	<div class="clearfix"></div>
</div>