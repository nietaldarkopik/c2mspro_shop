<?php
	if(isset($_REQUEST['command'])) :
		if($_REQUEST['command']=='buy'){
			$id = $itemDetail->itemid;
			$q = ($_REQUEST['quantity']<1) ? 1 : intval($_REQUEST['quantity']);
			$cart->addtocart($id,$q);
			$back = $_SERVER['HTTP_REFERER'];
			alert("Item Add", $back);
		}
	endif;

?>
<script language="javascript">
	function add(){
		document.form1.command.value='buy';
		document.form1.submit();
	}
</script>
<link rel="stylesheet" href="<?php echo URL.'plugins/cart/etalage.css';?>">
<script src="<?php echo URL.'plugins/cart/jquery.etalage.min.js';?>"></script>
<script>
	jQuery(document).ready(function($){

		$('#etalage').etalage({
			thumb_image_width: 300,
			thumb_image_height: 400,
			source_image_width: 900,
			source_image_height: 1200,
			show_hint: true,
			click_callback: function(image_anchor, instance_id){
				alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
			}
		});

	});
</script>
<div class="col-md-9">
	  <div class="single_left">
		<div class="grid images_3_of_2">
			<ul id="etalage">
			<?php
			$img = $itemLib->getImage();
			foreach ($img as $img) {
			//$images = URL.'thumb?src='.URL."images/cart/".$itemid."/".$img.'&w=900&h=1200&a=t';	
			$images = URL."images/cart/".$itemid."/".$img;
			echo '<li>
					<img class="etalage_thumb_image" src="'.$images.'" class="img-responsive" />
					<img class="etalage_source_image" src="'.$images.'" class="img-responsive" title="" />
				</li>';
			}
			?>
			</ul>
			 <div class="clearfix"></div>		
	  </div>
	  <div class="desc1 span_3_of_2">
		<h3><?php echo $itemDetail->name; ?></h3>
		<p>
			<?php if ($diskon != 0) echo '<span class="old_price">'.$itemLib->number($old_price,"IDR").'</span>'; ?>
			<span class="new_price"><?php echo $itemLib->number($new_price,"IDR"); ?></span>
		</p>
		<div class="det_nav">
			<h4>New Product :</h4>
			<ul>
				<?php
				$itemLib->limit = " LIMIT 3";
				$itemLib->where = " WHERE A.itemid !='$itemDetail->itemid'";
				$style = $itemLib->produk();
				foreach ($style as $style) :
					$itemLib->itemid = $style->itemid;
					$thumbnail = URL.'thumb?src='.URL.$itemLib->getLastImage().'&w=173&h=225&a=t';	
					echo '<li><a href="'.URL.PAGEHREF."/detail/".$style->itemid."/".$style->href.'"><img src="'.$thumbnail.'" class="img-responsive" alt=""/></a></li>';
				endforeach;
				?>
			</ul>
		</div>
		<div class="rightBox order">
			<?php include ("paycheck.php"); ?>
		</div>
		<div class="clearfix"></div>
		<form name="form1" method="post">
			<input type="hidden" name="command" />
			<div class="btn_form">
				<input type="text" name="quantity" value="1" maxlength="3" size="2" class="quantity">
				<a href="#" onclick='javascript:add()'>buy</a> &nbsp;
				<a href="<?php echo URL.PAGEHREF."/troll"; ?>">Checkout</a> &nbsp;
				<a href="<?php echo URL.PAGEHREF."/clear"; ?>">Clear</a>
			</div>
		</form>
		<div class="clearfix"></div>		
   	 </div>
	    <div class="clearfix"></div>
	   </div>
	    <div class="single-bottom1">
		<h6>Details</h6>
		<p class="prod-desc"><?php echo $itemDetail->description; ?></div>
	<div class="single-bottom2">
		<h6>Related Products</h6>
		<?php
		$itemLib->limit = " LIMIT 3";
		$itemLib->where = " WHERE A.categories = '$itemDetail->categories' AND A.itemid !='$itemDetail->itemid'";
		$related = $itemLib->produk();

		foreach ($related as $related) :
		$itemLib->itemid = $related->itemid;
		$thumbnail = URL.'thumb?src='.URL.$itemLib->getLastImage().'&w=173&h=225&a=t';	
		$diskon = $itemLib->diskon();
		$old_price = $related->price;
		$new_price = $old_price-$diskon;
		$linkto = URL.PAGEHREF."/detail/".$related->itemid."/".$related->href;
		$linkbuy = URL.PAGEHREF."/buy/".$related->itemid."/".$related->href;
		echo '
		<div class="product">
			<div class="product-desc">
				<div class="product-img">
                   <a class="product_link" href="'.$linkto.'">
                   <img src="'.$thumbnail.'" class="img-responsive " alt=""/>
                   </a>
               </div>
               <div class="prod1-desc">
                   <h5><a class="product_link" href="'.$linkto.'">'.$related->name.'</a></h5>
                   <p class="product_descr"> '.short_view($related->description,100).'</p>									
			   </div>
			  <div class="clearfix"></div>
	      	</div>
	      	<div class="product_price">';
	      	if ($diskon !=0) echo '<span class="price-access old_price clearfix">'.$itemLib->number($old_price,"IDR").'</span>';
			echo '<span class="price-access new_price clearfix">'.$itemLib->number($new_price,"IDR").'</span><br />								
				<a class="button1" href="'.$linkbuy.'"><span>Add to cart</span></a>
          	</div>
		 <div class="clearfix"></div>
	     </div>';
		endforeach;
		?>
	  </div>
</div>	