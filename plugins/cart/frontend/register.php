<div class="contentBox clearfix">
    <div class="clearfix_X leftBox push_1 login_wrap">
        <form id="registerForm" name="registerForm" method="post" enctype="multipart/form-data">
        <?PHP if ($msg != "") : ?>
            <div class="pesan login_row"><?PHP echo $msg; ?></div>
        <?PHP endif; ?>
        <div class="login_t_row">

            <div class="login_t_label">Your Email:</div>
            <div class="login_t_input">
    		<input class="login_input_field" size="30" placeholder="Email" name="EmailLoginForm[email]" id="EmailLoginForm_email" type="text" value="<?PHP echo $userinfo->email; ?>" required></div>            
            <div class="clear"></div>

        </div>
        <div class="login_t_row">

            <div class="login_t_label">Your Password:</div>
            <div class="login_t_input">
            <input class="login_input_field" size="30" placeholder="Password" name="EmailLoginForm[password]" id="EmailLoginForm_password" type="password" value=""/></div>   
            
            <div class="clear"></div>

        </div>
        <div class="login_t_row">

            <div class="login_t_label">Retype Password:</div>
            <div class="login_t_input">
            <input class="login_input_field" size="30" placeholder="Password" name="EmailLoginForm[repassword]" id="EmailLoginForm_repassword" type="password" value=""/></div>   
            
            <div class="clear"></div>

        </div>
         <div class="login_t_row">

            <div class="login_t_label">Your Name:</div>
            <div class="login_t_input">
            <input class="login_input_field" size="30" placeholder="Name" name="EmailLoginForm[name]" id="EmailLoginForm_name" type="text" value="<?PHP echo $userinfo->name; ?>" required ></div>            
            <div class="clear"></div>

        </div>
        <div class="login_t_row">

            <div class="login_t_label">Your Phone:</div>
            <div class="login_t_input">
            <input class="login_input_field" size="30" placeholder="Phone" name="EmailLoginForm[Phone]" id="EmailLoginForm_Phone" type="text" value="<?PHP echo $userinfo->phone; ?>" required></div>            
            <div class="clear"></div>

        </div>
        <div class="login_t_row">
            <div class="login_t_label">Regional:</div>
            <div class="login_t_input">
               <?PHP echo selectOption($region->regional(),"Select Regional",$userinfo->regional,"class='input small' required", "regional"); ?>
            </div>      
            <div class="clear"></div>
        </div>
        <?PHP if ($userinfo->regional == "ID") : ?>
        <div id="indonesia">
            <div class="login_t_row">
                <div class="login_t_label">Province:</div>
                <div class="login_t_input">
                    <?PHP echo selectOption($region->state(),"Select Province", $userinfo->state,"class='input small' required", "state"); ?>
                </div>           
                <div class="clear"></div>
            </div>
            <div class="login_t_row">
                <div class="login_t_label">Regency:</div>
                <div class="login_t_input select_kab">
                    <?PHP echo selectOption($region->regency(),"Select Regency", $userinfo->regency,"class='input small' required", "regency"); ?>
                </div>           
                <div class="clear"></div>
            </div>
            <div class="login_t_row">
                <div class="login_t_label">District:</div>
                <div class="login_t_input select_kec">
                    <?PHP echo selectOption($region->district(),"Select District",$userinfo->district,"class='input small' required", "district"); ?>
                </div>           
                <div class="clear"></div>
            </div>
             <div class="login_t_row">
                <div class="login_t_label">Village:</div>
                <div class="login_t_input select_kel">
                    <?PHP echo selectOption($region->village(),"Select Village",$userinfo->village,"class='input small' required", "village"); ?>
                </div>           
                <div class="clear"></div>
            </div>
        </div>
        <?PHP endif; ?>
        <div class="login_t_row">
            <div class="login_t_label">Your Address:</div>
            <div class="login_t_input">
            <textarea class="login_input_field" cols="35" rows="5" placeholder="address" name="EmailLoginForm[address]" id="EmailLoginForm_address" required></textarea></div>            
            <div class="clear"></div>
        </div>
        <div class="login_t_row">
            <div class="login_t_label">Identity Number</div>
            <div class="login_t_input">
             <input class="login_input_field" size="30" placeholder="Card Number" name="EmailLoginForm[number]" id="EmailLoginForm_number" type="text" value="" required></div>           
            <div class="clear"></div>
        </div>
        <div class="clearfix"></div>
        <div class="btn_form">
            <input type="hidden"  name="EmailLoginForm[userid]" value="<?PHP echo $userinfo->userid; ?>" />
            <input type="submit" name="submit" value="Continue" class="submit-btn"/>
         </div>
        </div>
        <div class="clear space_30"></div>
        </form>
    </div>
    <div class="rightBox">
        <?php include ("layout/widget.php"); ?>
    </div>
    <div class="clearfix"></div>
</div>
    <script>
    $(document).ready(function(){
        var loader = '<img src="<?PHP echo URL;?>images/loader.gif" />';
        $("#regional").select2();
        $("#state").select2();
        $("#regency").select2();
        $("#district").select2();
        $("#village").select2();
        $('#regional').change(function() {
            var hasil = $(this).val();
            if (hasil == "ID") {
                $('#state:input').removeAttr('disabled');
                $('#regency:input').removeAttr('disabled');
                $('#district:input').removeAttr('disabled');
                $('#village:input').removeAttr('disabled');
                $('#indonesia').show('fast');
            }else{
                $('#state:input').attr('disabled', 'disabled');
                $('#regency:input').attr('disabled', 'disabled');
                $('#district:input').attr('disabled', 'disabled');
                $('#village:input').attr('disabled', 'disabled');
                $('#indonesia').hide('fast');
            }
        });
        $('#state').change(function() {
            $('.select_kab').html(loader);
            var id = $(this).val();
            $.post("<?PHP echo URL; ?>/plugins/cart/ajax.php?kab",{ id: id}, function(data){
            $('.select_kab').html(data);
            $("#regency").select2();
            });
        });
        $('#regency').live("change", function() {
            $('.select_kec').html(loader);
            var id = $(this).val();
            $.post("<?PHP echo URL; ?>/plugins/cart/ajax.php?kec",{ id: id}, function(data){
             $('.select_kec').html(data);
             $("#district").select2();
            });
        });
        $('#district').live("change", function() {
            $('.select_kel').html(loader);
            var id = $(this).val();
            $.post("<?PHP echo URL; ?>/plugins/cart/ajax.php?kel",{ id: id}, function(data){
             $('.select_kel').html(data);
             $("#village").select2();
            });
        });
    });
</script>
