<?PHP
if(!isset($_SESSION['cart'])) :
    echo "There are no items in your shopping cart!";
else:
?>
<div class="contentBox clearfix">
    <div class="clearfix_X leftBox push_1 login_wrap">
        <form id="registerForm" name="registerForm" method="post" enctype="multipart/form-data">
          <?PHP if ($msg != "") : ?>
          <div class="pesan login_row"><?PHP echo $msg; ?></div>
          <?PHP endif; ?>
          <div class="login_row">
              <div class="login_t_label alignleft">&nbsp;</div>
              <div class="alignleft">
                  <div class="radio_check" id="usernotregister">
                    <input class="customer_type" checked="checked" type="radio" value="1" name="EmailLoginForm[isGuestCheckout]" id="EmailLoginForm_isGuestCheckout">
                  </div>
                  <p id="newCustomerLabel">
                     New Member
                     <!--
                      <span class="description"></span>
                     -->
                  </p>
                  <div class="clear"></div>
                  <div class="radio_check" id="userisregister">
                    <input value="0" class="customer_type" type="radio" value="1" name="EmailLoginForm[isGuestCheckout]" id="EmailLoginForm_isGuestCheckout">
                  </div>
                  <p id="existingCustomerLabel">
                      Already Member
                      <!--
                        <span class="description"></span>
                      -->
                  </p>
                  <div class="clear"></div>
              </div>
          </div>
          <div class="clear"></div>
          <div class="login_t_row">
              <div class="login_t_label">Masukkan email Anda:</div>
              <!-- The "valid" class will be disable in normal condition and will become "invalid" when user input error -->
              <div class="login_t_input">
              <input class="login_input_field" size="30" placeholder="Email" name="EmailLoginForm[email]" id="EmailLoginForm_email" type="text" value="" required></div>
              <!-- The "valid" class will be disable in normal condition and will become "invalid" when user input error -->
              <div class="clear"></div>
          </div>
          <div id="email_login_password_row" class="login_t_row">
              <div class="login_t_label"></div>
              <div class="login_t_input">
                 <input class="login_input_field" size="30" placeholder="Password" name="EmailLoginForm[password]" id="EmailLoginForm_password" type="password" value="" disabled required> </div>
          </div>
          <span id="lostpassword" class="lostpassword">
              <a class="lostpassword_link" href="/customer/account/forgotpassword/" target="_blank">Lupa kata sandi?</a>
          </span>
          <div class="clearfix"></div>
          <div class="btn_form">
              <input type="submit" value="Continue" class="submit-btn"/>
              <div class="unit or-divider">
                  <div class="inner">
                      <div class="loginReg__or">Or</div>
                  </div>
              </div>
              <input type="hidden" name="login" />
              <a href="javascript:;" onclick="trackFacebookActionCheckout(325000); loginByFacebookAccount(this, '/customer/account/facebookReauthen?redirect=/produk/checkout'); return false;">
              <span>Sign in with Facebook</span>
              </a>
           </div>
        </div>
        <div class="clear space_30"></div>
        </form>
    </div>
    <div class="rightBox">
        <?php include ("layout/widget.php"); ?>
    </div>
    <div class="clearfix"></div>
</div>

<script>
    $(document).ready(function(){
        function showHide()
        {
          var hasil = $("input.customer_type:checked").val();
          if (hasil == "1") {
              $('#EmailLoginForm_password:input').attr('disabled', 'disabled');
              $('#email_login_password_row').fadeOut();
              $('#lostpassword').fadeOut();
          }else{
              $('#EmailLoginForm_password:input').removeAttr('disabled');
              $('#email_login_password_row').fadeIn();
              $('#lostpassword').fadeIn();
          }
        }
        $("input.customer_type").click(function () {
            showHide();
        });
        showHide();
    });
</script>
<?PHP 
endif;
