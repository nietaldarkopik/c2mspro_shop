<?php
	if(isset($_REQUEST['command'])) :
		if($_REQUEST['command']=='delete' && $_REQUEST['pid']>0){
			$cart->remove_product($_REQUEST['pid']);
		}
		else if($_REQUEST['command']=='clear'){
			unset($_SESSION['cart']);
		}
		else if($_REQUEST['command']=='update'){
			$max=count($_SESSION['cart']);
			for($i=0;$i<$max;$i++){
				$pid=$_SESSION['cart'][$i]['itemid'];
				$q=intval($_REQUEST['product'.$pid]);
				if($q>0 && $q<=999){
					$_SESSION['cart'][$i]['qty']=$q;
				}
				else{
					$msg='Some products not updated!, quantity must be a number between 1 and 999';
				}
			}
		}
	endif;

?>
<script language="javascript">
	function del(pid){
		if(confirm('Do you really mean to delete this item')){
			document.form1.pid.value=pid;
			document.form1.command.value='delete';
			document.form1.submit();
		}
	}
	function clear_cart(){
		if(confirm('This will empty your shopping cart, continue?')){
			document.form1.command.value='clear';
			document.form1.submit();
		}
	}
	function update_cart(){
		document.form1.command.value='update';
		document.form1.submit();
	}
</script>
<form name="form1" method="post">
<input type="hidden" name="pid" />
<input type="hidden" name="command" />
	<div style="margin:0px auto;" class="checkout">
    <div style="padding-bottom:10px">
    </div>
    	<div style="color:#F00"><?php echo isset($msg) ? $msg : ""; ?></div>
    	<table border="0" cellpadding="5px" cellspacing="1px" width="100%">
    	<?php
			if(isset($_SESSION['cart']) && is_array($_SESSION['cart'])){
            	echo '<tr><th>Serial</th><th>Name</th><th>Price</th><th>Qty</th><th>Amount</th><th>Options</th></tr>';
				$max=count($_SESSION['cart']);
				$total = 0;
				for($i=0;$i<$max;$i++){
					$itemid=$_SESSION['cart'][$i]['itemid'];
					$q=$_SESSION['cart'][$i]['qty'];
					$itemLib->itemid = $itemid;
					$item = $itemLib->getItem();
					$price = $item->price-$itemLib->diskon();
					$subtotal = $q*$price;
					$total = $total+$subtotal;
					if($q==0) continue;
			?>
            		<tr style="<?php echo $i%2==0 ? 'background-color:#FFFFFF;' : ''; ?>"><td><?php echo $i+1?></td>
					<td><?php echo $item->name;?></td>
                    <td><?php echo $itemLib->number($price,"IDR");?></td>
                    <td><input type="text" name="product<?php echo $itemid?>" value="<?php echo $q?>" maxlength="3" size="2" /></td>                    
                    <td><?php echo $itemLib->number($subtotal,"IDR");?></td>
                    <td><a href="javascript:del(<?php echo $itemid?>)">Remove</a></td></tr>
            <?php					
				}
			?>
				<tr>
				<td colspan="2">
				<b>Order Total: <?php echo $itemLib->number($total,"IDR");?></b></td>
				<td colspan="4" align="right">
					<a class="button default" onclick="window.location='./'">Continue</a>
					<a class="button default" onclick="clear_cart()">Clear</a>
					<a class="button default" onclick="update_cart()">Update</a>
					<a class="button default" href="<?php echo URL.PAGEHREF."/checkout"; ?>">Checkout</a>
				</td>
				</tr>
			<?php
            }
			else{
				echo "<tr bgColor='#FFFFFF'><td>There are no items in your shopping cart!</td>";
			}
		?>
        </table>
    </div>
</form>