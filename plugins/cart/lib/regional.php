<?PHP
class regional{
	public $idregion, $idprov, $idkab, $idkec, $idkel;
	private $mysqli;
	function __construct(mysqli $conn)
    {
        $this->mysqli = $conn;
    }
    function getRegional() {
    	$query = "SELECT * FROM countries WHERE countryCode='$this->idregion'";
    	$query = $this->mysqli->query($query);
    	$data = $query->fetch_object();
    	return $data;
    }
    function regional() {
    	$query = "SELECT countryCode, countryName FROM countries";
    	$query = $this->mysqli->query($query);
    	while($data = $query->fetch_object()) {
    		$option[$data->countryCode] = $data->countryName;
    	}
    	return $option;
    }
     function getState() {
    	$query = "SELECT * FROM provinsi WHERE id_prov='$this->idprov'";
    	$query = $this->mysqli->query($query);
    	$data = $query->fetch_object();
    	return $data;
    }
	function state() {
		$query = "SELECT id_prov, nama FROM provinsi";
    	$query = $this->mysqli->query($query);
    	while($data = $query->fetch_object()) {
    		$option[$data->id_prov] = $data->nama;
    	}
    	return $option;
	}
	function getRegency() {
    	$query = "SELECT * FROM kabupaten WHERE id_kab='$this->idkab'";
    	$query = $this->mysqli->query($query);
    	$data = $query->fetch_object();
    	return $data;
    }
	function regency() {
		$query = "SELECT id_kab, nama FROM kabupaten WHERE id_prov='$this->idprov'";
    	$query = $this->mysqli->query($query);
    	$option = array();
    	while($data = $query->fetch_object()) {
    		$option[$data->id_kab] = $data->nama;
    	}
    	return $option;
	}
	function getDistrict() {
    	$query = "SELECT * FROM kecamatan WHERE id_kec='$this->idkec'";
    	$query = $this->mysqli->query($query);
    	$data = $query->fetch_object();
    	return $data;
    }
	function district() {
		$query = "SELECT id_kec, nama FROM kecamatan WHERE id_kab='$this->idkab'";
    	$query = $this->mysqli->query($query);
    	$option = array();
    	while($data = $query->fetch_object()) {
    		$option[$data->id_kec] = $data->nama;
    	}
    	return $option;
	}
	function getVillage() {
    	$query = "SELECT * FROM kelurahan WHERE id_kel='$this->idkel'";
    	$query = $this->mysqli->query($query);
    	$data = $query->fetch_object();
    	return $data;
    }
	function village() {
		$query = "SELECT id_kel, nama FROM kelurahan WHERE id_kec='$this->idkec'";
    	$query = $this->mysqli->query($query);
    	$option = array();
    	while($data = $query->fetch_object()) {
    		$option[$data->id_kel] = $data->nama;
    	}
    	return $option;
	}
}