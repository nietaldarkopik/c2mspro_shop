<?PHP
class item {
	public 	$data= null,
			$itemid = null,
			$catid = null,
			$diskonid = null,
			$diskon = 1, //1 atau 2
			$where = null,
			$order = null,
			$limit = null,
			$rekid = null,
			$dbUpload = '../../images/cart';
	private $tabelDB = "plugin_cart_item",
			$mysqli;
	
	function __construct(mysqli $conn)
    {
        $this->mysqli = $conn;
    } 
	function cekFolder() {
		$dirBlog = $this->dbUpload;
		is_dir($dirBlog) ? $dirBlog = $dirBlog : mkdir($dirBlog,0777);
	}
	function produk() {
		$item = array();
		$q = "SELECT A.*, B.name as cate FROM plugin_cart_item A INNER JOIN plugin_cart_categories B ON B.catid=A.categories $this->where ".$this->order." $this->limit";
		$query = $this->mysqli->query($q);
		while ($data = $query->fetch_object()) {
			$item[] = $data;
		}
		return $item;
	}
	function payment() {
		$item = array();
		$q = "SELECT * FROM plugin_cart_rekening";
		$query = $this->mysqli->query($q);
		while ($data = $query->fetch_object()) {
			$item[] = $data;
		}
		return $item;
	}
	function totalpayment() {
		$query = $this->mysqli->query("SELECT COUNT(rekid) as jumlah FROM plugin_cart_rekening");
		$data = $query->fetch_object();
		return $data->jumlah;
	}
	function paymentSelect() {
		$item = array();
		$q = "SELECT * FROM plugin_cart_rekening";
		$query = $this->mysqli->query($q);
		while ($data = $query->fetch_object()) {
			$item[$data->rekid] = $data->rekBank." a.n ".$data->rekName;
		}
		return $item;
	}
	function getPayment()
	{
		$query = $this->mysqli->query("SELECT * FROM plugin_cart_rekening WHERE rekid='$this->rekid' $this->where");
		$data = $query->fetch_object();
		return $data;
	}
	function addPayment() {
		foreach ($this->data as $key=>$value) {
			$insertData[] = "$key = '$value'";
		}
		$insertData = implode(',',$insertData);
		if (isset($insertData)) $this->mysqli->query("INSERT INTO plugin_cart_rekening SET $insertData");
	}
	function editPayment() {
		foreach ($this->data as $key=>$value) {
			$insertData[] = "$key = '$value'";
		}
		$insertData = implode(',',$insertData);
		if (isset($insertData)) $this->mysqli->query("UPDATE plugin_cart_rekening SET $insertData WHERE rekid='$this->rekid'");
	}
	function total() {
		$query = $this->mysqli->query("SELECT COUNT(A.itemid) as jumlah FROM plugin_cart_item A INNER JOIN plugin_cart_categories B ON B.catid=A.categories $this->where");
		$data = $query->fetch_object();
		return $data->jumlah;
	}
	function getItem() {
		$query = $this->mysqli->query("SELECT * FROM plugin_cart_item WHERE itemid='$this->itemid' $this->where");
		$data = $query->fetch_object();
		return $data;
	}
	function getLastImage(){
		$img = loadMaxDir($this->dbUpload.'/'.$this->itemid);
		return $this->dbUpload.'/'.$this->itemid.'/'.$img;
	}
	function getImage(){
		$img = loadDir($this->dbUpload.'/'.$this->itemid);
		return $img;
	}
	function delIPayment() {
		$query = $this->mysqli->query("DELETE FROM plugin_cart_rekening WHERE rekid='$this->rekid'");
	}
	function delItem() {
		$query = $this->mysqli->query("DELETE FROM plugin_cart_item WHERE itemid='$this->itemid' $this->where");
		rrmdir($this->dbUpload.'/'.$this->itemid);
	}
	function addItem($thumb) {
		foreach ($this->data as $key=>$value) {
			$insertData[] = "$key = '$value'";
		}
		$insertData = implode(',',$insertData);
		if (isset($insertData)) {
			$this->cekFolder();
			$this->mysqli->query("INSERT INTO plugin_cart_item SET $insertData"); 
			$dirBlog = $this->dbUpload.'/'.$this->mysqli->insert_id;
			for($i=0; $i<count($thumb["name"]); $i++) {
				$tmp_name = $thumb["tmp_name"][$i];
				if ($tmp_name !="") {
					is_dir($dirBlog) ? $dirBlog = $dirBlog : mkdir($dirBlog,0777);
					$thumbNail = $thumb["name"][$i];
					move_uploaded_file($tmp_name, "$dirBlog/$thumbNail");	
				}
			}
		}
	}
	function editItem($thumb) {
		foreach ($this->data as $key=>$value) {
			$insertData[] = "$key = '$value'";
		}
		$insertData = implode(',',$insertData);
		if (isset($insertData)) {
			$dirBlog = $this->dbUpload.'/'.$this->itemid;
			for($i=0; $i<count($thumb["name"]); $i++) {
				$tmp_name = $thumb["tmp_name"][$i];
				if ($tmp_name !="") {
					is_dir($dirBlog) ? $dirBlog = $dirBlog : mkdir($dirBlog,0777);
					$thumbNail = $thumb["name"][$i];
					move_uploaded_file($tmp_name, "$dirBlog/$thumbNail");	
				}
			}
			$query = "UPDATE plugin_cart_item SET $insertData WHERE itemid='$this->itemid' $this->where";
			$this->mysqli->query($query);		
		}
	}
	function diskon() {
		$query = $this->mysqli->query("SELECT A.*, B.price FROM plugin_cart_diskon A LEFT JOIN plugin_cart_item B on B.itemid = A.itemid WHERE A.itemid='$this->itemid' AND (A.categories='1' AND NOW() BETWEEN A.opentime AND A.untiltime) OR A.categories='2'");
		$potongan = 0;
		while ($data = $query->fetch_object()) {
			if ($data->type == 1) $diskon = $data->diskon*$data->price/100;
			else  $diskon = $data->diskon;
			$potongan = $potongan+$diskon;
		}
		return $potongan;
	}
	//Categories
	function categories(){
		$cat = array();
		$query = $this->mysqli->query("SELECT * FROM plugin_cart_categories $this->where");
		while ($data = $query->fetch_object()) {
			$cat[] = $data;
		}
		return $cat;
	}
	function total_cat() {
		$query = $this->mysqli->query("SELECT COUNT(catid) as jumlah FROM plugin_cart_categories $this->where");
		$data = $query->fetch_object();
		return $data->jumlah;
	}
	function getCat() {
		$query = $this->mysqli->query("SELECT * FROM plugin_cart_categories WHERE catid='$this->catid' $this->where");
		$data = $query->fetch_object();
		return $data;
	}
	function delCat() {
		$cek = $this->mysqli->query("SELECT * FROM plugin_cart_item WHERE categories='$this->catid'");
		if ($cek->num_rows > 0) { echo "<center><span style='color:red'>Delete product on this category first</span></center>"; }
		else { 
			$get = $this->getCat();
			@unlink($dbUpload.$get->thumb);
			$query = $this->mysqli->query("DELETE FROM plugin_cart_categories WHERE catid='$this->catid' $this->where"); 
		}
	}
	function addCat() {
		foreach ($this->data as $key=>$value) {
			$insertData[] = "$key = '$value'";
		}
		$insertData = implode(',',$insertData);
		if (isset($insertData)) $this->mysqli->query("INSERT INTO plugin_cart_categories SET $insertData");
	}
	function editCat() {
		foreach ($this->data as $key=>$value) {
			$insertData[] = "$key = '$value'";
		}
		$insertData = implode(',',$insertData);
		if (isset($insertData)) $this->mysqli->query("UPDATE plugin_cart_categories SET $insertData WHERE catid='$this->catid' $this->where");
	}
	//Diskon 
	function lisDiskon() {
		$cat = array();
		$query = $this->mysqli->query("SELECT A.*, B.name FROM plugin_cart_diskon A LEFT JOIN plugin_cart_item B on A.itemid=B.itemid $this->where");
		while ($data = $query->fetch_object()) {
			$cat[] = $data;
		}
		return $cat;
	}
	function totaldiskon() {
		$query = $this->mysqli->query("SELECT COUNT(diskonid) as jumlah FROM plugin_cart_diskon $this->where");
		$data = $query->fetch_object();
		return $data->jumlah;
	}
	function getDiskon() {
		$query = $this->mysqli->query("SELECT A.*, B.name FROM plugin_cart_diskon A LEFT JOIN plugin_cart_item B on A.itemid=B.itemid WHERE A.diskonid='$this->diskonid' $this->where");
		$data = $query->fetch_object();
		return $data;
	}
	function delDiskon() {
		$query = $this->mysqli->query("DELETE FROM plugin_cart_diskon WHERE diskonid='$this->diskonid' $this->where"); 
	}
	function addDiskon() {
		foreach ($this->data as $key=>$value) {
			$insertData[] = "$key = '$value'";
		}
		$insertData = implode(',',$insertData);
		if (isset($insertData)) $this->mysqli->query("INSERT INTO plugin_cart_diskon SET $insertData");
	}
	function editDiskon() {
		foreach ($this->data as $key=>$value) {
			$insertData[] = "$key = '$value'";
		}
		$insertData = implode(',',$insertData);
		if (isset($insertData)) $this->mysqli->query("UPDATE plugin_cart_diskon SET $insertData $this->where");
	}
	function hal() {
		$query = $this->mysqli->query("SELECT href FROM pages WHERE page_type='plugin' AND content='cart' LIMIT 1");
		$data = $query->fetch_object();
		return $data->href;
	}
	function number($data, $tax="IDR"){
		if ($data == "0") $value =  "0.00"; else if (!is_numeric($data)) $value =  $data;  else $value =  number_format($data , 2 , '.' , ','	 ); 
		if ($tax == "IDR") : return "Rp ".$value; 
		else : return "$ ".$value; endif; 
	}
}

?>