<?PHP
class shipping{
	public $id_kab, $value, $data="", $where="";
	private $mysqli;

	function __construct(mysqli $conn) {
        $this->mysqli = $conn;
    }

    function total () {
        $query = $this->mysqli->query("SELECT COUNT(id_kab) as jumlah FROM plugin_cart_shipping $this->where");
        $data = $query->fetch_object();
        return $data->jumlah;
    }

    function listData () {
        $data = array();
        $query = "SELECT A.*, B.nama as city, C.nama as province FROM plugin_cart_shipping A INNER JOIN kabupaten B ON B.id_kab=A.id_kab INNER JOIN  provinsi C ON C.id_prov = B.id_prov $this->where";
        $query = $this->mysqli->query($query);
        while($get = $query->fetch_object()) {
            $data[] = $get;
        }
        return $data;
    }

    function add() {

        $this->mysqli->query("INSERT INTO plugin_cart_shipping SET id_kab = '".$this->data["id_kab"]."', value= '".$this->data["value"]."' ");
    }
     function edit() {

        $this->mysqli->query("REPLACE INTO plugin_cart_shipping SET id_kab = '".$this->data["id_kab"]."', value= '".$this->data["value"]."' ");
    }

    function getShip () {
        $query = $this->mysqli->query("SELECT A.*, B.id_prov, B.nama as city, C.nama as province FROM plugin_cart_shipping A INNER JOIN kabupaten B ON B.id_kab=A.id_kab INNER JOIN  provinsi C ON C.id_prov = B.id_prov WHERE A.id_kab='$this->id_kab' ");
        $data = $query->fetch_object();
        return $data;
    }

    function delete() {
        $query = $this->mysqli->query("DELETE FROM plugin_cart_shipping WHERE id_kab='$this->id_kab' ");
    }
}
