<?PHP
class cart{
	private $mysqli;
	function __construct(mysqli $conn)
    {
        $this->mysqli = $conn;
    }
	function remove_product($pid){
		$pid=intval($pid);
		$max=count($_SESSION['cart']);
		for($i=0;$i<$max;$i++){
			if($pid==$_SESSION['cart'][$i]['itemid']){
				unset($_SESSION['cart'][$i]);
				break;
			}
		}
		$_SESSION['cart']=array_values($_SESSION['cart']);
	}
	function addtocart($pid,$q){
		if($pid<1 or $q<1) return;		
		if(!empty($_SESSION['cart']) && is_array($_SESSION['cart'])){
			if($this->product_exists($pid)) return;
			$max=count($_SESSION['cart']);
			$_SESSION['cart'][$max]['itemid']=$pid;
			$_SESSION['cart'][$max]['qty']=$q;
		}
		else{
			$_SESSION['cart']=array();
			$_SESSION['cart'][0]['itemid']=$pid;
			$_SESSION['cart'][0]['qty']=$q;
		}
	}
	function product_exists($pid){
		$pid=intval($pid);
		$max=count($_SESSION['cart']);
		$flag=0;
		if ($max > 0) :
			for($i=0;$i<$max;$i++) :
				if($pid==$_SESSION['cart'][$i]['itemid']):
					$flag=1;
					break;
				endif;
			endfor;
		endif;
		return $flag;
	}
	function setting() {
		$get = array();
		$query = $this->mysqli->query("SELECT * FROM plugin_cart_settings");
		while ($data = $query->fetch_object()) {
			$get[$data->setting_name] = $data->setting_value;
		}
		return $get;
	}
	function updateStting($data) {
		if (is_array($data)) :
			foreach($data as $key=>$value){
				$this->mysqli->query("UPDATE plugin_cart_settings SET setting_value='$value' WHERE setting_name='$key' ");
			}			
		endif;
	}
}

?>