<?PHP session_start();
include '../../configs/vars.php';
include '../../configs/database.php';
include '../../libraries/dbconnect.php'; 
include '../../libraries/settings.php';
include '../../libraries/url.php';
include '../../libraries/libraries.php';
include 'lib/regional.php';
$region = new regional($mysqli);
if (isset($_GET["kab"]) && isset($_POST["id"])) :
	$region->idprov = $_POST["id"];
	echo selectOption($region->regency(),"Select Regional","","class='input small' required", "regency");
elseif (isset($_GET["kec"]) && isset($_POST["id"])) :
	$region->idkab = $_POST["id"];
	echo selectOption($region->district(),"Select District","","class='input small' required", "district");
elseif (isset($_GET["kel"]) && isset($_POST["id"])) :
	$region->idkec = $_POST["id"];
	echo selectOption($region->village(),"Select Village","","class='input small' required", "village");
elseif (isset($_GET["shipping"]) && isset($_POST["id"])) :
	include 'lib/cart.php';
	include 'lib/shipping.php';
	$cart = new cart($mysqli);
	$shippingLib = new shipping($mysqli);
	$setting = $cart->setting();
	$shipping = $setting["Default_Shipping"] == "free" ? "0" : $setting["Default_Shipping"];
	$shippingLib->id_kab = $_POST["id"];
	$price = $shippingLib->getShip();
	$shipping = ($price != false) ? $price->value : $shipping;
	echo $shipping;
endif;

	
