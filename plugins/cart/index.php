<link rel="stylesheet" href="<?PHP echo URL.'javascripts/select2.css'; ?>" media="screen" />
<link rel="stylesheet" href="<?PHP echo URL.'plugins/cart/style.css'; ?>" media="screen" />
<script src="<?PHP echo URL.'javascripts/select2.js'; ?>"></script>
<?PHP
include 'libraries/url.php';
include 'lib/item.php';
include 'lib/cart.php';
include 'lib/user.php';
include 'lib/regional.php';
include 'lib/shipping.php';

$itemLib = new item($this->mysqli);
$cart = new cart($this->mysqli);
$userLib = new User($this->mysqli);
$region = new regional($this->mysqli);
$shippingLib = new shipping($this->mysqli);

$itemLib->dbUpload = 'images/cart';
$setting = $cart->setting();
$layout = "layout2";
$page = $setting["limit_posts"];
$action = (isset($_GET['node2'])) ? $_GET['node2'] : "page";
$msg = "";
if ($action == "buy"):
	$id = $_GET["node3"];
	$q = 1;
	$cart->addtocart($id,$q);
	$back = $_SERVER['HTTP_REFERER'];
	alert(null, $back);
elseif ($action == "detail") :
	$itemid = $_GET["node3"];
	$itemLib->itemid = $itemid;
	$itemDetail = $itemLib->getItem();
	$diskon = $itemLib->diskon();
	$old_price = $itemDetail->price;
	$new_price = $old_price-$diskon;
	include("frontend/$layout/detail.php");
elseif ($action == "user") :
	include("frontend/user.php");
elseif ($action == "clear") :
	unset($_SESSION['cart']);
	$back = $_SERVER['HTTP_REFERER'];
	alert(null, $back);
elseif ($action == "troll") :
	include("frontend/troll.php");
elseif ($action == "checkout") :
	if (isset($_SESSION["userchart"]) && $_SESSION["userchart"]["log"] == true) :
		alert(null, "./finishorder"); exit;
		exit;
	endif;	
	if (isset($_POST["login"])) :
	    $data = $_POST["EmailLoginForm"];
	    if ($data["isGuestCheckout"] == "0"):
	        $userLib->email = $data["email"];
	        $userLib->setpassword($data["password"]);
	        $cek = $userLib->cekLogin();
	        if ($cek == false) :  $msg = "<p class=\"error type-2\">Wrong Username & Password</p>";
	        else :
	        	alert(null, "./finishorder"); exit;
	        endif;
	    else:
          $userLib->email = $data["email"];
	        $cek = $userLib->cekEmail();
	        if ($cek != false) : 
	        	if ($cek->sess_token == 1) :
	        		$msg = "<p class=\"error type-2\">Email already exists</p>"; 
	        	else :
	        		$token = $cek->sess_token;
	        		alert(null, "./register/$token"); exit;
	        	endif;
	        else: 
	        	include("libraries/generator.php");
						$idCreator = new idCreator(9);
						$token = $idCreator->id;
						$regData= array("email"=> $data["email"], "sess_token"=>$token);
						$userLib->data = $regData;
						$userLib->register();
	        	alert(null, "./register/$token"); exit;
	        endif;
	    endif;
	endif;
	include("frontend/checkout.php");
elseif ($action == "register") :
	if(isset($_POST["submit"])) :
		$info = $_POST["EmailLoginForm"];
		if ($info["password"] != $info["repassword"]) :
			$msg = "Password and Retype Password not match";
		else:
			$data = array("email"=>$info["email"],"password"=>sha1(md5($info["password"])), "name"=>$info["name"],"phone"=>$info["Phone"], "address"=>$info["address"], "identity_Id"=>$info["number"], "regional"=>$_POST["regional"], "sess_token"=>"1");
			if ($_POST["regional"] == "ID") :
			$newData = array("state"=>$_POST["state"], "regency"=>$_POST["regency"], "district"=>$_POST["district"],"village"=>$_POST["village"]);
			$data = array_merge($data ,$newData);
			endif;
			$userLib->userid = $info["userid"];
			$userLib->data = $data;
			$userLib->updateInfo();
			$_SESSION["userchart"]["log"] = true;
			$_SESSION["userchart"]["userid"] = $info["userid"];
			$query = $this->mysqli->query("select href from pages WHERE content='cart' LIMIT 1");
			$page_url = "product";
			if ($query->num_rows !=0)
			{
				$href = $query->fetch_object();
				$page_url = (isset($href->href))?$href->href:$page_url;
			}
			alert(null, URL.$page_url."/finishorder"); exit;
		endif;
	endif;
	if (!isset($_GET["node3"])) :
	    exit;
	endif;
	$token = $_GET["node3"];
	$userLib->token = $token;
	$userinfo = $userLib->getInfoDetail();
	if ($userinfo== false) exit;
	$region->idprov = $userinfo->state;
	$region->idkab = $userinfo->regency;
	$region->idkec = $userinfo->district;
	$region->idkel =  $userinfo->village;
	include("frontend/register.php");
elseif ($action == "finishorder") :
	$userLib->userid = (isset($_SESSION["userchart"]["userid"]))?$_SESSION["userchart"]["userid"]:"";
	$msg = "";
	$userinfo = $userLib->getInfo();
	$shipping = $setting["Default_Shipping"] == "free" ? "0" : $setting["Default_Shipping"];
	$shippingLib->id_kab = (isset($userinfo->regency))?$userinfo->regency:"";
	$price = $shippingLib->getShip();
	$shipping = ($price != false) ? $price->value : $shipping;
	if (isset($_POST["finishSubmit"])) :
		$form = $_POST["finishOrderForm"];
		include("libraries/generator.php");
		$idCreator = new idCreator(3);
		$orderid = date("ymd")."-".$idCreator->id;
		$idCreator = new idCreator(9);
		$confirmation = $idCreator->id;
		require 'email/PHPMailerAutoload.php';		
		$mail = new PHPMailer;
		$mail->IsSMTP();
		//$mail->SMTPDebug = 2;
		//$mail->Debugoutput = 'html';
		$mail->Host = 'localhost';
		$mail->Port = 465;
		$mail->SMTPSecure = 'ssl';
		$mail->SMTPAuth = false;
		//$mail->Username = EMAIL;
		//$mail->Password = PW_EMAIL;
		$mail->setFrom(EMAIL, AUTHOR);
		$mail->addReplyTo(EMAIL, AUTHOR);
		//Set who the message is to be sent to
		$mail->addAddress($userinfo->email, $userinfo->name);
			
		$orderHTML = '<table border="2" cellpadding="5px" cellspacing="1px" class="finish-order" width="100%">';
        	$orderHTML .= '<tr><th>Name</th><th>Price</th><th>Qty</th><th>Amount</th></tr>';
			$max=count($_SESSION['cart']);
			$total = 0;
			for($i=0;$i<$max;$i++){
				$itemid=$_SESSION['cart'][$i]['itemid'];
				$q=$_SESSION['cart'][$i]['qty'];
				$itemLib->itemid = $itemid;
				$item = $itemLib->getItem();
				$price = $item->price-$itemLib->diskon();
				$subtotal = $q*$price;
				$total = $total+$subtotal;
				if($q==0) continue;
				$color = $i%2==0 ? 'background-color:#ECECEC;' : '';
        		$orderHTML .= '<tr style="'.$color.'"><td>'.($i+1).'. '.$item->name.'</td>';
                $orderHTML .= '<td>'.$itemLib->number($price,"IDR").'</td>';
                $orderHTML .= '<td>'.$q.'</td>';                    
                $orderHTML .= '<td>'.$itemLib->number($subtotal,"IDR").'</td></tr>';
               
        }
		$orderHTML .= '<tr><td colspan="3">Total</td><td>'.$itemLib->number($total,"IDR").'</td></tr>';	
		$orderHTML .= '<tr><td colspan="3">Shipping</td><td>'.$itemLib->number($shipping,"IDR").'</td></tr>';	
		$orderHTML .= '<tr><td colspan="3">Total Pembayaran</td><td><strong>'.$itemLib->number($total+$shipping,"IDR").'</strong></td></tr></table>';
		$link = URL.PAGEHREF."/confirmation/$confirmation";
		$payment = $itemLib->payment();
		$text_link = "<br />Pembayaran dapat dilakukan melalui : ";
		foreach ($payment as $payment) {
			$text_link .= "<br />$payment->rekBank : $payment->rekNomor ($payment->rekName)";
		}
		$text_link .= "<br /> Jika anda sudah Melakukan Pembayaran, harap melakukan konfirmasi pembayaran melalui link : <br /> <a href=\"$link\">$link</a>";
		$from = array("%DOMAIN%","%FULLNAME%","%BODYMSG%","%ORDER%","%KONFIRMASI%","%FOOTER%");
		$replaced = array(URL,$userinfo->name,nl2br($setting["payment_mail"]),$orderHTML,$text_link,nl2br($setting["footer_mail"]));
		$mail->Subject = 'Payment Confirmation';
		$bodyMSG = file_get_contents('email/order.htm', dirname(__FILE__));
		$mail->AltBody = 'This is a plain-text message body';
		$msgHTML = str_replace($from,$replaced,$bodyMSG);
		$mail->msgHTML($msgHTML);	
		if($mail->Send()) {
			echo $msgHTML ;
		    //echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
			include 'lib/order.php';
			$orderLib = new order($this->mysqli);
			$data = array("orderid"=>$orderid,"userid"=>$_SESSION["userchart"]["userid"], "paymentMethod" =>$form["payment"], "shipping"=>$form["shipping"], "total"=>$form["total"],"confirmation"=>$confirmation);
			if ($form["sameAsAdrr"] == "1") :
				$addr = $userinfo->address;
				$region->idprov = $userinfo->state;
		        $region->idkab = $userinfo->regency;
		        $region->idkec = $userinfo->district;
		        $region->idkel =  $userinfo->village;
			else:
				$region->idprov = $_POST["state"];
		        $region->idkab = $_POST["regency"];
		        $region->idkec = $_POST["district"];
		        $region->idkel =  $_POST["village"];
				$addr = $form["address"];
			endif;
			$vil = $region->getVillage();
			$dist = $region->getDistrict();
			$reg = $region->getRegency();
			$stat = $region->getState();
			$data["orderAddress"] = $addr." ".$vil->nama.", ".$dist->nama." ".$reg->nama."-".$stat->nama;
			$orderLib->data = $data;
			$orderLib->add();
			foreach ($_SESSION['cart'] as $additem) {
				$itemLib->itemid = $additem["itemid"];
				$item = $itemLib->getItem();
				$price = $item->price-$itemLib->diskon();
				$data = array("orderid"=>$orderid, "itemid"=>$additem["itemid"], "quantity"=>$additem["qty"],"price"=>$price,"subtotal"=>$additem["qty"]*$price);
				$orderLib->data = $data;
				$orderLib->addItem();
			}
			unset($_SESSION['cart']);
			alert("Thanks for your Order. Chek your email for detail information", PAGEHREF); exit;
		}		
	endif;	
	include("frontend/finish.php");
else :
	$cate = $itemLib->categories();
	foreach ($cate as $cate) :
		$cat[$cate->catid] = $cate->href;
	endforeach;
	if (in_array($action, $cat)) :
		$indeks = array_search($action,$cat);
		$itemLib->where = " WHERE A.categories='$indeks' ";
	else:
		$action = "page";
	endif;
	$totpage = ceil($itemLib->total()/$page);
	$pg = (isset($_GET["node3"]) && is_numeric($_GET["node3"])) ? $_GET['node3'] : "1";
	if (isset($pg)) :
		$num = $pg<1 ? 1: ($pg>$totpage ? $totpage : ($pg-1)*$page);
	endif;
	$itemLib->limit="LIMIT $num, $page";
	$itemLib->order="ORDER BY  A.itemid DESC ";
	$dataItem = $itemLib->produk();
	$link = URL.PAGEHREF."/".$action;
	include("frontend/$layout/home.php");
endif;
?>
