<?PHP session_start(); ?>
<div class="adminContent">
<?php
if (isset($_SESSION['c2msauth']))
{
include '../../configs/vars.php';
include '../../configs/database.php';
include '../../libraries/dbconnect.php'; 
include '../../libraries/settings.php';
include '../../libraries/url.php';
include '../../libraries/libraries.php';

if (isset($_GET["item"])) include "backend/item.php";
else if (isset($_GET["categories"])) include "backend/category.php";
else if (isset($_GET["setting"])) include "backend/setting.php";
else if (isset($_GET["discount"])) include "backend/discount.php";
else if (isset($_GET["user"])) include "backend/user.php";
else if (isset($_GET["order"])) include "backend/order.php";
else if (isset($_GET["setting"])) include "backend/setting.php";
else if (isset($_GET["payment"])) include "backend/payment.php";
else if (isset($_GET["shipping"])) include "backend/shipping.php";
else if (isset($_GET["layout"])) include "backend/layout.php";
else include "backend/menu.php";
}
?>
</div>