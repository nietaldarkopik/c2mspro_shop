	<?PHP
	if(isset($_GET['add']))
	{
		$act = 'add';
		echo '<h2>Add New Item</h2>';
		$data = new ArrayObject();
		$data->diskonid=""; $data->itemid=""; $data->categories=""; $data->type=""; $data->diskon=""; $data->opentime=""; $data->untiltime="";
	}else{
		$diskonid = $_GET['edit'];
		$act = 'edit';
		echo '<h2>Edit Item</h2>';
		$itemLib->diskonid = $diskonid;
		$data = $itemLib->getDiskon();
	}
	?>
    <form id="general" method="POST" action="<?PHP echo URL.'plugins/cart/manage.php?discount'; ?>" enctype="multipart/form-data">
    	<p>
        	<label>Produk</label>
            <select name="itemid<?PHP echo ($act=="add") ? "[]\" multiple=\"multiple\" style=\"height: 100px;" : ""; ?>" id="album2" class="small">
			<?php
				echo '<option value="all">All Produk</option>';
				$produklist = explode(",",$data->itemid);
				$produk = $itemLib->produk();
				
				foreach($produk as $produk) {
					if (in_array($produk->itemid, $produklist)) $selected = "selected=\"selected\"";
					else $selected = "";
					echo '<option value="'.$produk->itemid.'" '.$selected.'>'.$produk->name.'</option>';
				}
			?>
		</select>
        </p>
		<p>
        <label>Categories</label>
        <select name="categories" id="categories" class="small">
			<option value="1" <?PHP echo $data->categories == "1" ? "selected=\"selected\"" : ""; ?>>Priode</option>
			<option value="2" <?PHP echo $data->categories == "2" ? "selected=\"selected\"" : ""; ?>>Member</option>
		</select>
		</p>
		<p>
        <label>Type</label>
        <select name="type" id="type" class="small">
			<option value="1" <?PHP echo $data->type == "1" ? "selected=\"selected\"" : ""; ?>>Persen</option>
			<option value="2" <?PHP echo $data->type == "2" ? "selected=\"selected\"" : ""; ?>>Potongan</option>
		</select>
		</p>
		<p>
        	<label>Diskon</label>
            <input type="text" name="diskon" class="input medium" value="<?PHP echo $data->diskon; ?>"/>
        </p>
		<p>
        	<label>Open time (YYYY-MM-DD)</label>
            <?PHP echo dateSelected("opentime",$data->opentime); ?>
        </p>
		<p>
        	<label>Until time (YYYY-MM-DD)</label>
        	<?PHP echo dateSelected("untiltime",$data->untiltime); ?>
        </p>
		<p>
			<input type="hidden" name="<?PHP echo $act; ?>" value="<?PHP echo $data->diskonid; ?>" class="submit"/>
            <input type="submit" value="Save Changes" class="submit"/>
        </p>
    </form>
	<p align="right">
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php'; ?>"/>Back To Menu</a> | 	
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?discount'; ?>"/>Back To List</a>	
	</p>