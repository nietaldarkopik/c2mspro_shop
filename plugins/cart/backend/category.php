<?php
include 'lib/item.php';

$Upload = "images/cart";
$dbUpload = '../../'.$Upload;
$itemLib = new item($mysqli);
$itemLib->dbUpload = $dbUpload;
if (isset($_POST["add"])) :
	$thumb = $_FILES["photo"];
	$tmp_name = $thumb["tmp_name"];
	if ($tmp_name !="") {
		is_dir($dbUpload) ? $dbUpload = $dbUpload : mkdir($dbUpload,0777);
		$thumbNail = $thumb["name"];
		move_uploaded_file($tmp_name, "$dbUpload/$thumbNail");	
	}
	$data = ["href"=>generate_seo_link($_POST['name']),"name"=>$_POST["name"], "thumb"=>$thumbNail, "description"=>$_POST["description"]];
	$itemLib->data = $data;
	$itemLib->addCat();
	echo '<script>alert("Categories added"); location.replace("../../'.$itemLib->hal().'");</script>';
	exit;
elseif (isset($_POST["edit"])) :
	$thumb = $_FILES["photo"];
	$itemLib->catid = $_POST["edit"];
	$data = $itemLib->getCat();
	$tmp_name = $thumb["tmp_name"];
	if ($tmp_name !="") {
		$thumbNail = ($data->thumb=="") ? $thumb["name"] : $data->thumb;
		move_uploaded_file($tmp_name, "$dbUpload/$thumbNail");	
	}
	$data = array("href"=>generate_seo_link($_POST['name']), "name"=>$_POST["name"], "thumb"=>$thumbNail, "description"=>$_POST["description"]);
	$itemLib->data = $data;
	$itemLib->editCat();
	echo '<script>alert("Categories edited"); location.replace("../../'.$itemLib->hal().'");</script>';
	exit;
elseif (isset($_GET["add"]) || isset($_GET["edit"])) :
	include "cat_add.php";
	exit;
elseif (isset($_GET["del"])) :
	$itemLib->catid = $_GET["del"];
	$itemLib->delCat();
endif;
$x = 3;
if(isset($_GET['p'])) $no = $_GET['p']; else $no = 1;
$i = ($no - 1) * $x;
$y = $no*$x;
$totpage = ceil($itemLib->total_cat()/$x);
$itemLib->where = "LIMIT $i, $x";
$dataItem = $itemLib->categories();
$status = array("0"=>"Out Of Stock", "1"=>"Available","2"=>"Pre Order");
?>
	<h2>List Categories<a href="javascript:void(0);" class="openmodalbox gradientButton" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?categories&add'; ?>"/>Add Categories</a></h2>
    <div id="pag-top" class="pagination">
        <div class="pag-count" style="float:right";>
            <?PHP echo "viewing categories $no to $y (of $totpage image)"; ?>
        </div>
	</div>
    
    <table id="dataTable" cellpadding="0" cellspacing="0">				
		<thead>
			<tr>
				<th>Thumb</th>
				<th>Categories</th>
				<th>Description</th>
				<th></th>
			</tr>
		</thead>					
		<tbody>
			<?PHP
			foreach ($dataItem as $cat) {
				echo '<tr>
						<td><img src="'.URL.$Upload."/".$cat->thumb.'" width="auto" height="100px" /></td>
						<td>'.$cat->name.'</td>
						<td>'.$cat->description.'</td>
						<td><div class="post-action"><span class="edit-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/cart/manage.php?categories&edit='.$cat->catid.'"/>Edit</a></span> | <span class="trash-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/cart/manage.php?categories&del='.$cat->catid.'"/>Trash</a></span></div></td>
					</tr>';
			}
			?>					
		</tbody>
	</table>
	<div class="pagination-links">
        <?PHP echo pagging($no,  $totpage, URL.'plugins/cart/manage.php?categories'); ?>
	</div>
	<p align="right">
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php'; ?>"/>Back To Menu</a> | 	
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?item'; ?>"/>View Item</a>	
	</p>