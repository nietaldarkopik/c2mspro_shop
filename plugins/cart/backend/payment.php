<?php
include 'lib/item.php';
$itemLib = new item($mysqli);
if (isset($_POST["add"])) :
	$data = array("rekBank"=>$_POST["rekBank"], "rekNomor"=>generate_seo_link($_POST["rekNomor"]), "rekName"=>$_POST["rekName"]);
	$itemLib->data = $data;
	$itemLib->addPayment();
	echo '<script>alert("Bank Account added"); location.replace("../../'.$itemLib->hal().'");</script>';
	exit;
elseif (isset($_POST["edit"])) :
	$data = array("rekBank"=>$_POST["rekBank"], "rekNomor"=>generate_seo_link($_POST["rekNomor"]), "rekName"=>$_POST["rekName"]);
	$itemLib->data = $data;
	$itemLib->rekid = $_POST["edit"];
	$itemLib->editPayment();
	echo '<script>alert("Bank Account edited"); location.replace("../../'.$itemLib->hal().'");</script>';
	exit;
elseif (isset($_GET["add"]) || isset($_GET["edit"])) :
	include "payment_add.php";
	exit;
elseif (isset($_GET["del"])) :
	$itemLib->rekid = $_GET["del"];
	$itemLib->delIPayment();
endif;
$x = 10;
if(isset($_GET['p'])) $no = $_GET['p']; else $no = 1;
$i = ($no - 1) * $x;
$y = $no*$x;
$totpage = ceil ($itemLib->totalpayment()/$x);
$itemLib->where = "LIMIT $i, $x";
$dataItem = $itemLib->payment();
?>

<h2>List Payment<a href="javascript:void(0);" class="openmodalbox gradientButton" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?payment&add'; ?>"/>Add Payment</a></h2>
    <div id="pag-top" class="pagination">
        <div class="pag-count" style="float:right";>
            <?PHP echo "viewing posts $no to $y (of $totpage page)"; ?>
        </div>
	</div>
    
    <table id="dataTable" cellpadding="0" cellspacing="0">				
		<thead>
			<tr>
				<th>No</th>
				<th>Bank Name</th>
				<th>Bank Account</th>
                <th>Name</th>
				<th></th>
			</tr>
		</thead>					
		<tbody>
			<?PHP
			foreach ($dataItem as $bank) {
				$i++;

				echo '<tr>
						<td>'.$i.'</td>
						<td>'.$bank->rekBank.'</td>
						<td>'.$bank->rekNomor.'</td>
						<td>'.$bank->rekName.'</td>
						<td><div class="post-action"><span class="edit-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/cart/manage.php?payment&edit='.$bank->rekid.'"/>Edit</a></span> | <span class="trash-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/cart/manage.php?payment&del='.$bank->rekid.'"/>Trash</a></span></div></td>
					</tr>';
			}
			?>					
		</tbody>
	</table>
	<div class="pagination-links">
        <?PHP echo pagging($no,  $totpage, URL.'plugins/cart/manage.php?item'); ?>
	</div>
	<p align="right">
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php'; ?>"/>Back To Menu</a> | 	
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?categories'; ?>"/>View Categories</a>	
	</p>