<?PHP
include 'lib/item.php';
include 'lib/cart.php';
$itemLib = new item($mysqli);
$cart = new cart($mysqli);
$setting = $cart->setting();
if (isset($_POST["submitForm"])) :
    $setting = ["Currency"=>$_POST["Currency"], "Regional"=>$_POST["country"], "Default_page"=>$_POST["page"], "Default_Rek"=>$_POST["account"], "Default_Shipping"=>$_POST["shipping"], "limit_posts"=>$_POST["limit_posts"],"payment_mail"=>$_POST["order_text"],"order_mail"=>$_POST["confirm_text"],"footer_mail"=>$_POST["footer_text"]];
    $cart->updateStting($setting);
    echo "Setting update";
endif;
echo '<h2>Setting your shop</h2>';
?>
<form id="setting" method="POST" action="<?PHP echo URL.'plugins/cart/manage.php?setting'; ?>" enctype="multipart/form-data">
    	<p>
        	<label>Currency</label>
            <?php
			$currency = ["IDR"=>"Rupiah","USD"=>"Dolar"];
			echo selectOption($currency,"Select Currcency",$setting["Currency"],"class='input small'", "Currency");
			?>
        </p>
        <p>
        	<label>Regional</label>
            <?php
			$country = ["ID"=>"Indonesia"];
			echo selectOption($country,"Select Country",$setting["Regional"],"class='input small'", "country");
			?>
        </p>
		<p>
        	<label>Default Page</label>
            <?php
			$country = ["cate"=>"Categories","prod"=>"Product"];
			echo selectOption($country,"Select page",$setting["Default_page"],"class='input small'", "page");
			?>
        </p>
		<p>
        	<label>Default Account (for bill/payment)</label>
            <?php
			$payment = $itemLib->paymentSelect();
			echo selectOption($payment,"Select account",$setting["Default_Rek"],"class='input small'", "account");
			?>
        </p>
		<p>
        	<label>Shipping Price</label>
            <input type="text" name="shipping" class="input small" value="<?PHP echo $setting["Default_Shipping"]; ?>"/>
        </p>
        <p>
            <label>Limit Posts</label>
            <input type="text" name="limit_posts" class="input small" value="<?PHP echo $setting["limit_posts"]; ?>"/>
        </p>
        <p>
            <label>Payment Order Header (Text for Email)</label>
            <textarea name ="order_text" class="input long"><?PHP echo $setting["payment_mail"]; ?></textarea>
        </p>
        <p>
            <label>Confirmation Order (Text for Email)</label>
            <textarea name ="confirm_text" class="input long"><?PHP echo $setting["order_mail"]; ?></textarea>
        </p>
        <p>
            <label>Footer Mail(Text for Email)</label>
            <textarea name ="footer_text" class="input long"><?PHP echo $setting["footer_mail"]; ?></textarea>
        </p>
        <p>
            <input type="hidden" name="submitForm"/>
            <input type="submit" name="submit" value="Save Changes" class="openmodalbox submit"/>
        </p>
    </form>
	<p align="right">
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php'; ?>"/>Back To Menu</a> | 	
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?item'; ?>"/>Back To List</a>	
	</p>