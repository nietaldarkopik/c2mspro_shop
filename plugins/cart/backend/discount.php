<?php
include 'lib/item.php';
$itemLib = new item($mysqli);
if (isset($_POST["add"])) :
	$itemid = $_POST["itemid"];
	$categories = $_POST["categories"];
	$type = $_POST["type"];
	$diskon = $_POST["diskon"];
	$opentime = $_POST["opentime"]["y"]."-".$_POST["opentime"]["m"]."-".$_POST["opentime"]["d"];
	$untiltime = $_POST["untiltime"]["y"]."-".$_POST["untiltime"]["m"]."-".$_POST["untiltime"]["d"];
	foreach ($itemid as $itemid) {
		$data = array("itemid"=>$itemid, "categories"=>$categories, "type"=>$type, "diskon"=>$diskon, "opentime"=>$opentime, "untiltime"=>$untiltime);
		$itemLib->data = $data;
		$itemLib->addDiskon();
	}	
	echo '<script>alert("Diskon added"); location.replace("../../'.$itemLib->hal().'");</script>';
	exit;
elseif (isset($_POST["edit"])) :
	$itemLib->diskonid = $_POST["edit"];
	$itemid = $_POST["itemid"];
	$categories = $_POST["categories"];
	$type = $_POST["type"];
	$diskon = $_POST["diskon"];
	$opentime = $_POST["opentime"]["y"]."-".$_POST["opentime"]["m"]."-".$_POST["opentime"]["d"];
	$untiltime = $_POST["untiltime"]["y"]."-".$_POST["untiltime"]["m"]."-".$_POST["untiltime"]["d"];
	$data = array("itemid"=>$itemid, "categories"=>$categories, "type"=>$type, "diskon"=>$diskon, "opentime"=>$opentime, "untiltime"=>$untiltime);
	$itemLib->data = $data;
	$itemLib->editDiskon();
	echo '<script>alert("Diskon edited"); location.replace("../../'.$itemLib->hal().'");</script>';
	exit;
elseif (isset($_GET["add"]) || isset($_GET["edit"])) :
	include "discount_add.php";
	exit;
elseif (isset($_GET["del"])) :
	$itemLib->diskonid = $_GET["del"];
	$itemLib->delDiskon();
endif;
$x = 10;
if(isset($_GET['p'])) $no = $_GET['p']; else $no = 1;
$i = ($no - 1) * $x;
$y = $no*$x;
$totpage = ceil($itemLib->totaldiskon()/$x);
$itemLib->where = "LIMIT $i, $x";
$dataItem = $itemLib->lisDiskon();
$categories = array("1"=>"Periode", "2"=>"Member");
$type = array("1"=>"Persen", "2"=>"Potongan");
?>
	<h2>List discount<a href="javascript:void(0);" class="openmodalbox gradientButton" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?discount&add'; ?>"/>Add Discount</a></h2>
    <div id="pag-top" class="pagination">
        <div class="pag-count" style="float:right";>
            <?PHP echo "viewing discount $no to $y (of $totpage image)"; ?>
        </div>
	</div>
    
    <table id="dataTable" cellpadding="0" cellspacing="0">				
		<thead>
			<tr>
				<th>Item Name</th>
				<th>Categories</th>
				<th>Discount</th>
				<th></th>
			</tr>
		</thead>					
		<tbody>
			<?PHP
			foreach ($dataItem as $diskon) {
				$categories = ($diskon->categories == 1) ? "Periode" : "Member";
				$diskonx = ($diskon->type == 1) ? $diskon->diskon." %" : $itemLib::number($diskon->diskon);
				echo '<tr>
						<td>'.$diskon->name.'</td>
						<td>'.$categories.'</td>
						<td>'.$diskonx.'</td>
						<td><div class="post-action"><span class="edit-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/cart/manage.php?discount&edit='.$diskon->diskonid.'"/>Edit</a></span> | <span class="trash-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/cart/manage.php?discount&del='.$diskon->diskonid.'"/>Trash</a></span></div></td>
					</tr>';
			}
			?>					
		</tbody>
	</table>
	<div class="pagination-links">
        <?PHP echo pagging($no,  $totpage, URL.'plugins/cart/manage.php?discount'); ?>
	</div>
	<p align="right">
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php'; ?>"/>Back To Menu</a> | 	
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?item'; ?>"/>View Item</a>	
	</p>