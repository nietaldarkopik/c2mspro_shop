	<?PHP
	if(isset($_GET['add']))
	{
		$act = 'add';
		echo '<h2>Add New Item</h2>';
		$data = new ArrayObject();
		$data->name=""; $data->categories=""; $data->cpr=""; $data->short_description=""; $data->description=""; $data->price=""; $data->availability=""; $data->weight=""; $data->itemid="";
	}else{
		$itemid = $_GET['edit'];
		$act = 'edit';
		echo '<h2>Edit Item</h2>';
		$itemLib->itemid = $itemid;
		$data = $itemLib->getItem();
		$dir = 'images/cart/'.$data->itemid;
		if (isset($_GET["delphoto"])) {
			$img = $_GET["delphoto"];
			@unlink("../../".$dir."/".$img);
		}
	}
	?>
    <form id="general" method="POST" action="<?PHP echo URL.'plugins/cart/manage.php?item'; ?>" enctype="multipart/form-data">
    	<p>
        	<label>Name</label>
            <input type="text" name="name" class="input medium" value="<?PHP echo $data->name; ?>"/>
        </p>
		<p>
        <label>Categories</label>
        <select name="category_id" id="album2" class="small">
			<?php
			$q = $mysqli->query("select * from plugin_cart_categories order by name");
			while($cat = $q->fetch_object())
			{
				if ($cat->catid == $data->catid) $selected="selected='selected'"; else $selected="";
				echo '<option value="'.$cat->catid.'" '.$selected.'>'.$cat->name.'</option>';
			}
			?>
		</select>
		</p>
		<p>
        	<label>Code Product</label>
            <input type="text" name="cpr" class="input medium" value="<?PHP echo $data->cpr; ?>"/>
        </p>
        <p>
        	<label>Short Description</label>
            <textarea name="short_description" class="input medium"><?PHP echo $data->short_description; ?></textarea>
        </p>
		<p>
        	<label>Description</label>
            <textarea name="description" class="input medium"><?PHP echo $data->description; ?></textarea>
        </p>
		<p>
        	<label>Price</label>
            <input type="text" name="price" class="input medium" value="<?PHP echo $data->price; ?>"/>
        </p>
		<p>
        	<label>Availability</label>
            <select name="availability" id="album2" class="small">
				<option value="0" <?PHP echo $data->availability=="0"? "selected=\"selected\"" : ""; ?>>Out Of Stock</option>
				<option value="1" <?PHP echo $data->availability=="1"? "selected=\"selected\"" : ""; ?>>Available</option>
				<option value="2" <?PHP echo $data->availability=="2"? "selected=\"selected\"" : ""; ?>>Pre Order</option>
			</select>
        </p>
        <p>ration image 3:4. recomended size 900x1200</p>
        <p>
        	<label>Upload Image</label>
            <input type="file" name="photo[]" class="input small" />
        </p>
		<?PHP if(isset($_GET['add']))
		{
		?>
		<p>
        	<label>Upload Image II</label>
            <input type="file" name="photo[]" class="input small" />
        </p>
		<p>
        	<label>Upload Image III</label>
            <input type="file" name="photo[]" class="input small" />
        </p>
		<?PHP } else { ?>
		<p>
        	<label>List Image</label>
            <?PHP 
			$images = loadDir("../../".$dir);
			echo "<table border=\"0\">";
			echo "<tr>";
			foreach ($images as $image) {
				echo "
				<td align=\"center\">
					<img src=".URL.$dir."/".$image." width=\"auto\" height=\"120\" /> 
					<br /><span class=\"trash-post\"><a href=\"javascript:void(0);\" class=\"openmodalbox\" ><input type=\"hidden\" name=\"ajaxhref\" value=\"".URL."plugins/cart/manage.php?item&edit=".$data->itemid."&delphoto=".$image."\"/>Trash</a></span>
				</td>";
			}
			echo "</tr>";
			echo "</table>";
			?>
        </p>		
		<?PHP } ?>
		<p>
        	<label>Weight</label>
            <input type="text" name="weight" class="input medium" value="<?PHP echo $data->weight; ?>"/>&nbsp;Kg
        </p>
        <p>
			<input type="hidden" name="<?PHP echo $act; ?>" value="<?PHP echo $data->itemid; ?>" class="submit"/>
            <input type="submit" value="Save Changes" class="submit"/>
        </p>
    </form>
	<p align="right">
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php'; ?>"/>Back To Menu</a> | 	
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?item'; ?>"/>Back To List</a>	
	</p>