<?php
include 'lib/item.php';
$itemLib = new item($mysqli);
if (isset($_POST["add"])) :
	$thumb = $_FILES["photo"];
	$data = array("name"=>$_POST["name"], "href"=>generate_seo_link($_POST["name"]), "categories"=>$_POST["category_id"], "cpr"=>$_POST["cpr"], "short_description"=>$_POST["short_description"], "description"=>$_POST["description"], "price"=>$_POST["price"], "availability"=>$_POST["availability"], "weight"=>$_POST["weight"]);
	$itemLib->data = $data;
	$itemLib->addItem($thumb);
	echo '<script>alert("item added"); location.replace("../../'.$itemLib->hal().'");</script>';
	exit;
elseif (isset($_POST["edit"])) :
	$thumb = $_FILES["photo"];
	$data = array("name"=>$_POST["name"], "href"=>generate_seo_link($_POST["name"]), "categories"=>$_POST["category_id"], "cpr"=>$_POST["cpr"], "short_description"=>$_POST["short_description"], "description"=>$_POST["description"], "price"=>$_POST["price"], "availability"=>$_POST["availability"], "weight"=>$_POST["weight"]);
	$itemLib->data = $data;
	$itemLib->itemid = $_POST["edit"];
	$itemLib->editItem($thumb);
	echo '<script>alert("item edited"); location.replace("../../'.$itemLib->hal().'");</script>';
	exit;
elseif (isset($_GET["add"]) || isset($_GET["edit"])) :
	include "item_add.php";
	exit;
elseif (isset($_GET["del"])) :
	$itemLib->itemid = $_GET["del"];
	$itemLib->delItem();
endif;
$x = 10;
if(isset($_GET['p'])) $no = $_GET['p']; else $no = 1;
$i = ($no - 1) * $x;
$y = $no*$x;
$totpage = ceil ($itemLib->total()/$x);
$itemLib->where = "LIMIT $i, $x";
$dataItem = $itemLib->produk();
$status = array("0"=>"Out Of Stock", "1"=>"Available","2"=>"Pre Order");
?>
	<h2>List Item<a href="javascript:void(0);" class="openmodalbox gradientButton" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?item&add'; ?>"/>Add Item</a></h2>
    <div id="pag-top" class="pagination">
        <div class="pag-count" style="float:right";>
            <?PHP echo "viewing posts $no to $y (of $totpage image)"; ?>
        </div>
	</div>
    
    <table id="dataTable" cellpadding="0" cellspacing="0">				
		<thead>
			<tr>
				<th>Item</th>
				<th>Categories</th>
				<th>Price</th>
                <th>availability</th>
				<th></th>
			</tr>
		</thead>					
		<tbody>
			<?PHP
			foreach ($dataItem as $produk) {
				echo '<tr>
						<td>'.$produk->name.'</td>
						<td>'.$produk->cate.'</td>
						<td>'.$itemLib->number($produk->price,"IDR").'</td>
						<td>'.$status[$produk->availability].'</td>
						<td><div class="post-action"><span class="edit-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/cart/manage.php?item&edit='.$produk->itemid.'"/>Edit</a></span> | <span class="trash-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/cart/manage.php?item&del='.$produk->itemid.'"/>Trash</a></span></div></td>
					</tr>';
			}
			?>					
		</tbody>
	</table>
	<div class="pagination-links">
        <?PHP echo pagging($no,  $totpage, URL.'plugins/cart/manage.php?item'); ?>
	</div>
	<p align="right">
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php'; ?>"/>Back To Menu</a> | 	
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?categories'; ?>"/>View Categories</a>	
	</p>