<?php
include 'lib/order.php';
$orderLib = new order($mysqli);
if (isset($_POST["add"])) :
	$orderid = $_POST["orderid"];
	$categories = $_POST["categories"];
	$type = $_POST["type"];
	$order = $_POST["order"];
	$opentime = $_POST["opentime"]["y"]."-".$_POST["opentime"]["m"]."-".$_POST["opentime"]["d"];
	$untiltime = $_POST["untiltime"]["y"]."-".$_POST["untiltime"]["m"]."-".$_POST["untiltime"]["d"];
	foreach ($orderid as $orderid) {
		$data = array("orderid"=>$orderid, "categories"=>$categories, "type"=>$type, "order"=>$order, "opentime"=>$opentime, "untiltime"=>$untiltime);
		$orderLib->data = $data;
		$orderLib->addOrder();
	}	
	echo '<script>alert("Order added"); location.replace("../../'.$orderLib->hal().'");</script>';
	exit;
elseif (isset($_POST["edit"])) :
	$orderLib->orderid = $_POST["edit"];
	$orderid = $_POST["orderid"];
	$categories = $_POST["categories"];
	$type = $_POST["type"];
	$order = $_POST["order"];
	$opentime = $_POST["opentime"]["y"]."-".$_POST["opentime"]["m"]."-".$_POST["opentime"]["d"];
	$untiltime = $_POST["untiltime"]["y"]."-".$_POST["untiltime"]["m"]."-".$_POST["untiltime"]["d"];
	$data = array("orderid"=>$orderid, "categories"=>$categories, "type"=>$type, "order"=>$order, "opentime"=>$opentime, "untiltime"=>$untiltime);
	$orderLib->data = $data;
	$orderLib->editOrder();
	echo '<script>alert("Order edited"); location.replace("../../'.$orderLib->hal().'");</script>';
	exit;
elseif (isset($_GET["add"]) || isset($_GET["edit"])) :
	include "order_add.php";
	exit;
elseif (isset($_GET["del"])) :
	$orderLib->orderid = $_GET["del"];
	$orderLib->delOrder();
endif;
$x = 10;
if(isset($_GET['p'])) $no = $_GET['p']; else $no = 1;
$i = ($no - 1) * $x;
$y = $no*$x;
$orderLib->where = "LIMIT $i, $x";
$dataItem = $orderLib->daftar();
$totpage = ceil(count($dataItem)/$x);
$categories = array("1"=>"Periode", "2"=>"Member");
$type = array("1"=>"Persen", "2"=>"Potongan");
?>
	<h2>List order</h2>
    <div id="pag-top" class="pagination">
        <div class="pag-count" style="float:right";>
            <?PHP echo "viewing order $no to $y (of $totpage orders)"; ?>
        </div>
	</div>
    
    <table id="dataTable" cellpadding="0" cellspacing="0">				
		<thead>
			<tr>
				<th>Users</th>
				<th>Tanggal Order</th>
				<th>Total Tagihan</th>
				<th>Metode Pembayaran</th>
				<th>Status</th>
				<th></th>
			</tr>
		</thead>					
		<tbody>
			<?PHP
			foreach ($dataItem as $order) {
        $payment_method = ($order->paymentMethod == 'T')?'Transfer':'COD';
        $status = "";
        switch($order->status)
        {
          case "1":
          $status = "New Order";
          break;
          case "2":
          $status = "Waiting Confirmation";
          break;
          case "2":
          $status = "Waiting Confirmation";
          break;
          case "3":
          $status = "Delivered";
          break;
          case "4":
          $status = "Failed";
          break;
        }
				echo '<tr>
						<td>'.$order->userid.'</td>
						<td>'.$order->orderDate.'</td>
						<td>'.$order->total.'</td>
						<td>'.$payment_method.'</td>
						<td>'.$status.'</td>
						<td>
              <div class="post-action">
                <span class="edit-post">
                  <a href="javascript:void(0);" class="openmodalbox" >
                    <input type="hidden" name="ajaxhref" value="'.URL.'plugins/cart/manage.php?order&edit='.$order->orderid.'"/>Edit</a></span> | <span class="trash-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/cart/manage.php?order&del='.$order->orderid.'"/>
                    Trash
                  </a>
                </span>
              </div>
            </td>
					</tr>';
			}
			?>					
		</tbody>
	</table>
	<div class="pagination-links">
        <?PHP echo pagging($no,  $totpage, URL.'plugins/cart/manage.php?order'); ?>
	</div>
	<p align="right">
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php'; ?>"/>Back To Menu</a> | 	
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?item'; ?>"/>View Item</a>	
	</p>