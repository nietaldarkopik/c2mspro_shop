<?php
include 'lib/regional.php';
include 'lib/shipping.php';
include 'lib/item.php';

$shipping = new shipping($mysqli);
$region = new regional($mysqli);
$itemLib = new item($mysqli);

if (isset($_POST["add"])) :
	$data = array("id_kab"=>$_POST["regency"], "value"=>$_POST["shipping"]);
	$shipping->data = $data;
	$shipping->add();
elseif (isset($_POST["edit"])) :
	$data = array("id_kab"=>$_POST["regency"], "value"=>$_POST["shipping"]);
	$shipping->data = $data;
	$shipping->edit();
elseif (isset($_GET["add"]) || isset($_GET["edit"])) :
	include "shipping_add.php";
	exit;
elseif (isset($_GET["del"])) :
	$shipping->id_kab = $_GET["del"];
	$shipping->delete();
endif;

$x = 10;
if(isset($_GET['p'])) $no = $_GET['p']; else $no = 1;
$i = ($no - 1) * $x;
$y = $no*$x;
$totpage = ceil ($shipping->total()/$x);
$shipping->where = "ORDER BY B.id_kab ASC LIMIT $i, $x";
$dataItem = $shipping->listData();
$status = array("0"=>"Out Of Stock", "1"=>"Available","2"=>"Pre Order");
?>
	<h2>List Shipping<a href="javascript:void(0);" class="openmodalbox gradientButton" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?shipping&add'; ?>"/>Add Shipping Price</a></h2>
    <div id="pag-top" class="pagination">
        <div class="pag-count" style="float:right";>
            <?PHP echo "viewing posts $no to $y (of $totpage image)"; ?>
        </div>
	</div>
    
    <table id="dataTable" cellpadding="0" cellspacing="0">				
		<thead>
			<tr>
				<th>No.</th>
				<th>Province</th>
				<th>City</th>
				<th>Shipping Price</th>
				<th></th>
			</tr>
		</thead>					
		<tbody>
			<?PHP
			foreach ($dataItem as $ship) {
				echo '<tr>
						<td>'.($i+1).'</td>
						<td>'.$ship->province.'</td>
						<td>'.$ship->city.'</td>
						<td>'.$itemLib->number($ship->value,"IDR").'</td>
						<td><div class="post-action"><span class="edit-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/cart/manage.php?shipping&edit='.$ship->id_kab.'"/>Edit</a></span> | <span class="trash-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/cart/manage.php?shipping&del='.$ship->id_kab.'"/>Trash</a></span></div></td>
					</tr>';
				$i++;
			}
			?>					
		</tbody>
	</table>
	<div class="pagination-links">
        <?PHP echo pagging($no,  $totpage, URL.'plugins/cart/manage.php?shipping'); ?>
	</div>
	<p align="right">
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php'; ?>"/>Back To Menu</a> | 	
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?setting'; ?>"/>View Setting</a>	
	</p>