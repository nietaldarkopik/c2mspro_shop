	<?PHP
	if(isset($_GET['add']))
	{
		$act = 'add';
		echo '<h2>Add Shipping Price</h2>';
		$data = new ArrayObject();
		$data->id_prov="";  $data->id_kab=""; $data->value="";
	}else{
		$id_kab = $_GET['edit'];
		$act = 'edit';
		echo '<h2>Edit Shipping Price</h2>';
		$shipping->id_kab = $id_kab;
		$data = $shipping->getShip();
        $region->idprov =  $data->id_prov;
	}
	?>
    <form id="general" method="POST" action="<?PHP echo URL.'plugins/cart/manage.php?shipping'; ?>" enctype="multipart/form-data">
    	<p>
        	<label>Province</label>
            <?PHP echo selectOption($region->state(),"Select Province", $data->id_prov,"class='input small' required", "state"); ?>
        </p>
		<p>
        	<label>Region/City</label>
        	<span class="select_kab">
            	<?PHP echo selectOption($region->regency(),"Select Regency", $data->id_kab,"class='input small' required", "regency"); ?>
       		</span>
        </p>
        <p>
        	<label>Shipping Price</label>
            <input type="text" name="shipping" class="input small" value="<?PHP echo $data->value; ?>" /> &nbsp; (Only Number ex : 10000)
        </p>
        <p>
			<input type="hidden" name="<?PHP echo $act; ?>" value="<?PHP echo $data->id_kab; ?>" class="submit"/>
            <input type="submit" value="Save Changes" class="submit openmodalbox"/>
        </p>
    </form>
	<p align="right">
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php'; ?>"/>Back To Menu</a> | 	
	<a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/cart/manage.php?shipping'; ?>"/>Back To List</a>	
	</p>

	<script type="text/javascript" src="<?PHP echo URL;?>/javascripts/jquery.min.js"></script>
	<script>
    $(document).ready(function(){
        var loader = '<img src="<?PHP echo URL;?>images/loader.gif" />';
        $('#state').change(function() {
            $('.select_kab').html(loader);
            var id = $(this).val();
            $.post("<?PHP echo URL; ?>/plugins/cart/ajax.php?kab",{ id: id}, function(data){
	            $('.select_kab').html(data);
            });
        });
    });
	</script>