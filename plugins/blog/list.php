<?php
$x = 4;
if(isset($_GET['p'])) $no = $_GET['p']; else $no = 1;
$i = ($no - 1) * $x;
$y = $no*$x;
$q = $mysqli->query("select * from plugin_blog_posts join plugin_blog_categories on plugin_blog_posts.category_id=plugin_blog_categories.category_id order by plugin_blog_posts.id");
$totpage = ceil($q->num_rows/$x);
$q = $mysqli->query("select * from plugin_blog_posts join plugin_blog_categories on plugin_blog_posts.category_id=plugin_blog_categories.category_id order by plugin_blog_posts.id DESC LIMIT $i, $x");
?>
	<h2>Blog Post <a href="javascript:void(0);" class="openmodalbox gradientButton" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/blog/manage.php?addposts'; ?>"/>Add New</a></h2>    
    <table id="dataTable" cellpadding="0" cellspacing="0">				
		<thead>

			<tr>
				<th>Title</th>
				<th>Categories</th>
				<th>Comments</th>
                <th>Date</th>
			</tr>
		</thead>					
		
		<tbody>
			<?PHP
			while($blog = $q->fetch_object())
			{
			$date = explode(' ',$blog->date);
			$query = $mysqli->query("select * from plugin_blog_comments where post_id='$blog->id'");
			echo '<tr>
				<td><a href="#"><strong>'.$blog->title.'</strong></a><br /><div class="post-action"><span class="edit-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/blog/manage.php?editposts='.$blog->id.'"/>Edit</a></span> | <span class="trash-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/blog/manage.php?delposts='.$blog->id.'"/>Trash</a></span></div></td>							
				<td>'.$blog->category.'</td>
				<td><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/blog/manage.php?comments"/>'.$query->num_rows.'</a></td>
                <td>'.$blog->date.'</td>
				</tr>';					
			}
			?>					
		</tbody>
	</table>
	<div id="pag-top" class="pagination">
        <div class="pag-count">
            <?PHP echo "viewing posts $no to $y (of $totpage Page)"; ?>
        </div>
   		<div class="pagination-links">
        	<?PHP echo pagging($no,  $totpage, URL.'plugins/blog/manage.php?list'); ?>
    	</div>
	</div>
	<p align="right"><a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/blog/manage.php?categories'; ?>"/>>> View Categories</a></p>