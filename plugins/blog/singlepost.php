    <?php
    include "tinymce.php";
	$q = $this->mysqli->query("select * from plugin_blog_posts join plugin_blog_categories on plugin_blog_posts.category_id=plugin_blog_categories.category_id where plugin_blog_posts.post_href='$_GET[node3]'");
	$blog = $q->fetch_object();	
	$date = explode(' ',$blog->date);
	$comment = $this->mysqli->query("select * from plugin_blog_comments where post_id='$blog->id'");
	if (PAGE_TYPE != 'full')
		$thumbnail = URL.'thumb?src='.URL.$pathBlog."/".$blog->thumb.'&w=650&h=400&a=t';
		else
		$thumbnail = URL.'thumb?src='.URL.$pathBlog."/".$blog->thumb.'&w=1024&h=400&a=t';
	echo '
		<article class="entry">
		<div class="bordered">
			<figure class="add-border">
				<a href="'.URL.PAGEHREF.'/'.$blog->category_href.'/'.$blog->post_href.'" class="single-image">';
				if ($blog->thumb !="") echo '<img src="'.$thumbnail.'" class="img-blog"/>';				
				echo '</a>
			</figure>
		</div>
		<div class="entry-meta">
			<span class="date">'.date("d",strtotime($date[0])).'</span>
			<span class="month">'.date("M",strtotime($date[0])).'</span>
		</div>
		<div class="entry-body">					
			<div class="entry-title">
				<h2 class="title"><a href="'.URL.PAGEHREF.'/'.$blog->category_href.'/'.$blog->post_href.'">'.ucwords(strtolower($blog->title)).'</a></h2>
				<span class="comments">With <a href="'.URL.PAGEHREF.'/'.$blog->category_href.'/'.$blog->post_href.'#comments">'.$comment->num_rows.' </a>Comments</span>
				
			</div><!--/ .entry-title-->';
			if(isset($_SESSION['c2msauth']))
			{	
			echo '
			<form id="editContent" method="POST" enctype="multipart/form-data">
			<div id="editor1">
				'.$blog->post.'
			</div>
			<div class="edit_widget">
					<input type="hidden" name="blogid" value="'.$blog->id.'" />
                	<input type="submit" name="save_blog" class="savecontent" value="Save" id="saveEditorx" style="display:none" />
                    <input type="reset" class="savecontent" value="Cancel" id="closeEditorx" style="display:none"/>
                    <input type="button" class="savecontent" id="openEditorx" value="Edit Content"/>
            </div>
			</form>';
			}
			else
			{
				echo $blog->post;
			}
		echo '</div>
	</article><!-- end blog post -->';
	$q = $this->mysqli->query("select * from plugin_blog_comments where post_id='$blog->id'");
	$jumlahComments = $q->num_rows;
	echo '<section id="comments">';
	echo '<h4>'.$jumlahComments.' Comments</h4>';
	if ($jumlahComments >0)
	{
		echo '<ol class="comments-list">';
		while($comment = $q->fetch_object())
		{			
			$date = explode(' ',$comment->date);
			echo '
			<li class="comment">
				<article>
				<div class="bordered alignleft">
					<figure class="add-border">
						<img src="http://www.gravatar.com/avatar/'.md5($comment->email).'.png"/>
					</figure>
				</div><!--/ .bordered-->
				<div class="comment-body">
					<div class="comment-meta">
						
						<div class="author">'.ucwords($comment->name).'</div>
						<div class="date">'.date("F d, Y", strtotime($comment->date)).'</div>

					</div><!--/ .comment-meta -->

					<p>'. nl2br($comment->comment).'</p>';
					
					if(isset($_SESSION['c2msauth']))
					{
						echo '<a href="javascript:void(0);" class=" comment-reply-link button default align-btn-right openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/blog/manage.php?delcomment='.$comment->id.'"/>Trash</a>';
						echo '<a class="comment-reply-link button default align-btn-right openmodalbox" href="javascript:void(0);"><input type="hidden" name="ajaxhref" value="'.URL.'plugins/blog/manage.php?reply='.$comment->id.'"/>Reply</a>';
					}
				echo '</div><!--/ .comment-body -->
				</article><!-- end comment -->				
				<ul class="children">';
				$query = $this->mysqli->query("select * from plugin_blog_comments where parent_id='$comment->id'");
				while($child=$query->fetch_object()) {
				echo '<li class="comment">
						<article>
							<div class="bordered alignleft">
								<figure class="add-border">
						<img src="http://www.gravatar.com/avatar/'.md5($child->email).'.png"/>
					</figure>
							</div><!--/ .bordered-->	
							<div class="comment-body">
								<div class="comment-meta">						
									<div class="author">'.ucwords($child->name).'</div>
									<div class="date">'.date("F d, Y", strtotime($child->date)).'</div>
								</div><!--/ .comment-meta -->
								<p>'. nl2br($child->comment).'</p>';
					if(isset($_SESSION['c2msauth']))
					{
						echo '<a href="javascript:void(0);" class=" comment-reply-link button default align-btn-right openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/blog/manage.php?delcomment='.$child->id.'"/>Trash</a>';
					}								
					echo '</div><!--/ .comment-body -->
						</article>
					</li>';
				}
				echo '</ul>				
			</li>';
		}
		echo '</ol>';
	}	
	?>
	</section>
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5405a7d4219a99fd"></script>
	<div class="addthis_native_toolbox"></div>
    <div class="comment-form" id="comments" >
    
    	<h5>Leave a reply</h5>
        <p>Your email address will not be published. Required fields are marked * </p>
        <form method="post" action="#" class="comments-form" id="comment_form">
            <p class="input-block">
		    	<label>Name <span class="required">*</span> </label>
		        <input type="text" name="name" required />
		    </p>
		    <p class="input-block">
		    	<label>Email <span class="required">*</span></label>
		        <input type="text" name="email" required />
		    </p>
		    <p class="input-block">
		    	<label>Website</label>
		        <input type="text" name="website" />
		    </p>
		   <p class="input-block">
		    	<label>Comment <span class="required">*</span></label>
		        <textarea name="comment" rows="12" cols="10" required></textarea>
		    </p>
		    <p class="input-block">
		    	<label>Please input security code <span class="required">*</span></label>
		        <?PHP echo recaptcha_get_html($publickey, ""); ?>
		        <input type="hidden" name="ref" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
				<input type="hidden" name="post_id" value="<?php echo $blog->id; ?>" />
				
		    </p>
			<p class="form-allowed-tags">You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes:  <code>&lt;a href="" title=""&gt; &lt;abbr title=""&gt; &lt;acronym title=""&gt; &lt;b&gt; &lt;blockquote cite=""&gt; &lt;cite&gt; &lt;code&gt; &lt;del datetime=""&gt; &lt;em&gt; &lt;i&gt; &lt;q cite=""&gt; &lt;strike&gt; &lt;strong&gt; </code></p>
			<p class="input-block">
			<button type="submit" name="addcomment" id="submit" class="button default">Post it</button>
			</p>
        </form>                   
    </div><!-- end comment form -->