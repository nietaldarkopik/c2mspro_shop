<?PHP
echo '
<script type="text/javascript" src="'.URL.'extensions/tinymce/tinymce.jquery.min.js"></script>';
echo "<script type=\"text/javascript\">";
echo "
$(document).ready(function($) {
	$(\"#saveEditorx\").hide();
	$(\"#closeEditorx\").hide();
	
	$(\"#openEditorx\").click(function(){
		$(\"#saveEditorx\").show();
		$(\"#closeEditorx\").show();
		$(\"#openEditorx\").hide();
		tinyMCE.execCommand('mceAddEditor',false,'editor1');
	});
	$(\"#closeEditorx\").click(function(){
		$(\"#saveEditorx\").hide();
		$(\"#closeEditorx\").hide();
		$(\"#openEditorx\").show();
		tinyMCE.execCommand('mceRemoveEditor',false,'editor1');
	});
});

tinymce.init({
		menubar:false,
		plugins: [
			\"advlist autolink lists link image charmap print preview anchor\",
			\"searchreplace visualblocks code fullscreen\",
			\"insertdatetime media table contextmenu paste filemanager\"
		],
		toolbar: \"styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image media  | code\",
		autosave_ask_before_unload: false,
		min_height: 400
	});";
echo "</script>";
?>