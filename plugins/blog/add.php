<form id="general" method="POST" action="<?PHP echo URL.'plugins/blog/manage.php'; ?>" enctype="multipart/form-data">
<?PHP
if(isset($_GET['addposts']))
{
	$act = 'addpost';
	echo '<h2>Add New Blog Post</h2>';
    $data["title"] = "";
    $data["keywords"] = "";
    $data["description"] = "";
    $data["post"] = "";
}else{
	$act = 'editpost';
	echo '<h2>Edit New Blog Post</h2>';
	$q=$mysqli->query("SELECT * FROM plugin_blog_posts WHERE id='".$_GET["editposts"]."'");
	$data = $q->fetch_array();
}
?>
    	<p>
        	<label>Title</label>
            <input type="text" name="title" class="input long" value="<?PHP echo $data["title"]; ?>"/>
        </p>
		<p>
        	<label>Keywords</label>
            <input type="text" name="keywords" class="input long" value="<?PHP echo $data["keywords"]; ?>"/>
        </p>
        <p>
        	<label>Description</label>
            <textarea name="description" id="description" class="long"><?PHP echo htmlentities($data["description"]); ?></textarea>
        </p>
        <p>
        	<label>Thumbnail</label>
            <input type="file" name="thumb" class="input long"/>
        </p>
		<p>
        <label>Categories</label>
        <select name="category_id" id="album2" class="small">
			
			<option value="" onclick="hide('addalbum',true);"></option>
			<?php
			$q = $mysqli->query("select * from plugin_blog_categories order by category_id");
			while($cat = $q->fetch_object())
			{
				if ($cat->category_id == $data["category_id"]) $selected="selected='selected'"; else $selected="";
				echo '<option onclick="hide(\'addcategory\',true);" value="'.$cat->category_id.'" '.$selected.'>'.$cat->category.'</option>';
			}
			?>
			<option value="" onclick="show('addcategory',true);">Add New</option>
		</select>
		</p>
		<p id="addcategory" style="display:none;">
			<label>New Category</label>
			<input type="text" name="newcategory" class="input long"/>
		</p> 
        <p>
			<input type="hidden" name="<?PHP echo $act; ?>" value="<?PHP echo $data["id"];?>" />
            <input type="submit" name="submit" value="Save Changes" class="submit"/>
        </p>
</form>