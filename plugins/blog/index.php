<?php 
require_once('recaptchalib.php');
$publickey = "6Ldg6cASAAAAAA_xFBt6a4EzPUgbQJ3QkfFf88KR";
$privatekey = "6Ldg6cASAAAAAK71J-K7EdOEmyQpWJT3Sk3VleJm";
$dirBlog = '../../images/blog';
$pathBlog = 'images/blog';
if(isset($_POST['addcomment']))
{
	if(empty($_POST['name']) || empty($_POST['email']) || empty($_POST['comment']))
	{
		echo '<script>alert("Semua kolom harus di Isi")</script>';
	}
	else if (isset($_POST["recaptcha_response_field"])) {
		$resp = recaptcha_check_answer ($privatekey,
		                                        $_SERVER["REMOTE_ADDR"],
		                                        $_POST["recaptcha_challenge_field"],
		                                        $_POST["recaptcha_response_field"]);

		if ($resp->is_valid) {
			$name		= strip_tags($_POST['name']);
			$email		= strip_tags($_POST['email']);
			$website	= strip_tags($_POST['website']);
			$comment	= strip_tags($_POST['comment']);
			$post_id	= strip_tags($_POST['post_id']);
			$date		= date('Y-m-d');

			if($this->mysqli->query("insert into plugin_blog_comments(name,email,website,comment,active,date,post_id) value('$name','$email','$website','$comment','1','$date','$post_id')"))
			{
				echo '<script>location.replace("'.$_POST['ref'].'");</script>';
			}
		}else echo '<script>alert("Wrong Captcha")</script>'; 
	}else echo '<script>alert("Input Captcha")</script>'; 
}
$query = $this->mysqli->query("select category_href from plugin_blog_categories");
while ($data = $query->fetch_assoc()) $category[] = $data["category_href"];


if(isset($_GET['node3']) && !(is_numeric($_GET['node3'])))
{
	if (isset($_POST["save_blog"]))
	{
		$href = $_GET["node3"];
		$this->mysqli->query("update plugin_blog_posts set post='$_POST[editor1]' where post_href='".$href."'");
	}
	include("singlepost.php");
}
else if(isset($_GET['node2']) && !(is_numeric($_GET['node2'])) && in_array($_GET['node2'],$category))
{
		$q		= $this->mysqli->query("select * from plugin_blog_categories where category_href='$_GET[node2]'");
		$data	= $q->fetch_object();
		$cat	= $data->category_id;
		$x = 4;
		if(isset($_GET['node3']) && is_numeric($_GET['node3'])) $no = $_GET['node3']; else $no = 1;
		$i = ($no - 1) * $x;
		
		$q = $this->mysqli->query("select * from plugin_blog_posts join plugin_blog_categories on plugin_blog_posts.category_id=plugin_blog_categories.category_id where plugin_blog_posts.active='1' and plugin_blog_posts.category_id='$cat' order by plugin_blog_posts.id desc");
		if ($q->num_rows == 0) echo "No Article Found";
		else{
			
		$totpage = ceil($q->num_rows/$x);
		$query = "select * from plugin_blog_posts join plugin_blog_categories on plugin_blog_posts.category_id=plugin_blog_categories.category_id where plugin_blog_posts.active='1' and plugin_blog_posts.category_id='$cat' order by plugin_blog_posts.id desc LIMIT $i,$x";
		$q = $this->mysqli->query($query);
		
		}//end of article found
		$href = URL.PAGEHREF."/".$_GET["node2"];
		
}
else
{
	$x = 4;
	if(isset($_GET['node2']) && is_numeric($_GET['node2'])) $no = $_GET['node2']; else $no = 1;
	$i = ($no - 1) * $x;
	$q = $this->mysqli->query("select * from plugin_blog_posts join plugin_blog_categories on plugin_blog_posts.category_id=plugin_blog_categories.category_id where plugin_blog_posts.active='1' order by plugin_blog_posts.id desc ");
	if ($q->num_rows == 0) echo "No Article Found";
	else{
	$totpage = ceil($q->num_rows/$x);
	$q = $this->mysqli->query("select * from plugin_blog_posts join plugin_blog_categories on plugin_blog_posts.category_id=plugin_blog_categories.category_id where plugin_blog_posts.active='1' order by plugin_blog_posts.id desc LIMIT $i,$x");
	}//end of article found
	$href = URL.PAGEHREF;
}
while($blog = $q->fetch_object())
	{
		$date = explode(' ',$blog->date);
		if (PAGE_TYPE != 'full')
		$thumbnail = URL.'thumb?src='.URL.$pathBlog."/".$blog->thumb.'&w=650&h=400&a=t';
		else
		$thumbnail = URL.'thumb?src='.URL.$pathBlog."/".$blog->thumb.'&w=1024&h=400&a=t';
		$query = $this->mysqli->query("select * from plugin_blog_comments where post_id='$blog->id'");
		echo '
		<article class="entry">
		<div class="bordered">
			<figure class="add-border">
				<a href="'.URL.PAGEHREF.'/'.$blog->category_href.'/'.$blog->post_href.'" class="single-image">';
				if ($blog->thumb !="") echo '<img src="'.$thumbnail.'" class="img-blog"/>';				
		echo 	'</a>
			</figure>
		</div>
		<div class="entry-meta">
			<span class="date">'.date("d",strtotime($date[0])).'</span>
			<span class="month">'.date("M",strtotime($date[0])).'</span>
		</div>
		<div class="entry-body">					
			<div class="entry-title">
				<h2 class="title"><a href="'.URL.PAGEHREF.'/'.$blog->category_href.'/'.$blog->post_href.'">'.$blog->title.'</a></h2>
				<span class="comments">With <a href="'.URL.PAGEHREF.'/'.$blog->category_href.'/'.$blog->post_href.'#comments">'.$query->num_rows.' </a>Comments</span>
				
			</div><!--/ .entry-title-->		
			<p>'.short_view($blog->post,500).'</p>
			<a href="'.URL.PAGEHREF.'/'.$blog->category_href.'/'.$blog->post_href.'" class="button default small">Continue reading</a>
		</div>
	</article><!-- end blog post -->';
	}
	if (isset($totpage) && $totpage >1)
	{
	echo '<div class="pages"><ul class="page-list">';
	echo pagging($no,  $totpage, $href);
	echo '</ul></div>';
	}
?>
