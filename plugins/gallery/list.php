<?PHP
$x = 4;
if(isset($_GET['p'])) $no = $_GET['p']; else $no = 1;
$i = ($no - 1) * $x;
$y = $no*$x;
$q = $mysqli->query("select * from plugin_gallery where page_id='".PAGEID."'");
$jumlah = $q->num_rows;
if ($jumlah == 0) { echo "Please select the page where this plugin installed and then select edit"; exit; }
$totpage = ceil($jumlah/$x);
$q = $mysqli->query("select * from plugin_gallery where page_id='".PAGEID."' ORDER BY id DESC LIMIT $i, $x");

?>
	<h2>Gallery Library <a href="javascript:void(0);" class="openmodalbox gradientButton" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/gallery/manage.php?addphoto='.PAGEID.'&pageid='.PAGEID; ?>"/>Add New</a></h2>
    <div id="pag-top" class="pagination">
        <div class="pag-count">
            <?PHP echo "viewing image $no to $y (of $totpage image)"; ?>
        </div>
   		<div class="pagination-links">
        	<?PHP echo pagging($no,  $totpage, URL.'plugins/gallery/manage.php?pageid='.PAGEID ); ?>
    	</div>
	</div>
    
    <table id="dataTable" cellpadding="0" cellspacing="0">				
		<thead>
			<tr>
            	<th width="20%">&nbsp;</th>
				<th width="30%">File</th>
				<th width="20%">Title</th>
				<th width="30%">album</th>
			</tr>
		</thead>					
		
		<tbody>
			<?php					
			while($photo = $q->fetch_object())
			{
				echo '<tr>
                	<td><img src="'.URL.'images/gallery/thumbnails/'.$photo->photo.'" alt="alternatif text" width="75" height="75" /></td>
					<td><a href="#"><strong>'.$photo->photo.'</strong></a><br /><div class="post-action"><span class="edit-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/gallery/manage.php?editphoto='.$photo->id.'&pageid='.PAGEID.'" />Edit</a></span> | <span class="trash-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'plugins/gallery/manage.php?delphoto='.$photo->id.'&pageid='.PAGEID.'" />Trash</a></span></div></td>							
					<td>'.$photo->title.'</td>
					<td>'.$photo->album.'</td>
				</tr>';
			}
			?>										
		</tbody>
	</table>
	<p align="right"><a href="javascript:void(0);" class="openmodalbox next page-numbers" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/gallery/manage.php?albumid&pageid='.PAGEID; ?>"/>>> View Album</a></p>