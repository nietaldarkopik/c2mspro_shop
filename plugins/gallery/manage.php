<?php
session_start();
if (isset($_SESSION['c2msauth']))
{
include '../../configs/vars.php';
include '../../configs/database.php';
include '../../libraries/dbconnect.php'; 
include '../../libraries/settings.php';
include '../../libraries/resize_class.php';
include '../../libraries/url.php';
include 'libraries.php';
define("PAGEID",$_REQUEST["pageid"]);
if(isset($_POST['addphoto']) || isset($_POST['editphoto']))
{	
	if(empty($_POST['album']) && empty($_POST['newalbum']))
	{
		$album		= 'default';
		$album_href = 'default';
	}
	else
	{
		!empty($_POST['newalbum']) ? $album		= $_POST['newalbum'] : $album		= $_POST['album'];
		$album_href = generate_seo_link($album);
	}		
	$desc		= $_POST['desc'];
	$page_id	= PAGEID;
	$date		= date('Y-m-d');

	
	if(empty($_POST['title']))
	{
		$photo_href	= substr($photo,0,-4);
	}
	else
	{
		$photo_href	= generate_seo_link($_POST['title']);
	}
	
	if (!empty($_FILES['photo']['name']))
	{
		if (isset($_POST['addphoto']))
		{
			$photo		= substr(md5(microtime()),27,5).'.'.ext($_FILES['photo']['name']);
			$mysqli->query("insert into plugin_gallery SET album='$album', album_href='$album_href',photo='$photo',title='$desc',date=NOW(),page_id='".PAGEID."'");
		}else{
			$q=$mysqli->query("SELECT * FROM plugin_gallery WHERE id='".$_POST["editphoto"]."'");
			$data = mysql_fetch_object($q);
			
			$photo = $data->photo;
			$mysqli->query("UPDATE plugin_gallery SET album='$album', album_href='$album_href',photo='$photo',title='$desc',date=NOW(),page_id='".PAGEID."' WHERE id='".$_POST["editphoto"]."'");
			unlink("../../images/gallery/$photo");
		}
	
		//PRINT_R($_FILES);
		move_uploaded_file($_FILES['photo']['tmp_name'], "../../images/gallery/$photo");
		$resizeObj = new resize("../../images/gallery/$photo");
		
		$resizeObj -> resizeImage(280, 185, "crop");
		$resizeObj -> saveImage("../../images/gallery/thumbnails/$photo", 100);
		
		$resizeObj -> resizeImage(800, 600, "crop");
		$resizeObj -> saveImage("../../images/gallery/$photo", 100);
		
	}else{
		if (isset($_POST['addphoto']))
		{
			//Error karena Foto kosong 
		}else{
		$q=$mysqli->query("SELECT * FROM plugin_gallery WHERE id='".$_POST["editphoto"]."'");
		$data = mysql_fetch_object($q);
		
		$photo = $data->photo;
		$mysqli->query("UPDATE plugin_gallery SET album='$album', album_href='$album_href',title='$desc',date=NOW(),page_id='".PAGEID."' WHERE id='".$_POST["editphoto"]."'");
		unlink("../../images/gallery/$photo");
		}
	}
	//resize($_FILES['photo']['tmp_name'],$photo,280,185,'../../images/gallery/thumbnails/');
	//resize($_FILES['photo']['tmp_name'],$photo,$width,$height,'images/gallery/');
	$q=$mysqli->query("SELECT href FROM pages WHERE id='".PAGEID."'");
	$data = mysql_fetch_object($q);
	echo '<script>location.replace("'.URL.$data->href.'");</script>';
	exit;

}else if (isset($_GET["delphoto"])){
	$q=$mysqli->query("SELECT photo FROM plugin_gallery WHERE id='".$_GET["delphoto"]."'");
	$data= mysql_fetch_object($q);
	
	unlink("../../images/gallery/$data->photo");
	unlink("../../images/gallery/thumbnails/$data->photo");
	
	$mysqli->query("DELETE FROM plugin_gallery WHERE id='".$_GET["delphoto"]."'");

}else if (isset($_POST['editalbum'])) {
	$oldalbum		= $_POST['editalbum'];
	$album		= $_POST['album'];
	$album_href = generate_seo_link($_POST['album']);
	$mysqli->query("UPDATE plugin_gallery SET album='$album', album_href='$album_href' WHERE album='$oldalbum' AND page_id='".PAGEID."'");

}else if (isset($_GET['delalbum'])) {
	$album		= $_GET['delalbum'];
	$mysqli->query("DELETE FROM plugin_gallery WHERE album='$album' AND page_id='".PAGEID."'");	
}
?>
		 <div class="adminContent">
			<?PHP
			if (isset($_GET["addphoto"]) || isset($_GET["editphoto"])) include "add.php";
			else if (isset($_GET["editalbum"])) include "addcategory.php";
			else if (isset($_GET["albumid"])) include "album.php";
			else include "list.php";
			?>
		 </div>

<?PHP } ?>