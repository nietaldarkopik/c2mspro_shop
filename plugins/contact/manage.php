<?php
session_start();
if (isset($_SESSION['c2msauth']))
{
include '../../configs/vars.php';
include '../../configs/database.php';
include '../../libraries/dbconnect.php'; 
include '../../libraries/settings.php';
include '../../libraries/url.php';

if(isset($_POST['contactsettings']))
{
	//$href = $_POST["href"];
	$mysqli->query("update settings set value='$_POST[org]' where param='organization'");
	$mysqli->query("update settings set value='$_POST[email]' where param='email'");
	$mysqli->query("update settings set value='$_POST[altemail]' where param='altemail'");
	$mysqli->query("update settings set value='$_POST[address]' where param='address'");
	$mysqli->query("update settings set value='$_POST[phone]' where param='phone'");
	$mysqli->query("update settings set value='$_POST[fax]' where param='fax'");
	$mysqli->query("update settings set value='$_POST[hp]' where param='hp'");
	$mysqli->query("update settings set value='$_POST[map]' where param='map'");
	
	$thumb = $_FILES["img"];
	$tmp_name = $thumb["tmp_name"];
	if ($tmp_name != "") {
		$thumbNail = "contact_".$thumb["name"];
		move_uploaded_file($tmp_name, "../../images/contact/$thumbNail");
		$mysqli->query("update settings set value='$thumbNail' where param='imgcontact'");
	}
	//header("location: ".URL.$href);
	header("location: ../../contact-us");
}
?>
<div class="adminContent">
<h2>Setting Contact</h2>
<form id="general" method="post" action="<?PHP echo URL.'plugins/contact/manage.php'; ?>" enctype="multipart/form-data">
		<p>
			<label>Organisasi / Perusahaan</label>
			<input type="text" name="org" class="input medium" value="<?php echo ORGANIZATION ?>" />
		</p>
		<p>
			<label>No. Telp</label>
			<input type="text" name="phone" class="input short" value="<?php echo PHONE ?>" />
		</p>
		<p>
			<label>No. HP</label>
			<input type="text" name="hp" class="input medium" value="<?php echo HP ?>" />
		</p>
		<p>
			<label>No. Fax</label>
			<input type="text" name="fax" class="input short" value="<?php echo FAX ?>" />
		</p>
		<p>
			<label>Email</label>
			<input type="text" name="email" class="input medium" value="<?php echo EMAIL ?>" />
		</p>
		<p>
			<label>Alternate Email</label>
			<input type="text" name="altemail" class="input medium" value="<?php echo ALTEMAIL ?>" />
		</p>
		<p>
			<label>Address</label>
			<textarea name="address" class="long"><?php echo ADDRESS ?></textarea>
		</p>
		<p>
			<label>Image Contact</label>
			<input type="file" name="img" class="input medium" />
		</p>
		<p>
			<label>Map</label>
			<textarea name="map" class="long"><?php echo MAP?></textarea>
		</p>		
		<p>
			<label>&nbsp;</label>
			<input type="hidden" name="contactsettings" value="Save" />
			<input type="submit" name="submit" value="Save Changes" class="submit"/>
		</p>
</form>
</div>

<?PHP } ?>