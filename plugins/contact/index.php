<center><?PHP echo MAP; ?></center>
<div class="clearfix border-bottom"></div>
<div class="eight columns">
<?php	
if(isset($_POST['contact']))
{
	if(empty($_POST['name']) || empty($_POST['phone']) || empty($_POST['email']) || empty($_POST['subject']) ||  empty($_POST['message']))
	{
		echo '<script>alert("Semua kolom harus di isi !");</script>';
	}
	else
	{		
		$name		= strip_tags($_POST['name']);
		$subject	= '['.strtoupper($_SERVER['SERVER_NAME']).'] '.$_POST['subject'];
		$content	= strip_tags($_POST['message']);
		$phone		= strip_tags($_POST['phone']);
		$email		= strip_tags($_POST['email']);

		$message ='
		<html>
		 <head>
		  <title>'.$_POST['subject'].'</title> 
		 </head>
		 <body>
			<table width="100%">				
				<tr>
					<td  valign="top">Nama</td>
					<td width="1"  valign="top">:</td>
					<td  valign="top">'.$name.'</td>
				</tr>															
				<tr>
					<td valign="top">No. Telp / HP</td>
					<td width="1"  valign="top">:</td>
					<td valign="top">'.$phone.'</td>
				</tr>
				<tr>
					<td  valign="top">Email</td>
					<td width="1"  valign="top">:</td>
					<td  valign="top">'.$email.'</td>
				</tr>	
				<tr>
					<td  valign="top">Email</td>
					<td width="1"  valign="top">:</td>
					<td  valign="top">'.$email.'</td>
				</tr>	
				<tr>
					<td valign="top">Pesan</td>
					<td width="1"  valign="top">:</td>
					<td valign="top">'.$content.'</td>
				</tr>
			</table>
		 </body>
		</html>';

		$to			= EMAIL;
		$cc			= ALTEMAIL;
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: '.$name.' <'.$email.'>' . "\r\n";
		$headers .= 'Cc: '.$cc. "\r\n";

		if(mail($to, $subject, $message, $headers))
		{
			echo '<h3>Pesan anda telah terkirim. Terimakasih.</h3>';
		}			
	}
}else{
?>
	<h3 class="smart-head">Do you have any question?</h3>
	<section id="contact">
    <form method="post" action="" enctype="multipart/form-data" class="comments-form">
    <p class="input-block">
    	<label>Full Name <span class="required">*</span></label>
        <input type="text" name="name" class="text" required />
    </p>
   <p class="input-block">
    	<label>Phone Number <span class="required">*</span></label>
        <input type="text" name="phone" class="text" required />
    </p>
   <p class="input-block">
    	<label>Email <span class="required">*</span></label>
        <input type="text" name="email" class="text" required />
    </p>
    <p class="input-block">
    	<label>Subject <span class="required">*</span></label>
        <input  type="text" name="subject"  class="text" required/>
    </p>
    <p class="input-block">
    	<label>Message <span class="required">*</span></label>
        <textarea name="message" id="message" rows="12" cols="10" required></textarea>
		<input type="hidden" name="contact"/>
    </p>
	<p class="input-block">
		<button type="submit" name="submit" id="submit" class="button default">Send it</button>
	</p>
    </form>
	</section>
<?PHP } 
$thumbnail = URL.'thumb?src='.URL.'images/contact/'.IMGCONT.'&w=300&h=200&a=t';
?>
</div>
<div class="seven columns">
<h3 class="smart-head"><?php echo ORGANIZATION ?></h3>
<p><img src="<?PHP echo  $thumbnail; ?>" /></p>
<p>
<span class="ico_home">Address</span>: <cite><?php echo ADDRESS ?></cite>
<br><span class="ico_phone">Phone Number </span>: <?php echo PHONE ?> 
<!--<p><span class="ico_hphone">Handphone Number</span>: <?php echo HP ?></p>-->
<?php
	if(FAX != '')
	{
	?>
<br><span class="ico_fax">Fax Number</span>: <?php echo FAX ?>
<?php
	}	
	?>
<br><span class="ico_email">Email Address</span>: <?php echo EMAIL ?>
</div>