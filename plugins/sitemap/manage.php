<?PHP
session_start();
if (isset($_SESSION['c2msauth']))
{
include '../../configs/vars.php';
include '../../configs/database.php';
include '../../libraries/dbconnect.php'; 
include '../../libraries/settings.php';
include "libraries.php";

$sitemap = new sitemap(["mysqli" => $mysqli]);
if (isset($_GET["generate"])) :
	$sitemap->website = URL;
	$sitemap->generate();
	echo "succes XML Generated";
endif;

?>
<div class="adminContent">
	<table id="dataTable" cellpadding="0" cellspacing="0">				
		<tr>
			<td><a href="javascript:void(0);" class="openmodalbox gradientButton" >
			<input type="hidden" name="ajaxhref" value="<?PHP echo URL.'plugins/sitemap/manage.php?generate'; ?>"/>Generate</a></td>
			<td><a href="<?PHP echo URL.'sitemap.xml'; ?>" class="gradientButton" target="_blank">view sitemap</a></td>
		</tr>
	</table>
</div>
<?PHP } ?>