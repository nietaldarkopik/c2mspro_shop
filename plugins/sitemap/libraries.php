<?php
class sitemap
{
	private $loc = "";
	private $lastmod = "";
	private $changefreq = "daily";
	private $priority = "1.0";
	public $author = "http://www.c2ms.org";
	public $website = "";
	public $path = "../../";
	private $mysqli;
	
	function  __construct($array="")
	{
		if (is_array($array)) {
			$time = new DateTime;
			isset($array["loc"]) ? $this->loc = $array["loc"] : $this->loc;
			if (isset($array["lastmod"])) $this->lastmod = $array["lastmod"];
			else $this->lastmod = $time->format(DateTime::ATOM);
			isset($array["changefreq"]) ? $this->changefreq = $array["changefreq"] : $this->changefreq;
			isset($array["priority"]) ? $this->priority = $array["priority"] : $this->priority;
			isset($array["mysqli"]) ? $this->mysqli = $array["mysqli"] : $this->mysqli;
		}
	}
	function add($id)
	{	
		$loc = $this->loc;
		$lastmod = $this->lastmod;
		$changefreq = $this->changefreq;
		$priority = $this->priority;	
		$XML = $this->path."sitemap.xml";
		$dom = new DomDocument;
		$dom->preserveWhiteSpace = false;
		$dom->validateOnParse = true;
		$dom->formatOutput = true;
		$dom->load($XML);
		$xpath = new DomXPath($dom);
		$parent = $xpath->query("//urlset"); 
		$parent_node = $dom->createElement('url');
		$parent_node->appendChild($dom->createElement("loc", $loc));
		$parent_node->appendChild($dom->createElement("lastmod", $lastmod));
		$parent_node->appendChild($dom->createElement("changefreq", $changefreq));
		$parent_node->appendChild($dom->createElement("priority", $priority));
		$dom->appendChild($parent_node);
		$parent->item(0)->insertBefore($parent_node); 
		$dom->save($XML) or die("Error");
	}
	function delete($id)
	{
		$XML = $this->path."header.xml";
		$dom = new DomDocument;
		$dom->preserveWhiteSpace = false;
		$dom->validateOnParse = true;
		$dom->formatOutput = true;
		$dom->load($XML);
		// Locate the old product node to modify
		$xpath = new DOMXpath($dom);
		$nodelist = $xpath->query("/urlset/url/id={$id}]");
		$oldnode = $nodelist->item(0);
		// Remove
		$oldnode->parentNode->removeChild($oldnode);		   
		$dom->save($XML) or die("Error");		
	}
	function generate()
	{
		$file = $this->path."sitemap.xml";		// output file
		$pf = fopen ($file, "w");
		if (!$pf)
		{
			echo "cannot create $file\n";
			return;
		}

		fwrite ($pf,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
		<urlset
			  xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"
			  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
			  xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9
					http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">
		<!-- created by c2ms.org Plugin by mangcoding -->");
		$time = new DateTime;
		fwrite ($pf,"<url>
			<loc>".$this->website."</loc>
			<lastmod>".date($time->format(DateTime::ATOM))."</lastmod>
			<changefreq>daily</changefreq>
			<priority>1.0</priority>
		</url>
		");	
		
		$query = $this->mysqli->query("select * from pages where pid='0' and status='1' AND href !='error-page' order by oid");
		while($pages = $query->fetch_object()) {
			fwrite ($pf,"<url>
				<loc>".$this->website.$pages->href."</loc>
				<lastmod>".date($time->format(DateTime::ATOM))."</lastmod>
				<changefreq>daily</changefreq>
				<priority>1.0</priority>
			</url>
			");
			
			$queryx =  $this->mysqli->query("select * from pages where pid='$pages->id' and status='1' order by oid");
			while($subpage = $queryx->fetch_object())
			{
				fwrite ($pf,"<url>
				<loc>".$this->website.$pages->href."/".$subpage->href."</loc>
				<lastmod>".date($time->format(DateTime::ATOM))."</lastmod>
				<changefreq>daily</changefreq>
				<priority>1.0</priority>
				</url>
				");
			}
		}	
		$queryblog = $this->mysqli->query("select href from pages WHERE content='blog' LIMIT 1");
		if ($queryblog->num_rows !=0) :
		$href = $queryblog->fetch_object();
		$blog_href = $href->href;
		$query = $this->mysqli->query("select plugin_blog_categories.category_href, plugin_blog_posts.post_href, plugin_blog_posts.date from plugin_blog_posts join plugin_blog_categories on plugin_blog_posts.category_id=plugin_blog_categories.category_id order by plugin_blog_posts.id");
		//date(DATE_ATOM, mktime(0, 0, 0, 7, 1, 2000));
		while($blog = $query->fetch_object())
		{
			fwrite ($pf,"<url>
			<loc>".$this->website.$blog_href."/".$blog->category_href."/".$blog->post_href."</loc>
			<lastmod>".date(DATE_ATOM, strtotime($blog->date))."</lastmod>
			<changefreq>daily</changefreq>
			<priority>1.0</priority>
			</url>
			");
		}
		endif;
		fwrite ($pf, "</urlset>\n");
		fclose ($pf);		
	}
}  
?>
