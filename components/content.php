<?php
class content
{
	private $mysqli;
	function __construct(mysqli $conn)
	{
		$this->mysqli = $conn;
	}
	function load()
	{		
		if(isset($_POST['content']))
		{
			if(!empty($_POST['editor1']))
			{
				if($this->mysqli->query("update pages set content='$_POST[editor1]' where id='".PAGEID."'"))
				{
					echo '<script>location.replace("'.$_POST['ref'].'");</script>';
				}
			}			
		}
		if(PAGEHREF =='search') include 'components/result.php';
		else if(PAGETYPE == 'page')
		{	
			echo '
			<script type="text/javascript" src="'.URL.'extensions/tinymce/tinymce.jquery.min.js"></script>';
			echo "<script type=\"text/javascript\">
			$(document).ready(function($) {
				$(\"#saveEditorx\").hide();
				$(\"#closeEditorx\").hide();
				
				$(\"#openEditorx\").click(function(){
					$(\"#saveEditorx\").show();
					$(\"#closeEditorx\").show();
					$(\"#openEditorx\").hide();
					tinyMCE.execCommand('mceAddEditor',false,'editor1');
				});
				$(\"#closeEditorx\").click(function(){
					$(\"#saveEditorx\").hide();
					$(\"#closeEditorx\").hide();
					$(\"#openEditorx\").show();
					tinyMCE.execCommand('mceRemoveEditor',false,'editor1');
				});
			});						
			";
			echo "tinymce.init({
					plugins: [
						\"advlist autolink lists link image charmap print preview anchor\",
						\"searchreplace visualblocks code fullscreen textcolor\",
						\"insertdatetime media table contextmenu paste filemanager\"
					],
					image_advtab: true,
					toolbar: \"styleselect | bold italic | alignleft aligncenter alignright alignjustify | forecolor backcolor | bullist numlist | link image media  | code\",
					autosave_ask_before_unload: false,
					min_height: 400
				});";
			echo "</script>";
			
			if(isset($_SESSION['c2msauth']))
			{	
			echo '
			<form id="editContent" method="POST" enctype="multipart/form-data">
			<div id="admcomp">
			<div id="editor1">
				'.CONTENT.'
			</div>
			</div>
			<div class="edit_widget">
					<input type="hidden"  name="ref" value="'.$_SERVER['REQUEST_URI'].'" /><input type="hidden" name="content" />
                	<input type="submit" class="savecontent" value="Save" id="saveEditorx" style="display:none" />
                    <input type="reset" class="savecontent" value="Cancel" id="closeEditorx" style="display:none"/>
                    <input type="button" class="savecontent" id="openEditorx" value="Edit Content"/>
            </div>
			</form>';
			}
			else
			{
				echo CONTENT;
			}
		}
		elseif(PAGETYPE == 'plugin')
		{		
			if(isset($_SESSION['c2msauth']))
			{
echo
				'
				<div class="edit_widget">
	                    <a class="openmodalbox" href="javascript:void(0);"><input type="hidden" name="ajaxhref" value="'.URL.'plugins/'.CONTENT.'/manage.php?pageid='.PAGEID.'">Manage '.ucwords(CONTENT).'</a>
	            </div><div class="clear"></div>';
				include 'plugins/'.CONTENT.'/index.php';	
			}
			else
			{
				include 'plugins/'.CONTENT.'/index.php';
			}			
		}
	}
}  
?>