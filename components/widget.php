<?php
class widget
{
	private $mysqli;
	function __construct(mysqli $conn)
	{
		$this->mysqli = $conn;
	}
	function top()
	{
		$this->_widget("1");
	}
	function left()
	{
		$this->_widget("2");
	}
	function right()
	{
		$this->_widget("3");
	}
	function bottom()
	{
		$this->_widget("4");
	}
	function center()
	{
		$this->_widget("5");
	}
	function settings($widget, $alias, $show_alias, $position, $pageid, $status){
		$query ="UPDATE widgets SET position='$position', alias='$alias', show_alias='$show_alias', position='$position', page_id='$pageid', status='$status' WHERE widget='$widget'";
		$this->mysqli->query($query);
	}
	function _list()
	{
		$query = $this->mysqli->query("SELECT * FROM widgets ORDER by oid ASC");
		while($data = $query->fetch_assoc()) $list[] = $data;
		return $list;
	}
	function publish($id)
	{
		$this->mysqli->query("UPDATE widgets SET status='1' WHERE id='$id'");
	}
	function unpublish($id)
	{
		$this->mysqli->query("UPDATE widgets SET status='2' WHERE id='$id'");
	}
	function down($id)
	{
		$query = $this->mysqli->query("SELECT oid FROM widgets WHERE id='$id'");
		$data = $query->fetch_object();
		$query2 = $this->mysqli->query("SELECT * FROM widgets");
		if ($data->oid < $query2->num_rows)
		{
		$query = $this->mysqli->query("UPDATE widgets SET oid='$data->oid' WHERE oid='".($data->oid+1)."'");
		$query = $this->mysqli->query("UPDATE widgets SET oid='".($data->oid+1)."' WHERE id='".$id."'");
		}
	}
	function up($id)
	{
		$query = $this->mysqli->query("SELECT oid FROM widgets WHERE id='$id'");
		$data = $query->fetch_object();
		
		if ($data->oid >1)
		{
			$query = $this->mysqli->query("UPDATE widgets SET oid='$data->oid' WHERE oid='".($data->oid-1)."'");
			$query = $this->mysqli->query("UPDATE widgets SET oid='".($data->oid-1)."' WHERE id='".$id."'");
		}
	}
	function _widget($position)
	{
		if ($position == 4) $limit = "LIMIT 4"; else $limit="";
		$query = "select * from widgets WHERE position='$position' AND status='1' AND (find_in_set ('".PAGEID."',page_id) OR page_id='".PAGEID."' OR page_id='all') order by oid $limit";
		$wtop = $this->mysqli->query($query);
		$a=1;
		$jumlah = $wtop->num_rows;
		if ($jumlah == 4) $cols = "four";
		else if ($jumlah == 3) $cols = "five";
		else if ($jumlah == 2) $cols = "eight";
		else if ($jumlah == 1) $cols = "sixteen";
		while($widget = $wtop->fetch_object())
		{			
			$page_id = explode(',',$widget->page_id);
			if(in_array(PAGEID,$page_id) || $widget->page_id =="all")
			{	
				if ($position == 4) $class = $cols." columns"; 
				else  $class = "widget widget_popular_posts five columns";
				
				if ($widget->widget == "list_blog") $class .= " widget_nav_menu"; 				
				echo "<div id=\"widget_".$widget->widget."\" class=\"".$class."\">";
				if ($widget->show_alias == "1") echo "<h3 class=\"widget-title\">$widget->alias</h3>"; else echo "";
				if(isset($_SESSION['c2msauth'])){
					
					include 'widgets/'.$widget->widget.'/index.php';
					echo '<div class="edit_widget">
						<a class="openmodalbox" href="javascript:void(0);">Setting<input type="hidden" name="ajaxhref" value="'.URL.'manage/widgets.php?widget='.$widget->widget.'" /></a> 
						<a class="openmodalbox" href="javascript:void(0);">Edit<input type="hidden" name="ajaxhref" value="'.URL.'widgets/'.$widget->widget.'/manage.php" /></a>
						</div>';
				}else {
					include 'widgets/'.$widget->widget.'/index.php';
				}
				echo "</div>";
			$a++;			
			}
		}
		if ($jumlah == 0) return false;
	}	
}  
?>
