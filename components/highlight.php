<?php
class highlight
{
	private $mysqli;
	function __construct(mysqli $conn)
	{
		$this->mysqli = $conn;
	}
	function load()
	{
		if($_SERVER['QUERY_STRING'] == '')
		{
			if(HIGHLIGHT == '1')
			{
				echo '<table width="100%">';
				echo '<tr>';
				$qhighlight		= $this->mysqli->query("select * from highlight");
				while($highlight = $qhighlight->fetch_object())
				{
					echo '<td valign="top" id="highlight">';
					echo '<a href="'.$highlight->link.'">';
					echo '<img src="'.URL.'images/user/'.$highlight->image.'" align="'.$highlight->align.'" border="0" />';
					echo '<h1>'.$highlight->title.'</h1>';
					echo $highlight->text;
					echo '</a>';
					echo '</td>';
				}
				echo '</tr>';
				echo '</table>';
			}			
		}
	}	
} 
?>
