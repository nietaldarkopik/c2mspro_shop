<?php
class header
{
	private $images = "";
	private $title = "";
	private $text = "";
	private $link = "";
	public $DIR = './../images/slides/';
	public $path = 'images/slides/';
	public $pathXML = '/';
	
	function  __construct($array="")
	{
		if (is_array($array)) {
			isset($array["images"]) ? $this->images = $array["images"] : "";
			isset($array["title"]) ? $this->title = $array["title"] : "";
			isset($array["text"]) ? $this->text = $array["text"] : "";
			isset($array["link"]) ? $this->link = $array["link"] : "";
		}
	}
	function get() {
		$hasil = array();
		$pathXML = $this->pathXML."header.xml";
		$dataXML = simplexml_load_file($pathXML);
		foreach ($dataXML->header as $data) {
			$hasil[] = $data;
		}
	return $hasil;
	}
	function getById($id) {
		$XML = $this->pathXML."header.xml";
		$dom = new DomDocument;
		$dom->validateOnParse = true;
		$dom->load($XML);
		// Locate the old product node to modify
		$xpath = new DOMXPath($dom);
		$data = $xpath->query("//*[@id='$id']")->item(0);	
		$data = $dom->saveXML($data);
		$data = simplexml_load_string($data);
		return $data;
	}
	function add()
	{	
		$uploaddir = $this->DIR;
		if ($this->images["name"] !="")
		{
			$images = $this->images["name"];
			move_uploaded_file($this->images["tmp_name"], $uploaddir.$images);
			$images = $this->path.$images;
		}
		$header_title = $this->title;
		$header_text = $this->text;
		$header_link = ($this->link == "") ? "#" : $this->link;	
	
		$XML = $this->pathXML."header.xml";
		$dom = new DomDocument;
		$dom->preserveWhiteSpace = false;
		$dom->validateOnParse = true;
		$dom->formatOutput = true;
		$dom->load($XML);
		$xpath = new DomXPath($dom);
		$id = $xpath->query('//headers/header[not(../header/id > id)]/id')->item(0)->nodeValue;
		$oid = $xpath->query('//headers/header[not(../header/oid > oid)]/oid')->item(0)->nodeValue;
		$parent = $xpath->query("//headers"); 
		$parent_node = $dom->createElement('header');
		$attribute = $dom->createAttribute("id");
		$attribute->value = $id+1;
		$parent_node->appendChild($attribute);
		$parent_node->appendChild($dom->createElement("id", $id+1));
		$parent_node->appendChild($dom->createElement("oid", $oid+1));
		$parent_node->appendChild($dom->createElement("images", $images));
		$parent_node->appendChild($dom->createElement("header_title", $header_title));
		$parent_node->appendChild($dom->createElement("header_text", $header_text));
		$parent_node->appendChild($dom->createElement("header_link", $header_link));
		$parent_node->appendChild($dom->createElement("header_status", "1"));
		$dom->appendChild($parent_node);
		$parent->item(0)->insertBefore($parent_node); 
		$dom->save($XML) or die("Error");
	}
	function edit($id)
	{
		$uploaddir = $this->DIR;
		$data = $this->getById($id);
		if ($this->images["name"] !="")
		{
			@unlink("../".$data->images);
			$images = $this->images["name"];
			move_uploaded_file($this->images["tmp_name"], $uploaddir.$images);
			$images = $this->path.$images;
		}else $images = $data->images;
		$header_title = $this->title;
		$header_text = $this->text;
		$header_link = ($this->link == "") ? "#" : $this->link;		
		
		$XML = $this->pathXML."header.xml";
		$dom = new DomDocument;
		$parent_node = $dom->createElement('header');
		$attribute = $dom->createAttribute("id");
		$attribute->value = $id;
		$parent_node->appendChild($attribute);
		$parent_node->appendChild($dom->createElement("id", $id));
		$parent_node->appendChild($dom->createElement("oid", $data->oid));
		$parent_node->appendChild($dom->createElement("images", $images));
		$parent_node->appendChild($dom->createElement("header_title", $header_title));
		$parent_node->appendChild($dom->createElement("header_text", $header_text));
		$parent_node->appendChild($dom->createElement("header_link", $header_link));
		$parent_node->appendChild($dom->createElement("header_status", "1"));
		$dom->appendChild($parent_node);
		
		$domx = new DomDocument;
		$domx->preserveWhiteSpace = false;
		$domx->validateOnParse = true;
		$domx->formatOutput = true;
		$domx->load($XML);
		$xpath = new DomXPath($domx);
		$nodelist = $xpath->query("/headers/header[@id={$id}]");		
		$oldnode = $nodelist->item(0);					
		// Load the $parent document fragment into the $dom
		$newnode = $domx->importNode($dom->documentElement, true);
		// Replace
		$oldnode->parentNode->replaceChild($newnode, $oldnode);
		$domx->save($XML) or die("Error");
	}
	function delete($id)
	{
		$data = $this->getById($id);
		@unlink("../".$data->images);		
		$XML = $this->pathXML."header.xml";
		$dom = new DomDocument;
		$dom->preserveWhiteSpace = false;
		$dom->validateOnParse = true;
		$dom->formatOutput = true;
		$dom->load($XML);
		// Locate the old product node to modify
		$xpath = new DOMXpath($dom);
		$nodelist = $xpath->query("/headers/header[@id={$id}]");
		$oldnode = $nodelist->item(0);
		// Remove
		$oldnode->parentNode->removeChild($oldnode);		   
		$dom->save($XML) or die("Error");		
	}
	function logo() {
	is_file('templates/'.TPL.'/images/logo.png') ? list($w,$h) = getimagesize('templates/'.TPL.'/images/logo.png') : $w = 174; $h=23;
	$logo = is_file('images/user/'.LOGO) ? URL.'images/user/'.LOGO : URL.'templates/'.TPL.'/images/logo.png';
		echo '<a href="'.URL.'" class="logo" title="Site Logo">
			<img src="'.$logo.'" width="'.$w.'" height="'.$h.'" border="0" id="logo" />
			<span class="tagline">'.SITE.'</span>
		</a>';
	}
	function load()
	{	
		include "function.php";
		$data = $this->get();
		usort($data, "orderID");
		echo '<ul class="slides">';
		foreach($data as $value)
		{
			echo '<li>
					<a href="'.$value->header_link.'" title="'.$value->header_title.'"  class="img-hover" >
					<img src="'.URL.$value->images.'" alt="Slider Image"></a>
				</li>';
		}
		echo '</ul>';
		echo '<ul class="slide-nav slides-5">';
		foreach($data as $value)
		{
			echo '<li>
					<h4>'.$value->header_title.'</h4>
					<p>'.$value->header_text.'</p>
				</li>';
		}
		echo '</ul>';
	}
	function up($id) {
		//id Ini
		$data = $this->getById($id);
		$oidx = $data->oid;		
		$XML = $this->pathXML."header.xml";
		$dom = new DomDocument;
		$dom->preserveWhiteSpace = false;
		$dom->validateOnParse = true;
		$dom->formatOutput = true;
		$dom->load($XML);
		$xpath = new DomXPath($dom);
		$cek = $xpath->query("//headers/header/oid[.<'$oidx']");
		if (is_object($cek) == true && is_object($cek->item(0)) == true) {
			$jml = ($cek->length)-1;
			$oid = $cek->item($jml)->nodeValue;
			for($i=0; $i<$jml; $i++) {
				if ($cek->item($i)->nodeValue > $oid) $oid = $cek->item($i)->nodeValue;
			}
			$idx = $xpath->query("//headers/header[oid='$oid']/id")->item(0)->nodeValue;
			
			$parent = new DomDocument;
			$parent_node = $parent->createElement('oid', $oid);
			$parent->appendChild($parent_node);
			$dom = new DomDocument;
			$dom->load($XML);
			$xpath = new DomXPath($dom);
			$nodelist = $xpath->query("/headers/header[@id={$id}]/oid");
			$oldnode = $nodelist->item(0);                
			$newnode = $dom->importNode($parent->documentElement, true);
			$oldnode->parentNode->replaceChild($newnode, $oldnode);
			$dom->save($XML) or die("Error");
			
			$parent = new DomDocument;
			$parent_node = $parent->createElement('oid', $oidx);
			$parent->appendChild($parent_node);
			$dom = new DomDocument;
			$dom->load($XML);
			$xpath = new DomXPath($dom);
			$nodelist = $xpath->query("/headers/header[@id={$idx}]/oid");
			$oldnode = $nodelist->item(0);                
			$newnode = $dom->importNode($parent->documentElement, true);
			$oldnode->parentNode->replaceChild($newnode, $oldnode);
			$dom->save($XML) or die("Error");
		}
	}
	function down($id) {
		//id Ini
		$data = $this->getById($id);
		$oidx = $data->oid;
		
		$XML = $this->pathXML."header.xml";
		$dom = new DomDocument;
		$dom->preserveWhiteSpace = false;
		$dom->validateOnParse = true;
		$dom->formatOutput = true;
		$dom->load($XML);
		$xpath = new DomXPath($dom);
		$cek = $xpath->query("//headers/header/oid[.>'$oidx']");
		if (is_object($cek) == true && is_object($cek->item(0)) == true) {
			$jml = ($cek->length)-1;
			$oid = $cek->item($jml)->nodeValue;
			for($i=0; $i<$jml; $i++) {
				if ($cek->item($i)->nodeValue < $oid) $oid = $cek->item($i)->nodeValue;
			}
			$idx = $xpath->query("//headers/header[oid='$oid']/id")->item(0)->nodeValue;			
			$parent = new DomDocument;
			$parent_node = $parent->createElement('oid', $oid);
			$parent->appendChild($parent_node);
			$dom = new DomDocument;
			$dom->load($XML);
			$xpath = new DomXPath($dom);
			$nodelist = $xpath->query("/headers/header[@id={$id}]/oid");
			$oldnode = $nodelist->item(0);                
			$newnode = $dom->importNode($parent->documentElement, true);
			$oldnode->parentNode->replaceChild($newnode, $oldnode);
			$dom->save($XML) or die("Error");
			
			$parent = new DomDocument;
			$parent_node = $parent->createElement('oid', $oidx);
			$parent->appendChild($parent_node);
			$dom = new DomDocument;
			$dom->load($XML);
			$xpath = new DomXPath($dom);
			$nodelist = $xpath->query("/headers/header[@id={$idx}]/oid");
			$oldnode = $nodelist->item(0);                
			$newnode = $dom->importNode($parent->documentElement, true);
			$oldnode->parentNode->replaceChild($newnode, $oldnode);
			$dom->save($XML) or die("Error");
		}
	}
}  
?>
