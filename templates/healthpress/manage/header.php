<?PHP
include 'libraries.php';
include 'function.php';
$header = new header();
$header->pathXML = "../templates/".$tpl."/temp/";
$header->path = "templates/".$tpl."/slides/";
$header->DIR = "../templates/".$tpl."/slides/";
if (isset($_POST["submit"])){
	$data["images"] = $_FILES["images"];
	$data["title"] = $_POST["title"];
	$data["text"] = $_POST["text"];
	$data["link"] = $_POST["link"];
	$header = new header($data);
	$header->pathXML = "../templates/".$tpl."/temp/";
	$header->path = "templates/".$tpl."/slides/";
	$header->DIR = "../templates/".$tpl."/slides/";
	if (isset($_POST["add_header"])) $header->add();
	else if (isset($_POST["edit_header"])) {
		$id = $_POST["edit_header"];
		$header->edit($id);
	}
	header("location: ".$_SERVER['HTTP_REFERER']);		
}else if (isset($_GET["del"])) {
	$id = $_GET["del"];
	$header->delete($id);
	header('location: '.URL.'manage/setting.php?act=header');
}else if (isset($_GET["up"])) {
	$id = $_GET["up"];
	$header->up($id);
	header('location: '.URL.'manage/setting.php?act=header');
}else if (isset($_GET["down"])) {
	$id = $_GET["down"];
	$header->down($id);
	header('location: '.URL.'manage/setting.php?act=header');
}
?>
<div class="adminContent">
<?PHP
if (isset($_GET["add_header"])) $title = "Add Header";
else if (isset($_GET["edit_header"])) $title = "Edit Header";
else $title = "Header Setting";
if (isset($_GET["add_header"]) || isset($_GET["edit_header"])):
if (isset($_GET["add_header"])) {
$act = "add_header";
$id = "";
$data = new ArrayObject();
$data->images = "";
$data->header_title = "";
$data->header_text = "";
$data->header_link = "";
} else {
$id = $_GET["edit_header"];
$act = "edit_header";
$data = $header->getById($id);
}
echo "<h2>$title</h2>";
?>
	<form id="general" method="POST" action="<?PHP echo URL.'manage/setting.php?act=header'; ?>" enctype="multipart/form-data">
		<?PHP 
		if (isset($_GET["edit_header"])) {
			$thumbnail = URL.'thumb?src='.URL.$data->images.'&w=500&h=200';
			echo '<p><img src="'.$thumbnail.'" alt="alternatif text"/></p>';
		}
		?>
    	 <p>
        	<label>Upload Images (980x445 pixel)</label>
            <input type="file" name="images" class="input small" />
        </p>
		<p id="text">
			<label>Title</label>
			<textarea name="title" class="input long" maxlength="20"><?PHP echo $data->header_title; ?></textarea>
		</p>
		<p id="alt_text">
			<label>Text</label>
			<textarea name="text" class="input long" maxlength="30"><?PHP echo $data->header_text; ?></textarea>
		</p>
		<p>
			<label>URL Link</label>
			<input type="text" name="link" class="input medium" value="<?PHP echo $data->header_link; ?>" />
		</p>
        <p>
			<input type="hidden" name="<?PHP echo $act; ?>" value="<?PHP echo $id; ?>" class="submit" />
            <input type="submit" name="submit" value="Save Changes" class="submit"/>
        </p>
    </form>
<?PHP
else:
$getData = $header->get();
usort($getData, "orderID");
$jumlah = count($getData);
?>
<h2>
	<?PHP echo $title; 
	if ($jumlah < 5) :
	?>
	<a href="javascript:void(0);" class="openmodalbox gradientButton" >
	<input type="hidden" name="ajaxhref" value="<?PHP echo URL.'manage/setting.php?act=header&add_header'; ?>"/>Add New</a>
	<?PHP endif; ?>
</h2>
<table id="dataTable" cellpadding="0" cellspacing="0">				
	<thead>
		<tr>
			<th width="45%">Images</th>
			<th width="30%">Title</th>
			<th width="20%">Link</th>
			<th width="5%"></th>
		</tr>
	</thead>					
	
	<tbody>
		<?php					
		foreach ($getData as $data)
		{
			$thumbnail = URL.'thumb?src='.URL.$data->images.'&w=200&h=75';
			echo '<tr>
				<td valign="center"><img src="'.$thumbnail.'" alt="alternatif text"/></td>
				<td valign="center"><a href="#"><strong>'.$data->header_title.'</strong></a><br />
				<div class="post-action">
				<span class="edit-post"><a href="javascript:void(0);" class="openmodalbox" >
					<input type="hidden" name="ajaxhref" value="'.URL.'manage/setting.php?act=header&edit_header='.$data->id.'" />Edit</a> </span> | <span class="trash-post"><a href="javascript:void(0);" class="openmodalbox" >
					<input type="hidden" name="ajaxhref" value="'.URL.'manage/setting.php?act=header&del='.$data->id.'" />Trash</a> </span>
				</div></td>							
				<td valign="center">'.$data->header_link.'</td>
				<td><ul id="orderpage">
					<li>
					<a class="openmodalbox pageUp" href="javascript:void(0);"><input type="hidden" name="ajaxhref" value="'.$url.'manage/setting.php?act=header&up='.$data->id.'"><span>Up</span></a></li> 
					<li>
					<li><a class="openmodalbox pageDown" href="javascript:void(0);"><input type="hidden" name="ajaxhref" value="'. $url .'manage/setting.php?act=header&down='.$data->id.'"><span>Down</span></a></li></ul>
				</td>
			</tr>';
		}
		?>										
	</tbody>
</table>
<?PHP
endif;
?>
</div>