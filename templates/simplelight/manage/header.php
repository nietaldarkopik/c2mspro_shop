<?PHP
include 'libraries.php';
include 'function.php';
$header = new header();
$header->pathXML = "../templates/".$tpl."/temp/";
$header->path = "templates/".$tpl."/slides/";
$header->DIR = "../templates/".$tpl."/slides/";
if (isset($_POST["submit"])){
	$data["type"] = $_POST["Header_type"];
	$data["bg"] = $_FILES["bg"];
	$data["img"] = $_FILES["img"];
	$data["swf"] = $_FILES["swf"];
	$data["video"] = $_POST["video"];
	$data["text1"]	 = $_POST["text"];
	$data["text2"]	 = $_POST["alternatif"];
	$data["text3"]	 = $_POST["alternatifx"];
	$data["link"] = $_POST["link"];
	$header = new header($data);
	$header->pathXML = "../templates/".$tpl."/temp/";
	$header->path = "templates/".$tpl."/slides/";
	$header->DIR = "../templates/".$tpl."/slides/";
	if (isset($_POST["add_header"])) $header->add();
	else if (isset($_POST["edit_header"])) {
		$id = $_POST["edit_header"];
		$header->edit($id);
	}
	header("location: ".$_SERVER['HTTP_REFERER']);		
}else if (isset($_GET["del"])) {
	$id = $_GET["del"];
	$header->delete($id);
	header('location: '.URL.'manage/setting.php?act=header');
}else if (isset($_GET["up"])) {
	$id = $_GET["up"];
	$header->up($id);
	header('location: '.URL.'manage/setting.php?act=header');
}else if (isset($_GET["down"])) {
	$id = $_GET["down"];
	$header->down($id);
	header('location: '.URL.'manage/setting.php?act=header');
}
?>
<div class="adminContent">
<?PHP
if (isset($_GET["add_header"])) $title = "Add Header";
else if (isset($_GET["edit_header"])) $title = "Edit Header";
else $title = "Header Setting";
if (isset($_GET["add_header"]) || isset($_GET["edit_header"])):
if (isset($_GET["add_header"])) {
$act = "add_header";
$id = "";
$data = new ArrayObject();
$data->header_bg = "";
$data->header_content = "";
$data->Header_type = "img";
$data->header_text = "";
$data->header_text2 = "";
$data->header_text3 = "";
$data->header_link = "";
} else {
$id = $_GET["edit_header"];
$act = "edit_header";
$data = $header->getById($id);
}
echo "<h2>$title</h2>";
?>
	<script type="text/javascript">
		function showAdd(val) {
			if (val== "img") {
				$("#img").show();
				$("#text").show();
				$("#alt_text").show();
				$("#swf").hide();
				$("#video").hide();
			}
			else if (val== "video") {
				$("#video").show();
				$("#text").show();
				$("#alt_text").show();
				$("#img").hide();
				$("#swf").hide(); 
			}
			else if (val== "swf") {
				$("#swf").show();
				$("#img").hide();
				$("#video").hide();
				$("#text").hide();
				$("#alt_text").hide();
			}
		}
	</script>
	<form id="general" method="POST" action="<?PHP echo URL.'manage/setting.php?act=header'; ?>" enctype="multipart/form-data">
		<p>
        	<label>Select Type</label>
				<select name="Header_type" id="Header_type" class="small" onchange="showAdd(this.value);">
					<option value="img" <?PHP echo ($data->Header_type == "img") ? "selected=\"selected\"" : ""; ?>> img</option>
					<option value="video" <?PHP echo ($data->Header_type == "video") ? "selected=\"selected\"" : ""; ?>>video</option>
					<option value="swf" <?PHP echo ($data->Header_type == "swf") ? "selected=\"selected\"" : ""; ?>>swf</option>
				</select>
        </p>
		<script type="text/javascript">
			$(document).ready(function(){	
				showAdd("<?PHP echo $data->Header_type;?>");
			});	
		</script>
		<?PHP 
		if (isset($_GET["edit_header"])) {
			$thumbnail = URL.'thumb?src='.URL.$data->header_bg.'&w=500&h=200';
			echo '<p><img src="'.$thumbnail.'" alt="alternatif text"/></p>';
		}
		?>
    	 <p>
        	<label>Upload Background</label>
            <input type="file" name="bg" class="input small" />
        </p>
        <p id="img">
			<?PHP
			if (isset($_GET["edit_header"])) {
				if ($data->Header_type == "img") {
					$thumbnail = URL.'thumb?src='.URL.$data->header_content.'&w=500&h=200';
					echo '<img src="'.$thumbnail.'" alt="alternatif text"/>';
				}
			}
			?>
        	<label>Upload Images</label>
            <input type="file" name="img" class="input small" />
        </p>
		<p id="swf" style="display:none;">
        	<label>Upload Swf</label>
            <input type="file" name="swf" class="input small" />
        </p>
		<p id="video" style="display:none;">
        	<label>Embed Object</label>
            <textarea name="video" class="input long" ><?PHP echo stripslashes(urldecode($data->header_content)); ?></textarea>
        </p>
		<p id="text">
			<label>Text</label>
			<textarea name="text" class="input long" ><?PHP echo $data->header_text; ?></textarea>
		</p>
		<p id="alt_text">
			<label>Alternatif Text</label>
			<textarea name="alternatif" class="input long" ><?PHP echo $data->header_text2; ?></textarea>
		</p>
		<p id="alt_text">
			<label>Alternatif Text #2</label>
			<textarea name="alternatifx" class="input long" ><?PHP echo $data->header_text3; ?></textarea>
		</p>
		<p>
			<label>URL Link</label>
			<input type="text" name="link" class="input medium" value="<?PHP echo $data->header_link; ?>" />
		</p>
        <p>
			<input type="hidden" name="<?PHP echo $act; ?>" value="<?PHP echo $id; ?>" class="submit" />
            <input type="submit" name="submit" value="Save Changes" class="submit"/>
        </p>
    </form>
<?PHP
else:
$getData = $header->get();
usort($getData, "orderID");
?>
<h2>
	<?PHP echo $title; ?>
	<a href="javascript:void(0);" class="openmodalbox gradientButton" >
	<input type="hidden" name="ajaxhref" value="<?PHP echo URL.'manage/setting.php?act=header&add_header'; ?>"/>Add New</a>
</h2>
<table id="dataTable" cellpadding="0" cellspacing="0">				
	<thead>
		<tr>
			<th width="45%">Background</th>
			<th width="30%">Type</th>
			<th width="20%">Link</th>
			<th width="5%"></th>
		</tr>
	</thead>					
	
	<tbody>
		<?php					
		foreach ($getData as $data)
		{
			$thumbnail = URL.'thumb?src='.URL.$data->header_bg.'&w=200&h=75';
			echo '<tr>
				<td valign="center"><img src="'.$thumbnail.'" alt="alternatif text"/></td>
				<td valign="center"><a href="#"><strong>'.$data->Header_type.'</strong></a><br />
				<div class="post-action">
				<span class="edit-post"><a href="javascript:void(0);" class="openmodalbox" >
					<input type="hidden" name="ajaxhref" value="'.URL.'manage/setting.php?act=header&edit_header='.$data->id.'" />Edit</a> </span> | <span class="trash-post"><a href="javascript:void(0);" class="openmodalbox" >
					<input type="hidden" name="ajaxhref" value="'.URL.'manage/setting.php?act=header&del='.$data->id.'" />Trash</a> </span>
				</div></td>							
				<td valign="center">'.$data->header_link.'</td>
				<td><ul id="orderpage">
					<li>
					<a class="openmodalbox pageUp" href="javascript:void(0);"><input type="hidden" name="ajaxhref" value="'.$url.'manage/setting.php?act=header&up='.$data->id.'"><span>Up</span></a></li> 
					<li>
					<li><a class="openmodalbox pageDown" href="javascript:void(0);"><input type="hidden" name="ajaxhref" value="'. $url .'manage/setting.php?act=header&down='.$data->id.'"><span>Down</span></a></li></ul>
				</td>
			</tr>';
		}
		?>										
	</tbody>
</table>
<?PHP
endif;
?>
</div>