<?php
class header
{
	private $type = "";
	private $bg = "";
	private $img = "";
	private $swf = "";
	private $video = "";
	private $text1 = "";
	private $text2 = "";
	private $text3 = "";
	private $link = "";
	public $DIR = './../images/slides/';
	public $path = 'images/slides/';
	public $pathXML = '/';
	
	function  __construct($array="")
	{
		if (is_array($array)) {
			isset($array["type"]) ? $this->type = $array["type"] : "";
			isset($array["bg"]) ? $this->bg = $array["bg"] : "";
			isset($array["img"]) ? $this->img = $array["img"] : "";
			isset($array["swf"]) ? $this->swf = $array["swf"] : "";
			isset($array["video"]) ? $this->video = urlencode($array["video"]) : "";
			isset($array["text1"]) ? $this->text1 = $array["text1"] : "";
			isset($array["text2"]) ? $this->text2 = $array["text2"] : "";
			isset($array["text3"]) ? $this->text3 = $array["text3"] : "";
			isset($array["link"]) ? $this->link = $array["link"] : "";
		}
	}
	function get() {
		$hasil = array();
		$pathXML = $this->pathXML."header.xml";
		$dataXML = simplexml_load_file($pathXML);
		foreach ($dataXML->header as $data) {
			$hasil[] = $data;
		}
	return $hasil;
	}
	function getById($id) {
		$XML = $this->pathXML."header.xml";
		$dom = new DomDocument;
		$dom->validateOnParse = true;
		$dom->load($XML);
		// Locate the old product node to modify
		$xpath = new DOMXPath($dom);
		$data = $xpath->query("//*[@id='$id']")->item(0);	
		$data = $dom->saveXML($data);
		$data = simplexml_load_string($data);
		return $data;
	}
	function add()
	{	
		$uploaddir = $this->DIR;
		$Header_type = $this->type;
		if ($this->bg["name"] !="")
		{
			$header_bg = "bg_".$this->bg["name"];
			move_uploaded_file($this->bg["tmp_name"], $uploaddir.$header_bg);
			$header_bg = $this->path.$header_bg;
		}
		if ($this->type == "img") {	
			if ($this->img["name"] !="")
			{
				$header_content = "img_".$this->img["name"];
				move_uploaded_file($this->img["tmp_name"], $uploaddir.$header_content);
				$header_content = $this->path.$header_content;
			}
		}else if ($this->type == "video") {
			$header_content = $this->video;
		}else if ($this->type == "swf") {
			if ($this->swf["name"] !="")
			{
				$header_content = "swf_".$this->swf["name"];
				move_uploaded_file($this->swf["tmp_name"], $uploaddir.$header_content);
				$header_content = $this->path.$header_content;
			}
		}
		$header_text = $this->text1;
		$header_text2 = $this->text2;
		$header_text3 = $this->text3;
		$header_link = ($this->link == "") ? "#" : $this->link;	
	
		$XML = $this->pathXML."header.xml";
		$dom = new DomDocument;
		$dom->preserveWhiteSpace = false;
		$dom->validateOnParse = true;
		$dom->formatOutput = true;
		$dom->load($XML);
		$xpath = new DomXPath($dom);
		$id = $xpath->query('//headers/header[not(../header/id > id)]/id')->item(0)->nodeValue;
		$oid = $xpath->query('//headers/header[not(../header/oid > oid)]/oid')->item(0)->nodeValue;
		$parent = $xpath->query("//headers"); 
		$parent_node = $dom->createElement('header');
		$attribute = $dom->createAttribute("id");
		$attribute->value = $id+1;
		$parent_node->appendChild($attribute);
		$parent_node->appendChild($dom->createElement("id", $id+1));
		$parent_node->appendChild($dom->createElement("oid", $oid+1));
		$parent_node->appendChild($dom->createElement("Header_type", $Header_type));
		$parent_node->appendChild($dom->createElement("header_bg", $header_bg));
		$parent_node->appendChild($dom->createElement("header_content", $header_content));
		$parent_node->appendChild($dom->createElement("header_text", $header_text));
		$parent_node->appendChild($dom->createElement("header_text2", $header_text2));
		$parent_node->appendChild($dom->createElement("header_text3", $header_text3));
		$parent_node->appendChild($dom->createElement("header_link", $header_link));
		$parent_node->appendChild($dom->createElement("header_status", "1"));
		$dom->appendChild($parent_node);
		$parent->item(0)->insertBefore($parent_node); 
		$dom->save($XML) or die("Error");
	}
	function edit($id)
	{
		$uploaddir = $this->DIR;
		$Header_type = $this->type;
		$data = $this->getById($id);
		if ($this->bg["name"] !="")
		{
			@unlink("../".$data->header_bg);
			$header_bg = "bg_".$this->bg["name"];
			move_uploaded_file($this->bg["tmp_name"], $uploaddir.$header_bg);
			$header_bg = $this->path.$header_bg;
		}else $header_bg = $data->header_bg;
		if ($this->type == "img") {		
			if ($this->img["name"] !="")
			{
				@unlink("../".$data->header_content);
				$header_content = "img_".$this->img["name"];
				move_uploaded_file($this->img["tmp_name"], $uploaddir.$header_content);
				$header_content = $this->path.$header_content;
			} else $header_content = $data->header_content;
		}else if ($this->type == "video") {
			if ($this->video !="") {
				@unlink("../".$data->header_content);
				$header_content = $this->video;
			} else $header_content = $data->header_content;
		}else if ($this->type == "swf") {
			if ($this->swf["name"] !="")
			{
				@unlink("../".$data->header_content);
				$header_content = "swf_".$this->swf["name"];
				move_uploaded_file($this->swf["tmp_name"], $uploaddir.$header_content);
				$header_content = $this->path.$header_content;
			} else $header_content = $data->header_content;
		}
		$header_text = $this->text1;
		$header_text2 = $this->text2;
		$header_text3 = $this->text3;
		$header_link = ($this->link == "") ? "#" : $this->link;		
		
		$XML = $this->pathXML."header.xml";
		$dom = new DomDocument;
		$parent_node = $dom->createElement('header');
		$attribute = $dom->createAttribute("id");
		$attribute->value = $id;
		$parent_node->appendChild($attribute);
		$parent_node->appendChild($dom->createElement("id", $id));
		$parent_node->appendChild($dom->createElement("oid", $data->oid));
		$parent_node->appendChild($dom->createElement("Header_type", $Header_type));
		$parent_node->appendChild($dom->createElement("header_bg", $header_bg));
		$parent_node->appendChild($dom->createElement("header_content", $header_content));
		$parent_node->appendChild($dom->createElement("header_text", $header_text));
		$parent_node->appendChild($dom->createElement("header_text2", $header_text2));
		$parent_node->appendChild($dom->createElement("header_text3", $header_text3));
		$parent_node->appendChild($dom->createElement("header_link", $header_link));
		$parent_node->appendChild($dom->createElement("header_status", "1"));
		$dom->appendChild($parent_node);
		
		$domx = new DomDocument;
		$domx->preserveWhiteSpace = false;
		$domx->validateOnParse = true;
		$domx->formatOutput = true;
		$domx->load($XML);
		$xpath = new DomXPath($domx);
		$nodelist = $xpath->query("/headers/header[@id={$id}]");		
		$oldnode = $nodelist->item(0);					
		// Load the $parent document fragment into the $dom
		$newnode = $domx->importNode($dom->documentElement, true);
		// Replace
		$oldnode->parentNode->replaceChild($newnode, $oldnode);
		$domx->save($XML) or die("Error");
	}
	function delete($id)
	{
		$data = $this->getById($id);
		@unlink("../".$data->header_content);
		@unlink("../".$data->header_bg);
		
		$XML = $this->pathXML."header.xml";
		$dom = new DomDocument;
		$dom->preserveWhiteSpace = false;
		$dom->validateOnParse = true;
		$dom->formatOutput = true;
		$dom->load($XML);
		// Locate the old product node to modify
		$xpath = new DOMXpath($dom);
		$nodelist = $xpath->query("/headers/header[@id={$id}]");
		$oldnode = $nodelist->item(0);
		// Remove
		$oldnode->parentNode->removeChild($oldnode);		   
		$dom->save($XML) or die("Error");		
	}
	function orderID($a, $b) {
		return strcmp($a->oid, $b->oid);
	}
	function load()
	{	
		include "function.php";
		$data = $this->get();
		usort($data, "orderID");
		foreach($data as $data)
		{
			$x = 0; $y = 200; $speed = 400; $start = 500;
			if ($data->Header_type == "img") :
			echo '<li data-transition="fade" data-slotamount="10"> 										
					<img src="'.URL.$data->header_bg.'" alt="">';
					
					if ($data->header_content != "")
					{
					echo '<div class="caption lfr" data-x="365" data-y="20" data-speed="'.$speed.'" data-start="'.$start.'" >
					<img src="'.URL.$data->header_content.'" alt=""></div>';
					$start += $speed+50;
					}
					
					if ($data->header_text != "") {
					echo '<div class="caption sft big_black"  data-x="'.$x.'" data-y="'.$y.'" data-speed="'.$speed.'" data-start="'.$start.'" data-easing="easeOutBack">'.$data->header_text.'</div>';
					$start +=$speed+50; $y +=50;
					}
					
					if ($data->header_text2 != "")
					{
					echo '<div class="caption sfb medium_dark_back"  data-x="'.$x.'" data-y="'.$y.'" data-speed="'.$speed.'" data-start="'.$start.'" data-easing="easeOutExpo">'.$data->header_text2.'</div>';
					$start +=$speed+50; $y +=50;
					}
					
					if ($data->header_text3 != "")
					{
					echo '<div class="caption sfb medium_text"  data-x="'.$x.'" data-y="'.$y.'" data-speed="'.$speed.'" data-start="'.$start.'" data-easing="easeOutExpo">'.$data->header_text3.'</div>';
					$start +=$speed+50; $y +=50;
					}
					
					if ($data->header_link != "#") 
					echo '<div class="caption sfb" data-x="'.$x.'" data-y="'.$y.'" data-speed="'.$speed.'" data-start="'.$start.'"  data-easing="easeOutExpo"><a class="button dark small" href="'.$data->header_link.'">More</a></div>';
			echo '</li>';
			elseif ($data->Header_type == "video") :
			$x = 500; $y = 200; $speed = 400; $start = 500;
			echo '<li data-transition="slideright" data-slotamount="10"> 	
					<img src="'.URL.$data->header_bg.'" alt="">';
					if ($data->header_content != "")
					{
					echo '<div class="caption lfb boxshadow" data-x="-40" data-y="80" data-speed="'.$speed.'" data-start="'.$start.'" data-easing="easeOutBack">
						'.stripslashes(urldecode($data->header_content)).'
					</div>';
					$start +=$speed+50;
					}
					
					if ($data->header_text != "") {
					echo '<div class="caption sft big_black"  data-x="'.$x.'" data-y="'.$y.'" data-speed="'.$speed.'" data-start="'.$start.'" data-easing="easeOutBack">'.$data->header_text.'</div>';
					$start +=$speed+50; $y +=50;
					}
					
					if ($data->header_text2 != "")
					{
					echo '<div class="caption sfb medium_dark_back"  data-x="'.$x.'" data-y="'.$y.'" data-speed="'.$speed.'" data-start="'.$start.'" data-easing="easeOutExpo">'.$data->header_text2.'</div>';
					$start +=$speed+50; $y +=50;
					}
					
					if ($data->header_text3 != "")
					{
					echo '<div class="caption sfb medium_text"  data-x="'.$x.'" data-y="'.$y.'" data-speed="'.$speed.'" data-start="'.$start.'" data-easing="easeOutExpo">'.$data->header_text3.'</div>';
					$start +=$speed+50; $y +=50;
					}
					
					if ($data->header_link != "#") 
					echo '<div class="caption sfb" data-x="'.$x.'" data-y="'.$y.'" data-speed="'.$speed.'" data-start="'.$start.'" data-easing="easeOutExpo"><a class="button dark small" href="'.$data->header_link.'">More</a></div>';
			echo '</li>';
			
			elseif ($data->Header_type == "swf") :
				echo '<li data-transition="fade" data-slotamount="10"> 
						<img src="images/slides/'.$data->header_bg.'" alt="">
						<embed src="'.$data->header_content.'" width="'.$w.'" height="'.$h.'" />';
				echo '</li>';
			endif;
		}					
	}
	function up($id) {
		//id Ini
		$data = $this->getById($id);
		$oidx = $data->oid;
		
		$XML = $this->pathXML."header.xml";
		$dom = new DomDocument;
		$dom->preserveWhiteSpace = false;
		$dom->validateOnParse = true;
		$dom->formatOutput = true;
		$dom->load($XML);
		$xpath = new DomXPath($dom);
		$cek = $xpath->query("//headers/header/oid[.<'$oidx']");
		if (is_object($cek) == true && is_object($cek->item(0)) == true) {
			$jml = ($cek->length)-1;
			$oid = $cek->item($jml)->nodeValue;
			for($i=0; $i<$jml; $i++) {
				if ($cek->item($i)->nodeValue > $oid) $oid = $cek->item($i)->nodeValue;
			}
			$idx = $xpath->query("//headers/header[oid < '$oidx']/id")->item(0)->nodeValue;
			$oid = $cek->item(0)->nodeValue;
			
			$parent = new DomDocument;
			$parent_node = $parent->createElement('oid', $oid);
			$parent->appendChild($parent_node);
			$dom = new DomDocument;
			$dom->load($XML);
			$xpath = new DomXPath($dom);
			$nodelist = $xpath->query("/headers/header[@id={$id}]/oid");
			$oldnode = $nodelist->item(0);                
			$newnode = $dom->importNode($parent->documentElement, true);
			$oldnode->parentNode->replaceChild($newnode, $oldnode);
			$dom->save($XML) or die("Error");
			
			$parent = new DomDocument;
			$parent_node = $parent->createElement('oid', $oidx);
			$parent->appendChild($parent_node);
			$dom = new DomDocument;
			$dom->load($XML);
			$xpath = new DomXPath($dom);
			$nodelist = $xpath->query("/headers/header[@id={$idx}]/oid");
			$oldnode = $nodelist->item(0);                
			$newnode = $dom->importNode($parent->documentElement, true);
			$oldnode->parentNode->replaceChild($newnode, $oldnode);
			$dom->save($XML) or die("Error");
		}
	}
	function down($id) {
		//id Ini
		$data = $this->getById($id);
		$oidx = $data->oid;
		
		$XML = $this->pathXML."header.xml";
		$dom = new DomDocument;
		$dom->preserveWhiteSpace = false;
		$dom->validateOnParse = true;
		$dom->formatOutput = true;
		$dom->load($XML);
		$xpath = new DomXPath($dom);
		$cek = $xpath->query("//headers/header/oid[.>'$oidx']");
		if (is_object($cek) == true && is_object($cek->item(0)) == true) {
			$jml = ($cek->length)-1;
			$oid = $cek->item($jml)->nodeValue;
			for($i=0; $i<$jml; $i++) {
				if ($cek->item($i)->nodeValue < $oid) $oid = $cek->item($i)->nodeValue;
			}
			$idx = $xpath->query("//headers/header[oid > '$oidx']/id")->item(0)->nodeValue;
			$oid = $cek->item(0)->nodeValue;
			
			$parent = new DomDocument;
			$parent_node = $parent->createElement('oid', $oid);
			$parent->appendChild($parent_node);
			$dom = new DomDocument;
			$dom->load($XML);
			$xpath = new DomXPath($dom);
			$nodelist = $xpath->query("/headers/header[@id={$id}]/oid");
			$oldnode = $nodelist->item(0);                
			$newnode = $dom->importNode($parent->documentElement, true);
			$oldnode->parentNode->replaceChild($newnode, $oldnode);
			$dom->save($XML) or die("Error");
			
			$parent = new DomDocument;
			$parent_node = $parent->createElement('oid', $oidx);
			$parent->appendChild($parent_node);
			$dom = new DomDocument;
			$dom->load($XML);
			$xpath = new DomXPath($dom);
			$nodelist = $xpath->query("/headers/header[@id={$idx}]/oid");
			$oldnode = $nodelist->item(0);                
			$newnode = $dom->importNode($parent->documentElement, true);
			$oldnode->parentNode->replaceChild($newnode, $oldnode);
			$dom->save($XML) or die("Error");
		}
	}
}  
?>
