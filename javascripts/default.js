function toggle(ID)
{
	var e = document.getElementById(ID);
	if(e.style.display =='none')
	{
		e.style.display='block';
	}
	else
	{
		e.style.display='none';
	}
}

function show(ID,OVERLAY)
{
	var e = document.getElementById(ID);
	e.style.display='block';

	if(OVERLAY == true)
	{
		var f	= document.createElement('div');
		var id	= 'overlay';

		f.setAttribute('id',id); 
		f.style.top			= '0px';   
		f.style.left		= '0px';   
        f.style.margin		= '0px';      
		f.style.background	= '#000000';	
		f.style.position	= 'fixed';	
		f.style.width		= '100%';
		f.style.height		= '100%';
		f.style.zindex		= '1000';
		f.style.opacity		= '.5';
		f.style.filter		= 'progid:DXImageTransform.Microsoft.BasicImage(opacity=0.5)';
        document.body.appendChild(f);
	}
}	


function hide(ID,OVERLAY)
{
	var e = document.getElementById(ID);
	e.style.display='none';	
	
	if(OVERLAY == true)
	{
		var f=document.getElementById('overlay'); 
		f.style.zindex		= '1000';
		document.body.removeChild(f); 
	}
}		
function postconfirm(TEXT,POST,ACTION,ACTIONNAME)
{
		var f	= document.createElement('div');
		var id2	= 'myalert';

		f.setAttribute('id',id2); 
		f.style.top			= '0px';   
		f.style.left		= '0px';   
        f.style.margin		= '0px';      
		f.style.background	= '#000000';	
		f.style.position	= 'fixed';	
		f.style.width		= '100%';
		f.style.height		= '100%';
		f.style.zindex		= '100000';
		f.style.opacity		= '.5';
		f.style.filter		= 'progid:DXImageTransform.Microsoft.BasicImage(opacity=0.5)';
        document.body.appendChild(f);

		var g	= document.createElement('div');		
		var id1	= 'mytext';

		g.setAttribute('id',id1); 		
		g.style.left		= '50%';   
		g.style.marginLeft	= '-170px'; 
		g.style.width		= '340px';
		
		g.style.top			= '25%';   
		g.style.background	= '#F0F0F0';			
		g.style.zindex		= '100000';
		g.style.color		= '#000000';
		g.style.position	= 'fixed';
		g.style.padding		= '0px';
		g.style.border		= '2px solid #3472C5';
		g.innerHTML			= '<form method="post" action="'+ ACTION +'" style="margin:0px;padding:0px;"><table width="100%" cellspacing="0" cellpadding="5"><tr><td colspan="2" style="background:#2E57B1;color:#FFF;font-weight:bold;padding-left:10px;">Confirm</td></tr><tr><td width="40" valign="top" align="center"><img src="images/system/confirm.png" width="32" /></td><td style="font-family:arial;font-size:12px;padding-right:10px;">'+ TEXT +'</td></tr><tr><td colspan="2" align="center"><input type="hidden" name="input" value="'+ POST +'" /><input type="hidden" name="ref" value="'+ ACTION +'" /><input type="submit" value="OK" name="'+ ACTIONNAME +'" style="padding:0px;margin-bottom:5px;width:60px;cursor:pointer;font-family:arial;font-size:11px;" />&nbsp;<input type="button" value="Cancel" onclick="hidealert()" style="padding:0px;margin-bottom:5px;width:60px;cursor:pointer;font-family:arial;font-size:11px;" /></td></tr></table></form>';
        document.body.appendChild(g);
}

function bgcolor(ID,BG)
{
	var e = document.getElementById(ID);
	e.style.background=BG;
}

function fontcolor(ID,COLOR)
{
	var e = document.getElementById(ID);
	e.style.color=COLOR;
}


function setborder(ID,VAL)
{
	var e = document.getElementById(ID);
	e.style.borderWidth=VAL;
	e.style.borderColor='#FF0000';
	e.style.borderStyle='solid';
	e.style.margin='-1px';
}

function border(ID,VAL)
{
	var e = document.getElementById(ID);
	e.style.border=VAL;
}

function unsetborder(ID,VAL)
{
	var e = document.getElementById(ID);
	e.style.background='';
	e.style.borderWidth=VAL;	
	e.style.margin='0px';
}

function padding(ID,VAL)
{
	var e = document.getElementById(ID);
	e.style.padding=VAL;
}

function margin(ID,VAL)
{
	var e = document.getElementById(ID);
	e.style.margin=VAL;
}

function popup(URL,W,H) 
{
	id= window.open(URL,'c2mspro','toolbar=no,scrollbars=yes,location=no,statusbar=no,menubar=no,resizable=no,left=5,top=5');
	id.focus();
	id.resizeTo(W,H);	
}


function req(URL,DIV) 
{		
	var xmlhttp=false;

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') 
	{
	  xmlhttp = new XMLHttpRequest();
	}

	xmlhttp.open("GET",URL, true);
	xmlhttp.onreadystatechange=function() 
	{
		if (xmlhttp.readyState==1) 
		{			
			   document.getElementById(DIV).innerHTML = '<div style="margin-left:auto;margin-right:auto;width:100px;font-family:arial;font-size:11px;color:#F87878;text-align:center;"><img src="indicator.gif" /><br />loading content...</div>';			
		}		
		else if (xmlhttp.readyState==4) 
		{
			 if(xmlhttp.status==200)
			 {
			   document.getElementById(DIV).innerHTML = xmlhttp.responseText;
			 }
			 else
			 {				
			    document.getElementById(DIV).innerHTML = 'Cannot Display The Content';			   
			 }
		}			
	}

	xmlhttp.send(null)
	return false;
}

var id1=0;
function addimg(ID)
{
	id1 = id1 + 1;
	var n = document.getElementById(ID);
	n.innerHTML += '<table align=\'left\' cellspacing=\'0\' cellpadding=\'0\'><tr><td><div style=\'position:relative;width:170px;height:50px;margin:5px 10px 10px 0px;\'><input type=\'file\' size=\'5\' name=\'slide[]\' style=\'margin-top:-65px;-moz-opacity:0;filter:alpha(opacity:0);opacity:0;z-index:2;font-size:150%;\' onchange="var img=document.getElementById(\'' +id1 +'\');img.src=this.files[0].getAsDataURL();" /><div style=\'top:0px;left:0px;z-index:1;\'><img src=\'./../images/system/header3.gif\' id='+ id1 +' width=\'170\' height=\'50\' style=\'left:0px;\' /></div></div></td></tr></table>';
}