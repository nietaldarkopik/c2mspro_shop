<?PHP
	$dirbanner = '../../images/banner';
	$pathbanner = 'images/banner';
	$query = $this->mysqli->query("select * FROM widgets_banner");
?>
<ul>
	<?PHP 
	while($banner = $query->fetch_object())
	{
		$thumbnail = URL.'thumb?src='.URL.$pathbanner.'/'.$banner->banner_img.'&w='.$banner->banner_width.'&h='.$banner->banner_height;
		$link = $banner->banner_link;
		echo '<li>
				<div class="bordered alignleft">
					<figure class="add-border">
						<a href="'.$link .'" target="_blank">
							<img src="'.$thumbnail.'" alt="'.$banner->banner_title.'" />
						</a>
					</figure>
				</div><!--/ .bordered-->
			</li>';
	}
	?>
</ul>
