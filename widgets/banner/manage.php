<?PHP 
session_start(); 
if (isset($_SESSION['c2msauth']))
{
	include '../../configs/vars.php';
	include '../../configs/database.php';
	include '../../libraries/dbconnect.php'; 
	include '../../libraries/settings.php';
	include '../../libraries/url.php';
	include '../../libraries/libraries.php';	
	$DIR_BANNER =  "../../images/banner/";
	
	if(isset($_POST['add']) || isset($_POST['edit'])) {
		$title = $_POST["banner_title"];
		$link = $_POST["banner_link"];
		$width = $_POST["banner_width"];
		$height = $_POST["banner_height"];
		if(isset($_POST['add'])) {
			$photo		= substr(md5(microtime()),27,5).'.'.ext($_FILES['thumb']['name']);
			if($_FILES['thumb']['tmp_name'] != "")
			{
				move_uploaded_file($_FILES['thumb']['tmp_name'], $DIR_BANNER.$photo);				
				$this->mysqli->query("INSERT INTO widgets_banner SET banner_img ='$photo', banner_title = '$title', banner_link ='$link', banner_width ='$width', banner_height='$height'"); 
			}
		}else{
			$bannerid = $_POST["edit"];
			$q=$this->mysqli->query("SELECT * FROM widgets_banner WHERE bannerid='".$bannerid."'");
			$data = $q->fetch_object();			
			$photo = $data->banner_img;
			
			if($_FILES['thumb']['tmp_name'] != "")
			{
				move_uploaded_file($_FILES['thumb']['tmp_name'], $DIR_BANNER.$photo);				
				$this->mysqli->query("UPDATE widgets_banner SET banner_img ='$photo', banner_title = '$title', banner_link ='$link', banner_width ='$width', banner_height='$height' WHERE bannerid='$bannerid'"); 
			}else{
				$this->mysqli->query("UPDATE widgets_banner SET banner_title = '$title', banner_link ='$link', banner_width ='$width', banner_height='$height' WHERE bannerid='$bannerid'"); 
			}
		}
		header("location: ".$_SERVER['HTTP_REFERER']);
	}
	else if(isset($_GET['del'])) {
		$bannerid = $_GET["del"];
		$q=$this->mysqli->query("SELECT * FROM widgets_banner WHERE bannerid='".$bannerid."'");
		$data = $q->fetch_object();			
		$photo = $data->banner_img;
		@unlink($DIR_BANNER.$photo);
		$this->mysqli->query("delete from widgets_banner where bannerid='$bannerid'");
		echo '<div class="adminContent">';
		include "list.php";
		echo '</div>';
	}
	else if(isset($_GET['edit']) || isset($_GET['add'])) {
		echo '<div class="adminContent">';
		include "add.php";
		echo '</div>';
	}else{
		echo '<div class="adminContent">';
		include "list.php";
		echo '</div>';
	}
}
?>