<?PHP
$dirbanner = '../../images/banner';
$pathbanner = 'images/banner';
$x = 4;
if(isset($_GET['p'])) $no = $_GET['p']; else $no = 1;
$i = ($no - 1) * $x;
$q = $mysqli->query("select * FROM widgets_banner");
$total = $q->num_rows;
$totpage = ceil($total/$x);
($totpage > 1) ? $y = $no*$x : $y = $total;
$q = $mysqli->query("select * from widgets_banner ORDER BY bannerid DESC LIMIT $i, $x");
?>
<h2>Banner Library <a href="javascript:void(0);" class="openmodalbox gradientButton" ><input type="hidden" name="ajaxhref" value="<?PHP echo URL.'widgets/banner/manage.php?add'; ?>"/>Add New</a></h2>
<table id="dataTable" cellpadding="0" cellspacing="0">				
	<thead>
		<tr>
			<th width="20%">&nbsp;</th>
			<th width="30%">File</th>
			<th width="20%">Title</th>
			<th width="30%">link</th>
		</tr>
	</thead>					
	
	<tbody>
		<?php					
		while($banner = $q->fetch_object())
		{
			$thumbnail = URL.'thumb?src='.URL.$pathbanner.'/'.$banner->banner_img.'&w=125&h=75';
			$link = $banner->banner_link;
			echo '<tr>
				<td valign="center"><img src="'.$thumbnail.'" alt="alternatif text"/></td>
				<td valign="center"><a href="#"><strong>'.$banner->banner_img.'</strong></a><br /><div class="post-action"><span class="edit-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'widgets/banner/manage.php?edit='.$banner->bannerid.'" />Edit</a></span> | <span class="trash-post"><a href="javascript:void(0);" class="openmodalbox" ><input type="hidden" name="ajaxhref" value="'.URL.'widgets/banner/manage.php?del='.$banner->bannerid.'" />Trash</a></span></div></td>							
				<td valign="center">'.$banner->banner_title.'</td>
				<td valign="center">'.$link	.'</td>
			</tr>';
		}
		?>										
	</tbody>
</table>
<div id="pag-top" class="pagination">
	<div class="pag-count">
		<?PHP echo "viewing image $no to $y (of $total image)"; ?>
	</div>
	<div class="pagination-links">
		<?PHP echo pagging($no,  $totpage, URL.'widget/banner/manage.php'); ?>
	</div>
</div>