<?PHP
	$dirBlog = '../../images/blog';
	$pathBlog = 'images/blog';
	$query = $this->mysqli->query("select href from pages WHERE content='blog' LIMIT 1");
	$href = $query->fetch_object();
	
	$query = $this->mysqli->query("select * from plugin_blog_categories");
	if ($query->num_rows !=0) :
	$q = $this->mysqli->query("select * from plugin_blog_posts join plugin_blog_categories on plugin_blog_posts.category_id=plugin_blog_categories.category_id where plugin_blog_posts.active='1' order by plugin_blog_posts.id desc LIMIT 4");
	
	while($blog = $q->fetch_object())
	{
		$thumbnail = URL.'thumb?src='.URL.$pathBlog."/".$blog->thumb.'&w=50&h=50';
		$link = URL.$href->href.'/'.$blog->category_href.'/'.$blog->post_href;
		$date = explode(' ',$blog->date);
		echo '<ul>';
		$query = $this->mysqli->query("select * from plugin_blog_comments where post_id='$blog->id'");
		$num = $query->num_rows;
		echo '<li>
				<div class="bordered alignleft">
					<figure class="add-border">
						<img src="'.$thumbnail.'" alt="" />
					</figure>
				</div><!--/ .bordered-->
				<h6><a href="'.$link.'">'.$blog->title.'</a></h6>
				<div class="entry-meta">'.date("M d, Y",strtotime($date[0])).', 
				<a href="'.URL.PAGEHREF.'/'.$blog->category_href.'/'.$blog->post_href.'#comments">'.$num.' Comments </a></div>
			</li>';
		echo '</ul>';
	}
	else :
	echo "Plugin Tidak di temukan";
	endif;
	?>
</ul>
