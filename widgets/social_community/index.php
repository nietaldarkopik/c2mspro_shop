<?PHP
$query = $this->mysqli->query("SELECT * FROM widgets_social_community");
$data = $query->fetch_object();
?>
<ul class="social-icons clearfix">
    <li class="facebook"><a href="http://www.facebook.com/<?PHP echo $data->facebook;?>" target="_blank"><span></span></a></li>
    <li class="twitter"><a href="http://www.twitter.com/<?PHP echo $data->twitter;?>" target="_blank"><span></span></a></li>
	<li class="google"><a href="http://plus.google.com/<?PHP echo $data->google;?>/posts" target="_blank"><span></span></a></li>
    <!--<li class="vimeo"><a href="http://www.vimeo.com/<?PHP echo $data->vimeo;?>" target="_blank">Our Vimeo Videos<span></span></a></li>-->
</ul>
<div class="clear"></div>
