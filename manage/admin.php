<?php 
session_start();
$adminid = $_SESSION['adminid'];
include './../languages/id.php';
include './../configs/vars.php';
include './../configs/database.php';
include './../libraries/dbconnect.php'; 
include './../libraries/image.php';
include './../libraries/favicon.php';
include './../libraries/settings.php';
if (isset($_POST["action"])) :
$author = $_POST["author"];
$email = $_POST["email"];
$avatar = $_FILES["avatar"];
$mysqli->query("UPDATE admins set email='$email', admin_name='$author' where id='$adminid'");
$tmp_name = $avatar["tmp_name"];
if ($tmp_name != "") {
	$thumbNail = "avatar-admin.png";
	move_uploaded_file($tmp_name, "../images/user/$thumbNail");
}
header("location:".URL);
endif;
$query = "SELECT * FROM admins WHERE id='$adminid'";
$q = $mysqli->query($query);
$data = $q->fetch_object();
?>
<div class="adminContent">
<h2>Author Setting</h2>

<form method="post" action="<?PHP echo URL; ?>manage/admin.php" enctype="multipart/form-data">
    <p>
        <label>Author /User</label>
        <input type="text" name="author" class="input medium" value="<?PHP echo $data->admin_name; ?>" />
    </p>
    <p>
        <label>Email</label>
        <input type="text" name="email" class="input medium" value="<?PHP echo $data->email;;?>" />
    </p>
    <p>
        <label>Avatar</label>
        <input type="file" name="avatar" class="input medium"/>
		<span class="description">PNG (16x16 PX)</span>
    </p>
    <br class="clear"/>
    <p>
        <input type="hidden" name="action" value="general" />
        <input type="submit" name="submit" value="Save Changes" class="submit"/>
    </p>
</form>
</div>