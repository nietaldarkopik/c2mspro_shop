<?php 
include './../languages/id.php';
include './../configs/vars.php';
include './../configs/database.php';
include './../libraries/dbconnect.php'; 
include './../libraries/image.php';
include './../libraries/favicon.php';
include './../libraries/settings.php';
$act = $_GET["act"];
if ($act == "general")
{
	$q		= $mysqli->query("select * from settings where param='site'");
	$site	= $q->fetch_object();
	$tag = explode("|",$site->value);
	
	$q				= $mysqli->query("select * from settings where param='organization'");
	$organization	= $q->fetch_object();
	
	$q		= $mysqli->query("select * from settings where param='email'");
	$email	= $q->fetch_object();
	
	$q		= $mysqli->query("select * from settings where param='altemail'");
	$altemail	= $q->fetch_object();
	
	$q		= $mysqli->query("select * from settings where param='footer'");
	$footer	= $q->fetch_object();
	
	$q		= $mysqli->query("select * from settings where param='menu'");
	$menu	= $q->fetch_object();
	
	$q		= $mysqli->query("select * from settings where param='keyword'");
	$keyword	= $q->fetch_object();
	
	$q		= $mysqli->query("select * from settings where param='description'");
	$description	= $q->fetch_object();
?>
<div class="adminContent">
<h2>General Setting</h2>

<form method="post" action="<?PHP echo URL;?>manage/action.php" enctype="multipart/form-data">
    <p>
        <label>Site Title</label>
        <input type="text" name="SiteTitle" class="input medium" value="<?PHP echo $tag[0]?>" />
    </p>
	<p>
        <label>Site Keywords</label>
        <textarea name="keyword" class="input medium" rows="3"><?PHP echo $keyword->value;?></textarea>
    </p>
	<p>
        <label>Site Description</label>
        <textarea name="description" class="input medium" rows="3"><?PHP echo $description->value;?></textarea>
    </p>
    <p>
        <label>Tagline</label>
        <input type="text" name="Tagline" class="input medium" value="<?PHP echo $tag[1];?>" />
        <span class="description">Ketik slogan website Anda disini</span>
    </p>
    <p>
        <label>Email Address</label>
        <input type="text" name="EmailAddress" class="input medium" value="<?PHP echo $email->value;?>"/>
    </p>
    <p>
        <label>Alternatif Email</label>
        <input type="text" name="AlternateEmail" class="input medium" value="<?PHP echo $altemail->value;?>"/>
    </p>
    <p>
        <label>Copyright</label>
        <input type="text" name="Copyright" class="input medium" value="<?PHP echo $footer->value;?>"/>
    </p>
    <p>
        <label>Custom Logo</label>
        <input type="file" name="customLogo" class="input medium"/>
        <span class="description">
        <?php list($w,$h) = @getimagesize('./../templates/'.TPL.'/images/logo.png'); ?>
        Dimensi file image yang disarankan adalah : <?php echo $w.' x '.$h ?> pixel dan maksimal ukuran file adalah 1 MB (GIF, JPG, JPEG atau PNG)</span>
    </p>
    <p>
        <label>Custom Favicon</label>
        <input type="file" name="customFavicon" class="input medium"/>
        <span class="description">Upload gambar 16px x 16px type file .ico untuk favicon</span>
    </p>
    <br class="clear"/>
    <p>
        <input type="hidden" name="action" value="general" />
        <input type="submit" name="submit" value="Save Changes" class="submit"/>
    </p>
</form>
</div>
<?PHP 
} else if ($act =="header") { 
	include '../templates/'.$tpl.'/manage/header.php';
} 
?>