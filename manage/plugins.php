<?php 
include './../languages/id.php';
include './../configs/database.php';
include './../libraries/dbconnect.php';
include './../configs/vars.php'; 
include './../libraries/settings.php';

class plugins {
	private $mysqli;
	function __construct(mysqli $conn) {
		$this->mysqli = $conn;
	}
	function aktif($plugin) {
		$this->mysqli->query("UPDATE plugins SET active='1' WHERE plugin='$plugin'");
		$this->mysqli->query("UPDATE pages SET status='1', public='1' WHERE content='$plugin'");
	}
	function nonaktif($plugin) {
		$this->mysqli->query("UPDATE plugins SET active='0' WHERE plugin='$plugin'");
		$this->mysqli->query("UPDATE pages SET status='0', public='0' WHERE content='$plugin'");
	}
}
$plugin = new plugins($mysqli);
if (isset($_REQUEST["nonactive"])) $plugin->nonaktif($_REQUEST["nonactive"]);
if (isset($_REQUEST["active"])) $plugin->aktif($_REQUEST["active"]);
?>
<div class="adminContent">
	<h2>Plugins <!--<a href="#" class="gradientButton">Plugins bank</a>>--></h2>
    <table id="dataTable" cellpadding="0" cellspacing="0">				
		<thead>
			<tr>
				<th width="20%">Plugins</th>
				<th width="50%">Title</th>
				<th width="30%">&nbsp;</th>
			</tr>
		</thead>					
<?PHP
$query = $mysqli->query("SELECT * FROM plugins");
?>		
		<tbody>
			<?PHP
			while ($data = $query->fetch_object())
			{
				if ($data->active== 1) 
					$act = '<a class="openmodalbox" href="javascript:void(0);" title="unpublish post"><button type="submit" class="submit">Non Active</button> <input type="hidden" name="ajaxhref" value="'.URL.'manage/plugins.php?nonactive='.$data->plugin.'" class="small" /></a>';
				else
					$act = '<a class="openmodalbox" href="javascript:void(0);" title="unpublish post"><button type="submit" class="submit">Active</button> <input type="hidden" name="ajaxhref" value="'.URL.'manage/plugins.php?active='.$data->plugin.'" class="small" /></a>';
				echo '<tr>
					<td><a href="#"> <a class="openmodalbox" href="javascript:void(0);"><input type="hidden" name="ajaxhref" value="'.URL.'plugins/'.$data->plugin.'/manage.php"><strong>'.$data->plugin.'</strong></a></td>
					<td><strong>'.$data->title.'</strong></td>							
					<td>'.$act.'</td>
				</tr>';
			}
			?>					
		</tbody>
	</table>
</div>