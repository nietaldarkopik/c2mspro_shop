<?php 
include './../languages/id.php';
include './../configs/database.php';
include './../libraries/dbconnect.php';
include './../configs/vars.php'; 
include './../libraries/settings.php'; 
include './../components/widget.php';
include './../libraries/function.php';
$widget = new widget($mysqli);
$query = $mysqli->query("SELECT id, title FROM pages WHERE public='1'");
while($data = $query->fetch_assoc()) {
    $menuArray[$data["id"]] = $data["title"];
}

if (isset($_GET["publish"])) $widget->publish($_GET["publish"]);
else if (isset($_GET["unpublish"])) $widget->unpublish($_GET["unpublish"]);
else if (isset($_GET["up"])) $widget->up($_GET["up"]);
else if (isset($_GET["down"])) $widget->down($_GET["down"]);
else if (isset($_POST["update_widget_setting"]))
{
	if ($_REQUEST["pageid"][0] || $_REQUEST["pageid"][0] !="" || !empty($_REQUEST["pageid"][0])) $pageid = implode(",",$_REQUEST["pageid"]); else $pageid ="all";
	$widget->settings($_REQUEST["update_widget_setting"], $_REQUEST["alias"], $_REQUEST["show_alias"], $_REQUEST["position"], $pageid, $_REQUEST["status"]);
}
?>
<div class="adminContent">
	<?PHP
	if (isset($_GET["widget"]))
	{
	$edit = $_GET["widget"];
	$query= $mysqli->query("SELECT * FROM widgets WHERE widget='$edit'");
	$value = $query->fetch_object();
	?>
		<h2>Setting Widget <?PHP echo $edit." ($value->alias)"; ?></h2>

	    <form id="general" action="<?PHP echo URL.'manage/widgets.php'; ?>" methode= "post" enctype="multipart/form-data">
	    	<p>
	        	<label>Publish this widget</label>
	            <?PHP
	            $status = array("Choose:","Publish","Unpublish");
				echo selectOption($status,"",$value->status,'class="small"','status');
	            ?>
	        </p>
	        <p>
	        	<label>Select Widget Position</label>
				<?PHP
	            $position = array("1"=>"top","2"=>"left","3"=>"right","4"=>"bottom","5"=>"center");
				echo selectOption($position,"Choose:",$value->position,'class="small"','position');
	            ?>
	        </p>
	        <p>
	        	<label>Set on Page</label>
				<?PHP 
				if ($value->page_id != "all") $selected = explode(",",$value->page_id); else $selected="";
				echo selectOption($menuArray,"All Page",$selected,'multiple="multiple" class="small" size="7"','pageid[]'); 
				?>
	            <span class="description">Gunakan tombol CTRL untuk memilih lebih dari satu</span>
	        </p>
	    	<p>
	        	<label>Widget Title</label>
	            <input type="text" name="alias" class="input medium" value="<?PHP echo $value->alias; ?>" />
	            <span class="description">Kosongkan jika tidak menggunakan title</span>
	        </p>
			<p>
	        	<label>Show Alias</label>
				<?PHP
	            $show_alias = array("Choose:","Ya","Tidak");
				echo selectOption($show_alias,"",$value->show_alias,'class="small"','show_alias');
	            ?>
	        </p>
	        <p>
				<input type="hidden" name="update_widget_setting" value="<?PHP echo $_GET["widget"]; ?>"/>
	            <input type="submit" name="submit" value="Save Changes" class="openmodalbox submit"/>
	        </p>
	    </form>
	<p align="right"> <a class="openmodalbox" href="javascript:void(0);"><i>>> Back To List </i><input type="hidden" name="ajaxhref" value="<?php echo URL ?>manage/widgets.php" /></a></p>
	<?PHP
	}else{
	?>
	<h2>Setting Widget <!--<a href="#" class="gradientButton">Add New</a>--></h2>
    <table id="dataTable" cellpadding="0" cellspacing="0">				
		<thead>

			<tr>
				<th width="25%">Widget</th>
				<th width="25%">Title</th>
				<th width="35%">Set on Page</th>
                <th width="10%">Position</th>
 				<th width="5%">&nbsp;</th>
			</tr>
		</thead>					
		
		<tbody>
			<?PHP
			 $position = array("1"=>"top","2"=>"left","3"=>"right","4"=>"bottom","5"=>"center");
			foreach($widget->_list() as $value)
			{
				if ($value["page_id"] !="all")
				{
				$menu = explode(",",$value["page_id"]);
				$listmenu = "";
				foreach ($menu as $data) {
					if (array_key_exists($data, $menuArray)) $listmenu[] = $menuArray[$data];
				}
				$listmenu = implode(",", $listmenu);
				}else $listmenu = "all Page";
				if ($value["status"]== 1) 
					$act = '<span class="unpublish-post"><a class="openmodalbox" class="openmodalbox" href="javascript:void(0);" title="unpublish post">Unpublish <input type="hidden" name="ajaxhref" value="'.URL.'manage/widgets.php?unpublish='.$value["id"].'" class="small" /></a></span>';
				else
					$act = '<span class="publish-post"><a class="openmodalbox" class="openmodalbox" href="javascript:void(0);" title="publish post">Publish<input type="hidden" name="ajaxhref" value="'.URL.'manage/widgets.php?publish='.$value["id"].'" class="small" /></a></span>';
					
				echo '<tr>
					<td><a href="#"><strong>'.$value["widget"].'</strong></a><br /><div class="post-action"><span class="edit-post"><a class="openmodalbox" href="javascript:void(0);">Setting<input type="hidden" name="ajaxhref" value="'.URL.'manage/widgets.php?widget='.$value["widget"].'" class="small" /></a></span> | <span class="edit-post"><a class="openmodalbox" href="javascript:void(0);"><input type="hidden" name="ajaxhref" value="'.URL.'widgets/'.$value["widget"].'/manage.php" />
Edit</a></span> | '.$act.'</div></td>							
					<td>'.$value["alias"].'</td>
	                <td>'.$listmenu.'</td>
	                <td>'.$position[$value["position"]].'</td>
					<td>
					<ul id="orderpage">
					<li>
					<a class="openmodalbox pageUp" href="javascript:void(0);"><input type="hidden" name="ajaxhref" value="'.URL.'manage/widgets.php?up='.$value["id"].'"/><span>Up</span></a></li> 
					<li><li><a class="openmodalbox pageDown" href="javascript:void(0);"><input type="hidden" name="ajaxhref" value="'.URL.'manage/widgets.php?down='.$value["id"].'"/><span>Down</span></a></li></ul></td>
				</tr>';
			}
			?>					
		</tbody>
	</table>
	<?PHP } ?>
</div>
<?php
include 'footer.php'; 
?>