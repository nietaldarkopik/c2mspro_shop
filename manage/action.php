<?PHP
include './../languages/id.php';
include './../configs/vars.php';
include './../configs/database.php';
include './../libraries/dbconnect.php'; 
include './../libraries/image.php';
include './../libraries/resize_class.php';
include './../libraries/favicon.php';
include './../libraries/settings.php';
$act = isset($_POST["action"]) ? $_POST["action"] : "";
if ($act == "general")
{
	$randomStr = md5(microtime());
	$resultStr = substr($randomStr,27,5);
	$title = $_POST['SiteTitle']."|".$_POST['Tagline'];
	@$mysqli->query("update settings set value='$_POST[description]' where param='description'");
	@$mysqli->query("update settings set value='$_POST[keyword]' where param='keyword'");
	@$mysqli->query("update settings set value='$title' where param='site'");
	@$mysqli->query("update settings set value='$_POST[organization]' where param='organization'");
	@$mysqli->query("update settings set value='$_POST[EmailAddress]' where param='email'");
	@$mysqli->query("update settings set value='$_POST[AlternateEmail]' where param='altemail'");
	@$mysqli->query("update settings set value='$_POST[Copyright]' where param='footer'");
	if(!empty($_FILES["customLogo"]["name"]))
	{	
		@unlink('./../images/user/'.LOGO);
		list($w,$h) = @getimagesize('./../templates/'.TPL.'/images/logo.png');
		$pic	= 'logo_'.$resultStr.'.'.ext($_FILES['customLogo']['name']);
		
		move_uploaded_file($_FILES['customLogo']['tmp_name'], "./../images/user/$pic");
		//$resizeObj = new resize("./../images/user/$pic");
		//$resizeObj -> resizeImage($w, $h, "crop");
		//$resizeObj -> saveImage("./../images/user/$pic", 100);				
		//resize($_FILES['customLogo']['tmp_name'],$pic,$w,$h);		
		$mysqli->query("update settings set value='$pic',usedefault='0' where param='logo'");
	}
	
	if(!empty($_FILES["customFavicon"]["name"]))
	{	
		if(substr($_FILES['customFavicon']['name'],-3) == 'ico')
		{	
			$pic	= 'favicon_'.$resultStr.'.ico';
			$uploaddir = './../images/user/';
			$uploadfile = $uploaddir.$pic;

			if(move_uploaded_file($_FILES['customFavicon']['tmp_name'], $uploadfile))
			{
				@unlink('./../images/user/'.FAVICON);
				$mysqli->query("update settings set value='$pic',usedefault='0' where param='favicon'");
			}
		}
		else
		{
			$base	= 'favicon_'.$resultStr;
			$pic	= $base.'.'.ext($_FILES['customFavicon']['name']);				
			favicon($_FILES['customFavicon']['tmp_name'],$pic,16,16);		

			@unlink('./../images/user/'.FAVICON);
			$favicon = $base.'.png';
			$mysqli->query("update settings set value='$favicon',usedefault='0' where param='favicon'");
		}
	}
	header("location: ../");
}

elseif(isset($_GET['installTheme']))
{
	if($mysqli->query("update settings set value='$_GET[installTheme]' where param='template'"))
	{
		header("location: ../");
	}
}
elseif(isset($_GET['deleteTheme']))
{
	include './../libraries/rrmdir.php';
	delete_folder('./../templates/'.$_GET['deleteTheme']);
        header("location: ../");
}
?>