<?php 
include './../../../configs/vars.php';
include './../../../configs/database.php';
include './../../../libraries/dbconnect.php'; 
include '../../../libraries/settings.php';
include './../../../libraries/url.php';

if(isset($_POST['doedit']))
{
	$halaman	= $_POST['halaman'];
	$slideshow	= $_POST['slideshow'];
	$show_title = $_POST['show_title'];
	$site_title = $_POST['site_title'];
	if($_POST['type'] == 'page')
	{
		$href		= generate_seo_link($_POST['title']);
		$page_type	= 'page';

		//Array ( [parent] => 127 [title] => Office [type] => page [keywords] => [description] => [id] => 136 [doedit] => Save ) 
		$mysqli->query("update pages set pid='$_POST[parent]',href='$href',title='$_POST[title]',site_title='$_POST[site_title]',keywords='$_POST[keywords]',description='$_POST[description]',page_type='$page_type', pageType='$halaman', slideshow='$slideshow', show_title='$show_title' where id='$_POST[id]'");
		
		//print_r($_POST);
	}
	elseif($_POST['type'] == 'shortcut')
	{
		$href		= $_POST['shortcut'];
		$page_type	= 'shortcut';

		$mysqli->query("update pages set pid='$_POST[parent]',href='$href',title='$_POST[title]',site_title='$_POST[site_title]',keywords='$_POST[keywords]',description='$_POST[description]',page_type='$page_type', pageType='$halaman', slideshow='$slideshow', show_title='$show_title' where id='$_POST[id]'");

		//print_r($_POST);
	}
	else
	{
		$href		= generate_seo_link($_POST['title']);
		$content    = $_POST['type'];
		$page_type	= 'plugin';
		
		$mysqli->query("update pages set pid='$_POST[parent]',href='$href',title='$_POST[title]',site_title='$_POST[site_title]',keywords='$_POST[keywords]',description='$_POST[description]',page_type='$page_type',content='$content', pageType='$halaman', slideshow='$slideshow', show_title='$show_title' where id='$_POST[id]'");

		//print_r($_POST);
	}
	$cek = $mysqli->query("SELECT plugin FROM plugins WHERE plugin='sitemap' AND active='1'");
		if ($cek->num_rows != 0) :
		include ("../../../plugins/sitemap/libraries.php");
		$sitemap = new sitemap(array("mysqli" => $mysqli));
		$sitemap->website = URL;
		$sitemap->path = "../../../";
		$sitemap->generate();
	endif;
	header("location: ../../../");
}
else if(isset($_POST['addmenu']))
{
	// Array ( [parent] => [title] => asdsadsadsa [type] => statics [keyword] => asdsa [description] => asdasdsa [addmenu] => Add )
	$query = $mysqli->query("SELECT max(oid) as oid FROM pages");
	$data = $query->fetch_object();
	$num = (is_null($data->oid) == true) ? "0" : $data->oid;
	$oid = $num+1;
	$pid		= $_POST['parent'];
	
	$title		= strip_tags($_POST['title']);		
	$keywords	= strip_tags($_POST['keywords']);		
	$desc		= strip_tags($_POST['description']);
	$halaman	= $_POST['halaman'];
	$slideshow	= $_POST['slideshow'];
	$show_title = $_POST['show_title'];
	$site_title = $_POST['site_title'];
	if($_POST['type'] == 'page')
	{
		$href		= generate_seo_link($_POST['title']);
		$content    = '';
		$page_type	= 'page';
	}
	elseif($_POST['type'] == 'shortcut')
	{
		$href		= $_POST['shortcut'];
		$content    = '';
		$page_type	= 'shortcut';
	}
	else
	{
		$href		= generate_seo_link($_POST['title']);
		$content    = $_POST['type'];
		$page_type	= 'plugin';
	}
	
	$status		= '1';
	$public		= '1';
	
	if($mysqli->query("insert into pages(pid,oid,href,title,site_title,keywords,description,page_type,content,status,public,slideshow,pageType, show_title) value('$pid','$oid','$href','$title','$site_title','$keywords','$desc','$page_type','$content','$status','$public','$slideshow','$halaman', '$show_title')"))
	{
		$cek = $mysqli->query("SELECT plugin FROM plugins WHERE plugin='sitemap' AND active='1'");
		if ($cek->num_rows != 0) :
		include ("../../../plugins/sitemap/libraries.php");
		$sitemap = new sitemap(array("mysqli" => $mysqli));
		$sitemap->website = URL;
		$sitemap->path = "../../../";
		$sitemap->generate();
		endif;
	
		echo '<script>alert("Halaman '.$title.'  telah dibuat."); location.href=\'../../../\'; </script>';
	}
}

else if(isset($_GET['edit']))
{	
	include 'edit.php';
}
else if(isset($_GET['add']))
{	
	include 'add.php';
}
elseif(isset($_GET['list']))
{
	if(isset($_GET['up']))
	{
		//OID Halaman saat ini
		$id = $_GET["id"];
		$q = $mysqli->query("select pid, oid from pages where id='$id'");
		$r = $q->fetch_object();
		$thisoid = $r->oid;	
		$pid = $r->pid;	
	
		//OID halaman yg diatasnya
		$q = $mysqli->query("SELECT id, oid FROM pages WHERE oid < '$thisoid' and pid='$pid' ORDER BY oid DESC LIMIT 1 ");		
		if($q->num_rows != 0)
		{	
			$s = $q->fetch_object();
			$newoidup = $s->oid;
			$idup = $s->id;
			if($mysqli->query("update pages set oid='$newoidup' where id='$id'"))
			{						
				$mysqli->query("update pages set oid='$thisoid' where id='$idup'");
			}	
		}		
	}
	elseif(isset($_GET['down']))
	{
		//OID Halaman saat ini
		$id = $_GET["id"];
		$q = $mysqli->query("select pid, oid from pages where id='$id'");
		$r = $q->fetch_object();
		$thisoid = $r->oid;	
		$pid = $r->pid;	


		//OID halaman yg dibawahnya
		$q = $mysqli->query("SELECT id, oid FROM pages WHERE oid > '$thisoid' and pid='$pid' ORDER BY oid ASC LIMIT 1 ");		
		if($q->num_rows != 0)
		{	
			$s = $q->fetch_object();
			$newoidup = $s->oid;
			$idup = $s->id;
			if($mysqli->query("update pages set oid='$newoidup' where id='$id'"))
			{						
				$mysqli->query("update pages set oid='$thisoid' where id='$idup'");
			}	
		}	
	}
	elseif(isset($_GET['delete']))
	{
		if(isset($_GET['id']))
		{
			$mysqli->query("delete from pages where id='$_GET[id]'");
            $mysqli->query("delete from pages where pid='$_GET[id]'");
		}	
	}
	elseif(isset($_GET['homepage']))
	{
		if(isset($_GET['id']))
		{
			$q = "update settings set value='$_GET[id]' where param='homepage'";
			$mysqli->query($q);
		}	
	}	

	include 'list.php';
}
?>