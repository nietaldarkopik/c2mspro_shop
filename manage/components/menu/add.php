<div class="adminContent">
	<h2>Add Menu</h2>
    
    <form method="post" action="<?php echo $url; ?>manage/components/menu/menu.php" enctype="multipart/form-data">
    	<p>
        	<label>Menu</label>
            <input type="text" name="title" class="input medium" />
        </p>
        <p>
        	<label>Page Type</label>
            <select name="type">
            	<option value="" onclick="hide('addlink',true);">Pilih:</option>
                <option value="page" onclick="hide('addlink',true);">Page/Article</option>
                <option value="shortcut" onclick="show('addlink',true);">External Link</option>
				<?php
				$q	=  $mysqli->query("select * from plugins where active='1'");
				while($plugins = $q->fetch_object())
				{
					echo '<option value="'.$plugins->plugin.'" onclick="hide(\'addlink\',true);">'.$plugins->title.'</option>';
				}
				?>
            </select>
        </p>
		<p>
        	<label>Slideshow</label>
            <input type="radio" name="slideshow" value="1" /> Yes
			<input type="radio" name="slideshow" value="0" /> No
        </p>
		<p>
        	<label>Show Title</label>
            <input type="radio" name="show_title" value="1" /> Yes
			<input type="radio" name="show_title" value="0" /> No
        </p>
		<p id="addlink" style="display:none;">
			<label>link</label>
			<input type="text" id="shortcut" name="shortcut"  class="input medium" value="" />
		</p>
		<p>
			<label>Halaman</label>
			<select name="halaman" required>
				<option value="">pilih</option>
            	<option value="full">Full Page</option>
                <option value="left">Left Sidebar</option>
				<option value="right">Right Sidebar</option>
            </select>
		</p>
        <p>
        	<label>Parent</label>
            <select name="parent">
            	<option value="">Pilih:</option>
                <?php
				$q	=  $mysqli->query("select id,title from pages where pid='0' and page_type!='shortcut' AND href !='error-page'");
				while($parent = $q->fetch_object())
				{
					echo '<option value="'.$parent->id.'">'.$parent->title.'</option>';

					$q2	=  $mysqli->query("select id,title from pages where pid='$parent->id' and page_type!='shortcut'");
					while($parent = $q2->fetch_object())
					{
						echo '<option value="'.$parent->id.'">&nbsp;&nbsp;&nbsp;'.$parent->title.'</option>';
					}
				}
				?>
            </select>
        </p>
		<p>
        	<label>Title</label>
            <input type="text" name="site_title" class="input long"/>
        </p>
        <p>
        	<label>Keyword</label>
            <input type="text" name="keywords" class="input long"/>
        </p>
        <p>
        	<label>Description</label>
            <textarea name="description" class="long"></textarea>
        </p>
        <p>
            <input type="submit" name="addmenu" value="Add Menu" class="submit"/>
        </p>
    </form>
</div>