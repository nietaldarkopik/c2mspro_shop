<?php
$q = $mysqli->query("select * from pages where id='$_GET[id]'");
$r = $q->fetch_object();
?>
<div class="adminContent">
	<h2>Edit Menu</h2>
    
    <form method="post" action="<?php echo $url ?>manage/components/menu/menu.php" enctype="multipart/form-data">
    	<p>
        	<label>Menu</label>
            <input type="text" name="title" class="input medium" value="<?php echo $r->title; ?>"/>
        </p>
        <p>
        	<label>Page Type</label>
            <select name="type" id="type" style="width:220px;">
				<option value="page" onclick="hide('addlink',true);">Page/Article</option>
				<option value="shortcut" <?php if($r->page_type == 'shortcut') { echo 'selected'; } ?> onclick="show('addlink',true);">External Link</option>
				<?php
				$q	=  $mysqli->query("select * from plugins where active='1'");
				while($plugins = $q->fetch_object())
				{
					if($r->page_type == plugin)
					{
						if($r->content == $plugins->plugin)
						{
							echo '<option value="'.$plugins->plugin.'" onclick="hide(\'addlink\',true);" selected>'.$plugins->title.'</option>';
						}
					}
					else
					{
						echo '<option value="'.$plugins->plugin.'" onclick="hide(\'addlink\',true);" >'.$plugins->title.'</option>';
					}
				}
				?>
			</select>
        </p>
		<?php if($r->page_type != 'shortcut') $class="style=\"display:none\""; else $class ="";?>
		<p id="addlink"<?PHP echo $class;?>>
			<label>link</label>
			<input type="text" id="shortcut" name="shortcut"  class="input medium" value="<?PHP echo $r->href; ?>" />
		</p>
		<p>
        	<label>Slideshow</label>
            <input type="radio" name="slideshow" value="1" <?PHP echo ($r->slideshow == "1") ? "checked='checked'" : ""; ?> /> Yes
			<input type="radio" name="slideshow" value="0" <?PHP echo ($r->slideshow == "0") ? "checked='checked'" : ""; ?>/> No
        </p>
		<p>
        	<label>Show Title</label>
            <input type="radio" name="show_title" value="1" <?PHP echo ($r->show_title == "1") ? "checked='checked'" : ""; ?> /> Yes
			<input type="radio" name="show_title" value="0" <?PHP echo ($r->show_title == "0") ? "checked='checked'" : ""; ?> /> No
        </p>
		<p>
			<label>Halaman</label>
			<select name="halaman" required>
				<option value="">pilih</option>
            	<option value="full" <?PHP echo ($r->pageType == "full") ? "selected='selected'" : ""; ?>>Full Page</option>
                <option value="left" <?PHP echo ($r->pageType == "left") ? "selected='selected'" : ""; ?>>Left Sidebar</option>
				<option value="right" <?PHP echo ($r->pageType == "right") ? "selected='selected'" : ""; ?>>Right Sidebar</option>
            </select>
		</p>
        <p>
        	<label>Parent</label>
            <select name="parent">
            	<?php 
				if($r->pid != 0)
				{			
					$qy	=  $mysqli->query("select id,title from pages where id='$r->pid'");
					$p	= $qy->fetch_object();

					echo '<option value="'.$p->id.'">'.$p->title.'</option>';
				}
				else
				{
					echo '<option value="0">&nbsp;</option>';
				}
				$q	=  $mysqli->query("select id,title from pages where pid='0' and page_type!='shortcut' AND href !='error-page'");
				while($parent = $q->fetch_object())
				{
					echo '<option value="'.$parent->id.'">'.$parent->title.'</option>';

					$q2	=  $mysqli->query("select id,title from pages where pid='$parent->id' and page_type!='shortcut'");
					while($parent = $q2->fetch_object())
					{
						echo '<option value="'.$parent->id.'">&nbsp;&nbsp;&nbsp;'.$parent->title.'</option>';
					}
				}
				?>
            </select>
        </p>
		<p>
        	<label>Title</label>
            <input type="text" name="site_title" class="input long" value="<?php echo $r->site_title; ?>"/>
        </p>
        <p>
        	<label>Keyword</label>
            <input type="text" name="keywords" class="input long" value="<?php echo $r->keywords; ?>" />
        </p>
        <p>
        	<label>Description</label>
            <textarea name="description" class="long"><?php echo $r->description; ?></textarea>
        </p>
        <p>
			<input type="hidden" name="id" value="<?php echo $r->id; ?>" />
            <input type="submit" name="doedit" value="Save Changes" class="submit"/>
        </p>
    </form>
</div>