<?php
include './../languages/id.php';
include './../configs/vars.php';
include './../configs/database.php';
include './../libraries/dbconnect.php'; 
include './../libraries/settings.php';
include './../libraries/image.php';
include './../libraries/favicon.php';
?>
<div class="adminContent">
	<h2>Themes</h2>
    <table id="dataTable" cellpadding="0" cellspacing="0">				
		<thead>

			<tr>
				<th width="25%">Templete Name</th>
				<th width="30%">Preview</th>
				<th width="15%">Status</th>
				<th width="35%">&nbsp;</th>
			</tr>
		</thead>					
		
		<tbody>
		<?php
	 $dir =  '../templates/';
		if (is_dir($dir)) 
		{
			if ($data = opendir($dir)) {
				while (false !== ($file = readdir($data)))
				{
					if((filetype($dir.$file) == 'dir') && ($file != '.') && ($file != '..') && ($file{0} != '.'))
					{
						$screenshoot = is_file($dir.$file."/screenshot.png") ? "<a href=\"".URL."templates/".$file."/screenshot.png\" target=\"_blank\">
						<img src=\"".URL."templates/".$file."/screenshot.png\" width=\"100\" height=\"75\" /> </a>": "No Image Availabale";
						if($file == TPL)
						{
							echo '<tr>
								<td><a href="#"><strong>'.ucwords($file).'</strong></a></td>
								<td><strong>'.$screenshoot.'</a></td>
								<td>Aktif</td>
								<td>&nbsp;</td>
							</tr>';
						}
						else
						{
								echo '<tr>
									<td><a href="#"><strong>'.ucwords($file).'</strong></a></td>
									<td><strong>'.$screenshoot.'</a></td>									
									<td>nonAktif</td>
									<td>
									<input type="submit" name="submit" value="Active" class="submit" onclick="location.replace(\''. URL.'manage/action.php?installTheme='.$file.'\');" />
									<input type="submit" name="submit" value="Delete" class="openmodalbox submit" onclick="location.replace(\''. URL.'manage/action.php?deleteTheme='.$file.'\');" />
									
									</td>
								</tr>';
						}				
					}
				}
				closedir($data);
			}
		}?>
							
		</tbody>
	</table>
</div>